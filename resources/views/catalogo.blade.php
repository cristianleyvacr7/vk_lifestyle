@extends('layout')
@section('head')

	<!-- opengraph-->
	@if(isset($productos['metacampo']['nombre']))
		@if(strtolower($productos['metacampo']['nombre']) == 'ofertas')
			<meta property="og:site_name" content="Ofertas de Calzado de Piel para Dama - My VK Shoes | Leather Lifestyle"/>
			<title>Ofertas de Calzado de Piel para Dama - My VK Shoes | Leather Lifestyle</title>
		@endif
		@if(strtolower($productos['metacampo']['nombre']) == 'sandalias')
			<meta property="og:site_name" content="Sandalias de Piel para Dama - My VK Shoes | Leather Lifestyle"/>
			<title>Sandalias de Piel para Dama - My VK Shoes | Leather Lifestyle</title>
		@endif
		@if(strtolower($productos['metacampo']['nombre']) == 'botas')
			<meta property="og:site_name" content="Botas de Piel para Dama - My VK Shoes | Leather Lifestyle"/>
			<title>Botas de Piel para Dama - My VK Shoes | Leather Lifestyle</title>
		@endif
	@else
		<meta property="og:site_name" content="Calzado de Piel para Dama - My VK Shoes | Leather Lifestyle"/>
		<title>Calzado de Piel para Dama - My VK Shoes | Leather Lifestyle</title>
	@endif
    <meta property="og:image" content="{{asset('images/opengraph.jpg')}}"/>

	@vite(['resources/stylus/catalogo.styl'])
@endsection
@section('contenido')
	<div class="catalogo">
		<div class="layout">
			<div class="catalogo-top">
				<div class="catalogo-top-33">
					@if(count($filtros)>0)
						<div class="filtro" id="abrir-filtros">
							<p>Filtro</p>
							<span class="icon icon-desglozar"></span>
						</div>
					@endif
				</div>
				<div class="catalogo-top-33">
					@if(count($banners) <= 0)
						<h1 class="titulo">{{ isset($productos['metacampo']['nombre']) ? strtoupper($productos['metacampo']['nombre']) : 'CATALOGO' }}</h1>
					@endif
				</div>
				<div class="catalogo-top-33">

				</div>
			</div>
			<div class="catalogo-productos">
				@if(isset($productos['productos']) && isset($productos['productos']['data']) && count($productos['productos']['data']) > 0)
					<div class="productos">
						@foreach($productos['productos']['data'] as $producto)
							<div class="productos-item">
								<a href="{{route('detalle', $producto['slug'])}}">
									<div class="productos-item-imagen">
										@if(count($producto['imagenes'])>0)
											@if(count($producto['imagenes'])>1)
												<img src="{{$producto['imagenes'][1]['imagen_url']}}" alt="VK_LIFESTYLE">
												<img class="scnd" src="{{$producto['imagenes'][0]['imagen_url']}}" alt="K_LIFESTYLE">
											@else
												<img src="{{$producto['imagenes'][0]['imagen_url']}}" alt="VK_LIFESTYLE">
											@endif
										@else
											<img src="/images/default.jpg" alt="VK_LIFESTYLE">
										@endif
										<span class="etiqueta">
											<svg xmlns="http://www.w3.org/2000/svg" width="17" height="21" viewBox="0 0 17 21" fill="none">
												<path d="M15.9535 5.73902C16.0767 5.57049 16.2046 5.39609 16.3278 5.22062L16.4009 5.11395V4.76889L16.3439 4.67235C16.2963 4.59129 16.2476 4.50969 16.1994 4.42862C16.0959 4.25315 15.9871 4.07182 15.8887 3.89529C15.7334 3.61155 15.5263 3.23769 15.3368 2.86062C15.2794 2.73355 15.186 2.62736 15.0689 2.55605C14.9518 2.48474 14.8165 2.45165 14.6807 2.46115C14.3219 2.46809 13.9558 2.46649 13.6006 2.46542H13.293C12.1088 2.46275 11.2736 1.80995 10.9432 0.628087C10.93 0.583615 10.9143 0.539983 10.8961 0.49742L10.8842 0.46862L10.7874 0.21582H5.99153L5.90661 0.496887C5.75127 1.01369 5.54829 1.40355 5.27179 1.71769C4.84875 2.20089 4.33561 2.44409 3.70648 2.45902C3.12448 2.47289 2.55179 2.46702 2.11839 2.45902C1.98396 2.45107 1.85027 2.48444 1.73429 2.5549C1.61831 2.62536 1.52528 2.72973 1.467 2.85475C1.19723 3.37369 0.912954 3.89795 0.63852 4.40515L0.598132 4.47929C0.579337 4.50916 0.558575 4.53767 0.535995 4.56462C0.516837 4.58915 0.497678 4.61422 0.479037 4.63929L0.401367 4.74595V5.14222L0.504927 5.25689C1.18428 6.01849 1.7347 6.93742 2.18674 8.06649C2.82208 9.65156 2.7325 11.2436 1.91438 12.9326C1.47632 13.8366 1.02894 14.53 0.504927 15.115L0.401367 15.2297V15.5444L0.453147 15.6356C0.54428 15.8023 0.635759 15.9689 0.727582 16.1353C0.958002 16.554 1.19619 16.9886 1.41781 17.4153C1.58868 17.7476 1.83464 17.9022 2.1914 17.9022H2.19813C2.32551 17.9022 2.45289 17.9022 2.58079 17.898C3.11205 17.8905 3.61638 17.8836 4.10622 17.9513C5.3013 18.1177 6.3369 18.7604 7.19179 19.3678C7.40253 19.5172 7.60085 19.6878 7.81315 19.8697C7.90946 19.9534 8.00888 20.0393 8.10985 20.123L8.21651 20.2116H8.61108L8.72344 20.1049C8.76124 20.0681 8.798 20.0302 8.83477 19.9929C8.89485 19.9283 8.95884 19.8678 9.02635 19.8116C10.0231 19.0276 11.1276 18.2649 12.4438 18.0084C13.0098 17.898 13.6265 17.9017 14.223 17.9065H14.4539H14.4632C14.791 17.9065 15.198 17.834 15.4595 17.2889C15.6475 16.8974 15.867 16.5065 16.0808 16.1278C16.173 15.9641 16.2647 15.8004 16.3548 15.635L16.4065 15.5438V15.2382L16.3133 15.1257C14.9893 13.5294 14.3214 12.0324 14.2116 10.4084C14.1355 9.28409 14.4498 8.15449 15.2011 6.85315C15.4322 6.46804 15.6833 6.09611 15.9535 5.73902ZM14.0086 13.778C14.2732 14.2686 14.5829 14.7337 14.8821 15.1838C14.9505 15.2868 15.0194 15.3902 15.0893 15.4932C14.8485 15.923 14.6362 16.3193 14.4425 16.7006H14.428C14.2292 16.7097 14.0236 16.714 13.8248 16.7177C13.2697 16.7289 12.6955 16.7401 12.1326 16.8526C10.7273 17.1332 9.54104 17.923 8.46816 18.7342L8.40137 18.7849L8.29781 18.7081C8.05858 18.5326 7.81108 18.3508 7.55994 18.1806C6.05263 17.1614 4.67373 16.6724 3.27049 16.6724C3.06993 16.6724 2.86885 16.6823 2.66726 16.7022C2.37522 16.7305 2.30221 16.6665 2.21729 16.4457C2.09492 16.1488 1.94802 15.8633 1.7782 15.5924L1.71865 15.4905C2.32458 14.6933 2.82614 13.8175 3.21043 12.8857C4.04201 10.8761 3.94001 8.83235 2.907 6.81742C2.63101 6.27875 2.30014 5.77102 1.98014 5.28035C1.89108 5.14382 1.80202 5.00782 1.71451 4.87075C1.93664 4.47609 2.1536 4.07075 2.36072 3.66435L2.55697 3.66809C2.97121 3.67555 3.39632 3.68409 3.82195 3.65369C5.20499 3.55502 6.20797 2.80729 6.80292 1.43182C6.80913 1.41689 6.81483 1.40569 6.81897 1.39715H6.84072C7.9281 1.40995 8.99321 1.40569 9.98376 1.39715C10.6398 2.92035 11.7769 3.68409 13.365 3.66542H13.4908C13.8015 3.66169 14.1262 3.65742 14.4374 3.67235C14.63 4.01209 14.8112 4.35502 14.9867 4.68569L15.0546 4.81422C14.9448 5.00142 14.834 5.18755 14.7258 5.36622C14.399 5.91075 14.0904 6.42489 13.8165 6.97635C12.711 9.20569 12.7757 11.4932 14.0086 13.778Z" fill="white"/>
											</svg>
											PIEL
										</span>
										@if(date('Y-m-d', strtotime($producto['created_at'])) > date('Y-m-d', strtotime(date('Y-m-d')."- 1 week")))
											<span class="etiqueta">NEW</span>
										@endif
									</div>
									<div class="productos-item-precio">
										<span class="txt-black">{{ $producto['nombre'] }}</span>
										@if(isset($producto['variantes'][0]['precio_descuento']) && $producto['variantes'][0]['precio_descuento'] > 0)
											<p style="text-decoration: line-through; color: rgb(255, 77, 77);">${{ number_format($producto['variantes'][0]['precio'], 2, '.', ',') }} {{ app()->isLocale('es') ? 'MXN' : 'USD' }}</p>
											<p>${{ number_format($producto['variantes'][0]['precio_descuento'], 2, '.', ',') }} {{ app()->isLocale('es') ? 'MXN' : 'USD' }}</p>
										@else
											<p>${{ number_format($producto['variantes'][0]['precio'], 2, '.', ',') }} {{ app()->isLocale('es') ? 'MXN' : 'USD' }}</p>
										@endif
									</div>
								</a>
								<div class="compra-rapida">
									<div class="compra-rapida-contenido">
										<div class="talla">
											<p>Seleccionar talla</p>
											<span class="icon icon-cerrar"></span>
										</div>
										<div class="recuadros">
											@foreach($producto['variantes'] as $variante)
												<div class="recuadros-item">
													<input type="radio" name="talla-producto-{{$producto['producto_id']}}" id="talla-producto-{{$variante['id']}}" {{ $variante['stock'] < 1 ? 'disabled' : '' }} @click="seleccionarProductoVariante({{$variante['id']}})">
													<label for="talla-producto-{{$variante['id']}}">{{ $variante['nombre'] }}</label>
												</div>
											@endforeach
										</div>
										<div class="boton" @click="addToCart({ variante_id: producto_variante_id_selected, cantidad: 1, codigo_pais: '{{ isset($position) && $position->countryCode ? $position->countryCode : 'US' }}', moneda: '{{ app()->isLocale('es') ? 'MXN' : 'USD' }}' })">
											<button>Agregar al carrito</button>
										</div>
									</div>
								</div>
							</div>
						@endforeach
						{{$paginacion->appends(request()->except('page'))->onEachSide(5)->links()}}
					</div>
				@endif
			</div>
		</div>
    </div>
	@if(count($filtros)>0)
		<div class="filtro-float">
			<div class="layout">
				<div class="filtro-float-titulo">
					<p>Filtro</p>
					<span class="icon icon-cerrar" id="cerrar-filtros"></span>
				</div>
				<div class="filtro-float-contenido">
					<div class="filtro-float-contenido-opciones">
						@foreach ($filtros as $key => $filtro)
							<div class="opcion">
								<p {{ $key == 0 ? 'class=selected' : '' }} onclick="mostrarFiltro('{{$filtro['slug']}}')" style="cursor: pointer" id="opcion-{{$filtro['slug']}}">{{ strtoupper($filtro['nombre']) }}</p>
							</div>
						@endforeach
					</div>
					<div class="filtro-float-contenido-resultado">
						@foreach ($filtros as $key => $filtro)
							<div class="filtro-float-contenido-resultado-item colores" {{ $key != 0 ? 'style=display:none;' : '' }} id="div-valores-{{$filtro['slug']}}">
								<div class="inputs">
									@foreach ($filtro['catalogos'] as $catalogo)
										<div class="input checkbox col-33">
											<input
												type="checkbox"
												id="check-{{$filtro['slug']}}-{{$catalogo['slug']}}"
												name="check-{{$filtro['slug']}}"
												value="{{$catalogo['slug']}}"
											>
											<label for="check-{{$filtro['slug']}}-{{$catalogo['slug']}}"><span class="check"></span>{{$catalogo['nombre']}}</label>
										</div>
									@endforeach
								</div>
							</div>
						@endforeach
					</div>
				</div>
				<div class="filtro-float-aplicar">
					<button id="aplicar-filtros">Aplicar</button>
					<a href="{{ route('catalogo', $metacampo_slug) }}"><p class="ver-mas">Eliminar filtros</p></a>
				</div>
			</div>
		</div>
	@endif
@endsection
@section('js')
    <script src="{{ asset('plugins/slick/slick.min.js') }}"></script>
	<script src="{{ asset('/plugins/jquery.jscroll.min.js') }}" type="module"></script>

	<script type="module">
		$(function(){
            @foreach($productos['filtros'] as $filtro)
                @foreach($filtro['valores'] as $valor)
                    $('#check-{{$filtro["filtro"]}}-{{$valor}}').prop('checked', true);
                @endforeach
            @endforeach
		});

		$('#cerrar-filtros').on('click', function(){
			$('.filtro-float').removeClass('active')
		})

		$('#abrir-filtros').on('click', function(){
			$('.filtro-float').addClass('active')
		})

		$('#aplicar-filtros').on('click', function(){
			let $params = {};
			@foreach ($filtros as $filtro)
				if($('input[name="check-{{$filtro["slug"]}}"]:checked')?.length > 0){
					let catalogos = [];
					$('input[name="check-{{$filtro["slug"]}}"]:checked').each(function() {
						catalogos.push($(this).val());
					});
					$params['{{$filtro["slug"]}}'] = catalogos.join(',');
				}
			@endforeach
			let $url = "{{url()->current()}}";
			let $result = $.param($params);//.replace(/%2C/g, ',');
			$url = $url + '?' + $result;
			location.href = $url;
		});

		$('ul.pagination').hide();
        $('.catalogo-productos').jscroll({
            autoTrigger: true,
            loadingHtml: '<p style="text-align:center; font-size: 18px; padding: 40px 0;" class="lazyloading">Cargando más productos...</p>',
            padding: 0,
            nextSelector: '.pagination li.active + li a',
            contentSelector: 'div.productos',
            callback: function() {
                $('ul.pagination').remove();
            }
        });

	</script>
	<script>
		function mostrarFiltro(filtro){
			$(".filtro-float-contenido-resultado-item").hide();
			$("#div-valores-"+filtro).show();
			$('.opcion p').removeClass('selected');
			$('#opcion-'+filtro).addClass('selected');
		}
	</script>
@endsection
