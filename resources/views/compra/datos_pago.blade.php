<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
	<link rel="manifest" href="/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" type="text/css" href="{{asset('/plugins/sweetalert/sweetalert.css')}}">
    <link rel="stylesheet" href="{{asset('/plugins/validationEngine/validationEngine.jquery.css')}}">
	<script src="https://js.stripe.com/v3/"></script>
	<script src="https://checkout.stripe.com/checkout.js"></script>

	<!-- Google tag (gtag.js) -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-KXX7Q2QCHG"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'G-KXX7Q2QCHG');
	</script>

	<!-- Meta Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window, document,'script','https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '419164004404931');
	fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=419164004404931&ev=PageView&noscript=1"/></noscript>
	<!-- End Meta Pixel Code -->

	@vite(['resources/stylus/compra.styl'])
    <script src="{{asset('/plugins/sweetalert/sweetalert.min.js')}}"></script>
    <title>DATOS DE PAGO - VK_LIFESTYLE</title>
</head>
<body>
    <div class="spinner">
		<div id="loading"></div>
	</div>
    <div class="top-bar">
        <div class="img">
            <a href="{{route('home')}}">
				<img src="{{asset('/images/logo.svg')}}" alt="VK_LIFESTYLE">
            </a>
        </div>
    </div>
    <div class="Compra" id="app">
        <div class="layout">
            <div class="menu-oruga">
                <div class="menu-oruga-item">
                    <a href="{{ route('home', 'carrito') }}">CARRITO</a>
                    <span class="icon icon-siguiente"></span>
                </div>
                <div class="menu-oruga-item">
                    <a href="{{ route('datos_envio') }}">DATOS DE ENVÍO</a>
                    <span class="icon icon-siguiente"></span>
                </div>
                <div class="menu-oruga-item active">
                    <a href="{{ route('datos_pago') }}">PAGO</a>
                    <span class="icon icon-siguiente"></span>
                </div>
                <div class="menu-oruga-item">
                    <span>RESUMEN</span>
                </div>
            </div>
            <div class="Compra-container">
                <div class="col">
                    <div class="title-1">
                        <h1>Pago</h1>
                    </div>
                    <div class="metodos">
						@if(isset($credenciales['stripe']))
							<div id="div-tarjeta" class="item selected">
								<a href="#">
									<img src="/images/visa.jpg" alt="VK_LIFESTYLE">
								</a>
							</div>
							@if(app()->isLocale('es'))
								<div id="div-oxxo" class="item">
									<a href="#">
										<img src="/images/oxxo.png" alt="VK_LIFESTYLE" style="width: 100px">
									</a>
								</div>
							@endif
						@endif
                        {{-- <div id="div-paypal" class="item">
                            <a href="#">
                                <img src="/images/paypal.jpg" alt="VK_LIFESTYLE">
                            </a>
                        </div> --}}
                    </div>
                    <div class="form">
                        <form id="form-pay" action="{{ route('datos_pago') }}" method="post">
							@csrf
                            <input id="input-metodo-pago" name="metodo_pago" value="{{ isset($credenciales['stripe']) ? 'tarjeta' : 'paypal' }}" hidden>
							@if(isset($credenciales['stripe']))
								<div class="group-inputs">
									<h3>datos de la tarjeta</h3>
									<br>
									<div class="input">
										<div id="card-element" class="stripe-input">
											<!-- A Stripe Element will be inserted here. -->
										</div>

										<div id="card-errors" role="alert" class="stripe-error"></div>
									</div>
									@if($credenciales['stripe']['no_meses'] != 0 && app()->isLocale('es'))
										<div v-if="venta?.total >= 300">
											<label class="opcion">
												<input type="radio" name="no_meses" value="0" checked>
												<span class="outer">
													<span class="inner"></span>
												</span> 1 Pago
											</label>
											<div class="subtitulo">
												<h3>Meses sin intereses</h3>
											</div>
											@if($credenciales['stripe']['no_meses'] >= 3)
												<label class="opcion" v-if="venta?.total >= 300">
													<input type="radio" name="no_meses" value="3">
													<span class="outer">
														<span class="inner"></span>
													</span> 3 Meses de @{{ (venta?.total / 3) | currency }} x mes
												</label>
											@endif
											@if($credenciales['stripe']['no_meses'] >= 6)
												<label class="opcion" v-if="venta?.total >= 600">
													<input type="radio" name="no_meses" value="6">
													<span class="outer">
														<span class="inner"></span>
													</span> 6 Meses de @{{ (venta?.total / 6) | currency }} x mes
												</label>
											@endif
											@if($credenciales['stripe']['no_meses'] >= 9)
												<label class="opcion" v-if="venta?.total >= 900">
													<input type="radio" name="no_meses" value="9">
													<span class="outer">
														<span class="inner"></span>
													</span> 9 Meses de @{{ (venta?.total / 9) | currency }} x mes
												</label>
											@endif
											@if($credenciales['stripe']['no_meses'] >= 12)
												<label class="opcion" v-if="venta?.total >= 1200">
													<input type="radio" name="no_meses" value="12">
													<span class="outer">
														<span class="inner"></span>
													</span> 12 Meses de @{{ (venta?.total / 12) | currency }} x mes
												</label>
											@endif
										</div>
									@endif
								</div>
							@endif
                        </form>


                    </div>
                    <div class="buttons-bot">
                        <div class="buttons-bot-item">
                            <a href="{{route('datos_envio')}}" class="btn-return"><span class=""></span>Volver a datos de envío</a>
                        </div>
                        <div class="buttons-bot-item">
                            <button id="accion-pagar" class="btn-sig">PAGAR</button>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="resume">
						<p class="cantidad-titulo"><span>@{{ cantidad_productos }}</span> @{{ cantidad_productos == 1 ? 'PRODUCTO' : 'PRODUCTOS' }}</p>
                        <div class="carrito-contenido-tabla">
							<div class="carrito-contenido-tabla-item" v-for="producto in venta?.productos">
								<div class="seccion1">
									<div class="seccion1-imagen">
										<template v-if="producto?.producto?.producto_imagenes?.length > 0">
											<img :src="'{{ env('URL_CDN_PV') }}images/productos/'+producto?.producto?.producto_imagenes[0]?.imagen" alt="VK_LIFESTYLE">
										</template>
										<template v-else>
											<img src="/images/default.jpg" alt="VK_LIFESTYLE">
										</template>
									</div>
									<div class="seccion1-texto">
										<div class="seccion1-texto-info">
											<h3>@{{ producto?.producto?.nombre }} - @{{ producto?.producto?.nombre_variante }}</h3>
											<p v-if="producto?.precio_unitario != producto?.precio_venta" style="text-decoration: line-through">@{{ producto?.precio_unitario | currency }} @{{ venta?.moneda }}</p>
											<p>@{{ producto?.precio_venta | currency }} @{{ venta?.moneda }}</p>
											<br>
											<p>Cantidad: @{{ producto?.cantidad }}</p>
										</div>
									</div>
								</div>
								<div class="seccion">
									<p class="ver-mas">Eliminar</p>
								</div>
							</div>
						</div>
                        <div class="codigo">
                            <h3>código de descuento</h3>
                            <div class="codigo-input">
								<div v-if="venta">
									<div v-if="!venta?.cupon_cantidad || venta?.cupon_cantidad == 0">
										<input v-on:keyup.enter="aplicarDescuento()" v-model="venta.cupon_codigo" type="text" placeholder="Código de descuento">
										<button @click="aplicarDescuento()">APLICAR</button>
									</div>
									<div v-else>
										<p style="color: white">@{{ venta?.cupon_codigo }}</p>
										<button @click="eliminarDescuento()">ELIMINAR</button>
									</div>
								</div>
                            </div>
                        </div>
						<div class="suma">
                            <div class="campo">
                                <p>Subtotal:</p>
                                <p>@{{ venta?.subtotal | currency }} @{{ venta?.moneda }}</p>
                            </div>
							<div class="campo" v-if="venta?.descuento_3x2 > 0">
                                <p>Promoción 3x2:</p>
                                <p>-@{{ venta?.descuento_3x2 | currency }} @{{ venta?.moneda }}</p>
                            </div>
                            <div class="campo" v-if="venta?.descuento > 0">
                                <p>Descuento:</p>
                                <p>-@{{ venta?.descuento | currency }} @{{ venta?.moneda }}</p>
                            </div>
                            <div class="campo">
                                <p>Envío:</p>
                                <p>@{{ venta?.envio | currency_envio }}<span v-if="venta?.envio > 0"> @{{ venta?.moneda }}</span></p>
                            </div>
                            <div class="campo total">
                                <p>TOTAL:</p>
                                <p><strong>@{{ venta?.total | currency }} @{{ venta?.moneda }}</strong></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
@vite(['resources/js/app.js'])
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="{{asset('/plugins/spin.min.js')}}"></script>
<script src="{{asset('/plugins/validationEngine/jquery.validationEngine.js')}}"></script>
<script src="{{asset('/plugins/validationEngine/jquery.validationEngine-es.js')}}"></script>
<script>
    $(function(){
        @if(Session::get('alert') != null)
            swal("", "{{Session::get('alert')['mensaje']}}", "{{Session::get('alert')['estatus']}}");
        @endif
    });
</script>
<script type="module">

	@if(isset($credenciales['stripe']))
		var stripe = Stripe("{{$credenciales['stripe']['public_key']}}");
		var elements = stripe.elements();
		var style = {
			base: {
				fontSize: '16px',
				color: '#000',
			}
		};

		var card = elements.create("card", {
			hidePostalCode: true,
			style: style
		});

		card.mount("#card-element");

		let input_valor = 'tarjeta';
		$('#div-tarjeta').click(function(){
			input_valor = 'tarjeta';
			$('.group-inputs').show();
			$('#div-tarjeta').addClass('selected');
			$('#div-oxxo').removeClass('selected');
			$('#input-metodo-pago').val(input_valor);
		});

		$('#div-oxxo').click(function(){
			input_valor = 'oxxo';
			$('.group-inputs').hide();
			$('#div-tarjeta').removeClass('selected');
			$('#div-oxxo').addClass('selected');
			$('#input-metodo-pago').val(input_valor);
		});

	@else
		let input_valor = 'paypal';
	@endif
    // $('#div-paypal').click(function(){
    //     input_valor = 'paypal';
    //     $('.group-inputs').hide();
    //     $('#div-tarjeta').removeClass('selected');
    //     $('#div-paypal').addClass('selected');
    //     $('#input-metodo-pago').val(input_valor);
    // });

	$('#accion-pagar').on('click', function(event) {
			event.preventDefault();
			$('.spinner').show();

			$("#accion-pagar").prop( "disabled", true);
			if (input_valor == 'tarjeta') {
				stripe.createToken(card).then(function(result) {
                    if(result.error)
                    {
                        $('.spinner').hide();
                        var errorElement = document.getElementById('card-errors');
                        $("#accion-pagar").prop( "disabled", false);
                        errorElement.textContent = result.error.message;
                    }
                    else
                    {
                        stripeTokenHandler(result.token);
                    }
				});
			}
			else if(input_valor == 'oxxo'){

				console.log('ENTRA A LA FUNCION OXXO');

				//oxxoSourceHandler();

				var ventaEcommerce;

				fetch('/new-intent')
					.then(response => response.json())
					.then(data => {

						console.log('DATA');
						console.log(data);

						$('.spinner').hide();


						const clientSecret = data.payment_intent.client_secret;
						ventaEcommerce = data.venta_ecommerce;


						oxxoSourceHandler(ventaEcommerce);

						// return stripe.confirmOxxoPayment(
						// 	clientSecret,
						// 	{
						// 		payment_method: {
						// 			billing_details: {
						// 				name: 'Alan López',
						// 				email: 'a@gmail.com'
						// 			}
						// 		}
						// 	}
						// );
						// })
						// .then(function(result) {

						// 	console.log('RESULT');
						// 	console.log(result);


						// 	console.log('VENTA ECOMMERCE');
						// 	console.log(ventaEcommerce);

						// 	if (result.error) {
						// 		$('.spinner').hide();
						// 		var errorElement = document.getElementById('card-errors');
						// 		$("#accion-pagar").prop("disabled", false);
						// 		errorElement.textContent = result.error.message;
						// 	} else {
						// 		$('.spinner').hide();
						// 		oxxoSourceHandler(result.paymentIntent, ventaEcommerce);
						// 	}
						});


			}


	});

	@if(isset($credenciales['stripe']))
		function stripeTokenHandler(token) {
			var form = document.getElementById('form-pay');
			var hiddenInput = document.createElement('input');
			hiddenInput.setAttribute('type', 'hidden');
			hiddenInput.setAttribute('name', 'stripeToken');
			hiddenInput.setAttribute('value', token.id);
			form.appendChild(hiddenInput);
			form.submit();
		}


		function oxxoSourceHandler(ventaEcommerce) {

			console.log('ENTRA A LA FUNCION OXXO HANDLER');

			var form = document.getElementById('form-pay');
			// var hiddenInput = document.createElement('input');
			// hiddenInput.setAttribute('type', 'hidden');
			// hiddenInput.setAttribute('name', 'intent');
			// hiddenInput.setAttribute('value', intent.id);

			var hiddenInput2 = document.createElement('input');
			hiddenInput2.setAttribute('type', 'hidden');
			hiddenInput2.setAttribute('name', 'ventaEcommerce');
			hiddenInput2.setAttribute('value', ventaEcommerce);

			//form.appendChild(hiddenInput);
			form.appendChild(hiddenInput2);
			form.submit();
		}

	@endif
    var opts = {
			lines: 8 // The number of lines to draw
			, length: 18 // The length of each line
			, width: 5 // The line thickness
			, radius: 20 // The radius of the inner circle
			, scale: 1 // Scales overall size of the spinner
			, corners: 1 // Corner roundness (0..1)
			, color: '#FFFFFF' // #rgb or #rrggbb or array of colors
			, opacity: 0.25 // Opacity of the lines
			, rotate: 0 // The rotation offset
			, direction: 1 // 1: clockwise, -1: counterclockwise
			, speed: 1 // Rounds per second
			, trail: 60 // Afterglow percentage
			, fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
			, zIndex: 2e9 // The z-index (defaults to 2000000000)
			, className: 'spinner' // The CSS class to assign to the spinner
			, top: '50%' // Top position relative to parent
			, left: '50%' // Left position relative to parent
			, shadow: true // Whether to render a shadow
			, hwaccel: false // Whether to use hardware acceleration
			, position: 'absolute' // Element positioning
		};

    var target = document.getElementById('loading');
    var spinner = new Spinner(opts).spin(target);

</script>
</html>

