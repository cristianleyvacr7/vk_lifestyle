@extends('layout')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{asset('/plugins/slick/slick.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('/plugins/slick/slick-theme.css')}}">
	<script src="https://js.stripe.com/v3/"></script>
	<script src="https://checkout.stripe.com/checkout.js"></script>
	@vite(['resources/stylus/gracias.styl'])
    <title>GRACIAS POR TU COMPRA - VK_LIFESTYLE</title>
@endsection

@section('contenido')
    <div class="Gracias">
        <div class="Gracias-banner">
            <h1>¡GRACIAS POR TU COMPRA!</h1>
        </div>
		@if(isset($venta['id']))
			<div class="layout">
				<div class="Gracias-orden">
					@if($venta['folio'] != null)
						<h2>No. de orden: {{ str_pad($venta['folio'], 8, "0", STR_PAD_LEFT) }}</h2>
					@else
						<h2>No. de orden: {{ str_pad($venta['id'], 8, "0", STR_PAD_LEFT) }}</h2>
					@endif

					@if (isset($voucher_url))
					<div class="oxxo" style="display: flex; justify-content: center; align-items: center; flex-direction: column;">
						<a href="{{ $voucher_url }}" id="oxxo-voucher" target="_blank" style="text-align: center;">
							Descargar información de pago
							<br>
							<img src="/images/oxxo.png" alt="OXXO" style="width: 100px; margin-top: 10px;">
							<p><strong>Tu pago en OXXO se verá reflejado a las 24 hrs hábiles. Te sugerimos tomar una foto a tu comprobante y guardarlo para cualquier aclaración.</strong></p>
						</a>
					</div>
					@endif


					<p>Hemos enviado un mensaje de confirmacion de tu compra al correo:
						<br>
						<br>
						<strong>{{ $venta['cliente']['email'] }}</strong>
					</p>
					<p>
						Dirección de envío
						<br>
						<br>
						@php
							$direccion = $venta['cliente']['direcciones'][0]['calle'].' #'.$venta['cliente']['direcciones'][0]['numero_ext'];
							if($venta['cliente']['direcciones'][0]['numero_int'])
							{
								$direccion .= ' no. int. '.$venta['cliente']['direcciones'][0]['numero_int'];
							}
							$direccion .= ' Col. '.$venta['cliente']['direcciones'][0]['colonia'].', '.$venta['cliente']['direcciones'][0]['ciudad'].'. '.$venta['cliente']['direcciones'][0]['estado'].', C.P. '.$venta['cliente']['direcciones'][0]['cp'];
						@endphp
						<strong>{{ $direccion }}</strong>
					</p>
					<div class="boton" style="text-align: center;">
						<a href="{{ env('URL_PV').'odcs/vk/'.str_pad($venta['id'], 8, "0", STR_PAD_LEFT).'.pdf' }}" target="_blank">
							<button style="width: 50%; font-weight: 700;">Descargar orden de compra</button>
						</a>
					</div>
				</div>
				<div class="Gracias-resumen">
					<div class="carusel" id="carusel">
						@foreach ($venta['productos'] as $producto)
							<div class="carusel-item">
								<div class="img">
									@if(count($producto['producto']['producto_imagenes'])>0)
										<img src="{{ env('URL_CDN_PV') }}images/productos/{{$producto['producto']['producto_imagenes'][0]['imagen']}}" alt="VK_LIFESTYLE">
									@else
										<img src="/images/default.jpg" alt="VK_LIFESTYLE">
									@endif
								</div>
								<div class="txt">
									<p>{{ $producto['producto']['nombre'] }} - {{ $producto['producto']['nombre_variante'] }}</p>
									<div class="campo">
										<span>Cantidad:</span>
										<span>{{ intval($producto['cantidad']) }}</span>
									</div>
									<br>
									<div class="campo">
										@if($producto['precio_venta'] != $producto['precio_unitario'])
										<p style="text-decoration: line-through">${{ number_format($producto['precio_venta'], 2, '.', ',') }} {{ $venta['moneda'] }}</p>
										@endif
										<span class="price">${{ number_format($producto['precio_venta'], 2, '.', ',') }} {{ $venta['moneda'] }}</span>
									</div>
								</div>
							</div>
						@endforeach
					</div>
					<div class="suma">
						<h3>Total</h3>
						<div class="suma-content">
							<div class="campo">
								<p>Subtotal:</p>
								<p>${{ number_format($venta['subtotal'], 2, '.', ',') }} {{ $venta['moneda'] }}</p>
							</div>
							@if ($venta['descuento_3x2'] > 0)
								<div class="campo">
									<p>Promoción 3x2:</p>
									<p>-${{ number_format($venta['descuento_3x2'], 2, '.', ',') }} {{ $venta['moneda'] }}</p>
								</div>
							@endif
							@if ($venta['descuento'] > 0)
								<div class="campo">
									<p>Descuento:</p>
									<p>-${{ number_format($venta['descuento'], 2, '.', ',') }} {{ $venta['moneda'] }}</p>
								</div>
							@endif
							<div class="campo">
								<p>Envío:</p>
								<p>{{ $venta['envio'] != 0 ? '$'.number_format($venta['envio'], 2, '.', ',').' '.($venta['moneda']) : 'GRATIS' }}</p>
							</div>
						</div>
						<div class="campo total">
							<p>TOTAL:</p>
							<p><strong>${{ number_format($venta['total'], 2, '.', ',') }} {{ $venta['moneda'] }}</strong></p>
						</div>
					</div>
				</div>
			</div>
		@endif
    </div>
@endsection
@section('js')
    <script src="{{ asset('plugins/slick/slick.min.js') }}"></script>
    <script type="module">
        $('#carusel').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: false,
            arrows: true,
            prevArrow: '<button class="slick-arrow slick-prev icon-anterior"></button>',
            nextArrow: '<button class="slick-arrow slick-next icon-siguiente"></button>',
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: 950,
                    settings: {
                        slidesToShow: 3,
                    }
                },
                {
                    breakpoint: 860,
                    settings: {
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: 590,
                    settings: {
                        slidesToShow: 1,
                    }
                }
            ]
        });
    </script>
	<script type="module">
		$(".header").addClass("fixed");
		$(".header-container ul li").addClass("fixed");
        $(".header-container").addClass("fixed");
		$(window).scroll(function() {
            var scroll = $(window).scrollTop();
            if (scroll >= 0) {
                $(".header").addClass("fixed");
				$(".header-container ul li").addClass("fixed");
                $(".header-container").addClass("fixed");
            }
        });
	</script>
@endsection
