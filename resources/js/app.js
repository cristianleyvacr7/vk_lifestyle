import "./bootstrap";

window.Vue = Vue;
import Vue from "vue";
import Vuex from 'vuex';
import cart from './store/cart';
import { mapState, mapActions } from 'vuex';
import component_instagram from './components/component-instagram.vue';


Vue.use(Vuex);
Vue.component("component-instagram", component_instagram);

const store = new Vuex.Store({
    modules: {cart}
});

Vue.filter('currency', function (value) {
	if (!value) return '$0.00';
	var $currency = parseFloat(value).toFixed(2);
	$currency = $currency.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	return '$' + $currency;
});

Vue.filter('currency_envio', function (value) {
	if (!value) return 'GRATIS';
	var $currency = parseFloat(value).toFixed(2);
	$currency = $currency.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	return $currency == 0 ? 'GRATIS' : '$' + $currency;
});
const app = new Vue({
    el: "#app",
    store: store,
	data: {
        producto_variante_id_selected: null,
		// posts_instagram: []
	},
    mounted() {
		this.$nextTick(() => {
			$('.bag-count').show();
		});
		this.fetchCart();
	},
	methods: {
        ...mapActions(['fetchCart', 'addToCart', 'updateQuantity', 'removeFromCart']),

		seleccionarProductoVariante: function(producto_variante){
            this.producto_variante_id_selected = producto_variante;
		},

        aplicarDescuento: async function(){
            try {
                if (this.venta.cupon_codigo == null || this.venta.cupon_codigo.trim() == '') {
                    return swal('','No ha ingresado un código de descuento','warning');
                }
                $('.spinner').show();
                const response = await axios.post(`/api/cart/cupon/aplicar/${encodeURIComponent(this.venta.cupon_codigo)}`);
                if(response.data.alert){
                    $('.spinner').hide();
                    return swal('',response.data.alert,'warning');
                }

                this.fetchCart();
                $('.spinner').hide();
                swal('','Cupón aplicado','success');
            } catch (error) {
                console.error(error);
                console.log(e.response);
                swal('','Ocurrió un error al cargar la información','error');
            }
        },

        eliminarDescuento: async function(){
            try {
                $('.spinner').show();
                const response = await axios.get(`/api/cart/cupon/eliminar`);
                if(response.data.alert){
                    $('.spinner').hide();
                    return swal('', response.data.alert, 'warning');
                }

                this.fetchCart();
                $('.spinner').hide();
                swal('','Cupón eliminado','success');
            } catch (error) {
                console.error(error);
                console.log(e.response);
                swal('','Ocurrió un error al cargar la información','error');
            }
        },
    },
	computed: {
		...mapState({
            venta: state => state.cart.venta,
            cantidad_productos: state => state.cart.cantidad_productos,
        }),
	}
});
