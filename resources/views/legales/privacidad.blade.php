@extends('layout')
@section('head')
	@vite(['resources/stylus/aviso.styl'])
    <title>AVISO DE PRIVACIDAD - VK LIFESTYLE</title>
@endsection
@section('contenido')
    <div class="aviso">
        <div class="aviso-contenido layout">
            <div class="aviso-contenido-menu">
                <div class="opciones">
                    <div class="opciones-opcion">
                        <a href="{{route('privacidad')}}" id="privacidad" class="selected">AVISO DE PRIVACIDAD</a>
                    </div>
                    <div class="opciones-opcion">
                        <a href="{{route('cambios')}}" id="cambios">CAMBIOS Y DEVOLUCIONES</a>
                    </div>
                    <div class="opciones-opcion">
                        <a href="{{route('terminos')}}" id="terminos">TÉRMINOS Y CONDICIONES</a>
                    </div>
                </div>
            </div>
            <div class="aviso-contenido-informacion">
                <div class="privacidad active">
					<h1>Aviso de privacidad</h1>
					<p>
						Vk Leather S.A de C.V. con domicilio en calle Elba N 505, Fraccionamiento Amberes Residencial C.P. 37235 en Leon Gto. México, en poseedor y operador de https://myvkshoes.com/ Vk Leather S.A de C.V. es y se hace responsable del manejo de datos personales, por lo cual reconocen y privilegian la privacidad y confidencialidad de cualquier información que tengan o lleguen a tener en su poder, ya sea de manera temporal o definitiva, incluyendo datos o información personal. En Vk Leather S.A. de C.V. reconocemos que toda la información personal que se encuentra en nuestro poder ha sido proporcionada de manera libre y voluntaria por sus titulares y/o ha sido adquirida de forma legítima a través de medios legales idóneos bajo el principio de privacidad y confidencialidad de la información, con el objeto de garantizar la seguridad y protección de la misma y de sus titulares.
						<br>
						<br>
						Por lo anterior, hemos desarrollado un Aviso de Privacidad que deseamos le ayude a comprender la forma en la que recabamos, utilizamos, transferimos, almacenamos y protegemos su información.
						<br>
						<br>
						La información personal, incluso la patrimonial o sensible, está constituida por datos que pueden ser utilizados exclusivamente para identificar o contactar a una sola persona. Se puede solicitar información personal en cualquier momento cuando te contactes con Vk Leather S.A. de C.V. o una compañía afiliada a Vk Leather S.A. de C.V. y sus afiliadas pueden compartir información personal entre ellas y utilizarla en virtud del presente Aviso de Privacidad. Así mismo podrán combinarla con otra información para proporcionar y manejar nuestros productos, servicios, contenidos y publicidad.
						<br>
						<br>
						De conformidad con la Ley de Protección de Datos Personales en Posesión de los Particulares (la Ley) Vk Leather S.A. de C.V. al procesar la información del cliente está obligado a observar los principios de licitud, consentimiento, información, calidad, finalidad, lealtad, proporcionalidad y responsabilidad previstos por la Ley. Mantendremos políticas, medidas de seguridad física, electrónica y de procedimientos para proteger la confidencialidad de la información que obtenemos sobre nuestros clientes y aquellos que utilizan y visitan nuestro sitio Web.
						<br>
						<br>
						Vk Leather S.A. de C.V. hace de su conocimiento que sus datos personales recabados, que se recaben o sean generados con motivo de la relación jurídica que tengamos celebrada, o que en su caso se celebre; los datos que recabamos son:
						<br>
						<br>
						● Nombre completo
						<br>
						● Correo electrónico
						<br>
						● Domicilio
						<br>
						● Número telefónico
						<br>
						● Número telefónico (móvil)
						<br>
						● Datos de Tarjeta bancaria y/o datos de cuenta bancaria

					</p>
                    <div class="preguntas-bloque">
                        <div class="preguntas-bloque-titulo">
                            <h3>FINALIDADES ESENCIALES.</h3>
                            <div class="icono">
                                <span class="icon icon-desglozar"></span>
                            </div>
                        </div>
                        <div class="preguntas-bloque-respuesta">
                            <p>
                                1. Lacomercializaciondetodaclasedecalzado
								<br>
								2. Lafacturacióndelosserviciosprestadosoproductosadquiridos.
								<br>
								<br>
								Al proporcionar los datos personales de terceros usted reconoce tener consentimiento de éstos para que nosotros tratemos sus datos para contactarlos.
								<br>
								Queda convenido que usted acepta la transferencia que pudiera realizarse con estos fines, en su caso a las entidades que conformen parte de nosotros directo o indirectamente, subsidiarias y afiliadas, así como a Terceros Nacionales o Extranjeros, los cuales son:
								<br>
								<br>
								a) Instituciones Bancarias y demás proveedores relacionados en virtud del procesamiento de pagos.
								<br>
								b) Personas relacionadas con nosotros para fines relacionados con el servicio, comprobación, revisión y certificación en materia fiscal y administrativa.
								<br>
								c) Proveedoresquenosasistanparaelefectivocumplimientodelosservicios.
								<br>
								d) Empresas que tengan celebrados convenios de colaboración con nosotros para
								llevar a cabo la promoción y ofrecimiento de sus productos y servicios.
								<br>
								e) Terceros para dar cumplimiento a la legislación aplicable y/o resoluciones
								judiciales o administrativas.
								<br>
								f) La autoridad competente cuando lo requiera.
								<br>
								<br>
								Para limitar el uso y divulgación de sus datos, mantendremos medidas de seguridad física, administrativa y técnica, políticas y procedimientos para proteger la seguridad y confidencialidad de la información obtenida.
                            </p>
                        </div>
                    </div>
                    <div class="preguntas-bloque">
                        <div class="preguntas-bloque-titulo">
                            <h3>COOKIES.</h3>
                            <div class="icono">
                                <span class="icon icon-desglozar"></span>
                            </div>
                        </div>
                        <div class="preguntas-bloque-respuesta">
                            <p>
                                La página web, los servicios online, los mensajes de correo electrónico y la publicidad de Vk Leather S.A. de C.V. pueden utilizar “cookies” y otras tecnologías, como son etiquetas de píxel y contador de visitas.
								<br>
								<br>
								Los cookies son pequeños grupos de datos (los”cookies”), almacenados por su buscador de internet en el disco duro de su unidad central de procesos (“CPU”). Al igual que miles de otros sitios web, utilizamos los cookies para mejorar su experiencia de visualizaciones y medir el uso de los sitios web. Con la ayuda de los cookies, podemos presentar ofertas personalizadas o contenidos de su interés. También utilizar los cookies para reconocerle en las siguientes visitas o recordar su nombre de usuarios para que no necesite insertarse cada vez que visita nuestro sitio. Puede configurar su buscador para que le notifique cuando recibe un cookie o impedir el envío de los
								mismos. Sin embargo debe tener en cuenta que al aceptar cookies puede limitar la funcionalidad que le ofrecemos al visitar el sitio.
								<br>
								<br>
								Toda la información recopilada mediante cookies y otras tecnologías es tratada como información general. Sin embargo, en la medida en que la legislación local considere a las direcciones IP (protocolo de Internet) o a identificadores similares como información personal, también lo haremos nosotros. De igual manera en la medida en que la información general se combine con datos personales, trataremos a la información así combinada como información personal a los efectos del presente aviso de privacidad. Como sucede con la mayoría de las páginas web, la información se recopila automáticamente y se almacena en archivos de registro. Esta información incluye direcciones IP, (Protocolo de Internet), tipo del navegador, proveedor de servicio de Internet (ISP), páginas de origen y destino, sistema operativo, hora y fecha de visita, y datos de navegación por la página.
								<br>
								<br>
								Esta información se utiliza para analizar tendencias, administrar la página, conocer la conducta del usuario en la página, y recopilar información de carácter demográfico acerca de nuestra base de usuarios en conjunto. Vk Leather S.A. de C.V. puede utilizar esta información en sus servicios de marketing y publicidad.
                            </p>
                        </div>
                    </div>
                    <div class="preguntas-bloque">
                        <div class="preguntas-bloque-titulo">
                            <h3>Comunicación por correo electrónico.</h3>
                            <div class="icono">
                                <span class="icon icon-desglozar"></span>
                            </div>
                        </div>
                        <div class="preguntas-bloque-respuesta">
                            <p>
                                Ocasionalmente, podemos enviarle correos electrónicos con información que podría resultar útil, inclusive información sobre nuestros productos o servicios u ofertas de otras compañías afiliadas o de terceros. Podrá optar por no recibir estos mensajes electrónicos si así lo desea.
                            </p>
                        </div>
                    </div>
                    <div class="preguntas-bloque">
                        <div class="preguntas-bloque-titulo">
                            <h3>Información que podemos revelar y destinatarios de la
								misma.</h3>
                            <div class="icono">
                                <span class="icon icon-desglozar"></span>
                            </div>
                        </div>
                        <div class="preguntas-bloque-respuesta">
                            <p>
                                Podemos revelar información personal recopilada a terceros como son, bancos y prestadores de servicios relacionados con los servicios contratados por usted.
								<br>
								<br>
								En todos los casos, requerimos a los proveedores de servicios a quienes revelamos información personal que cumplan con nuestro Aviso de Privacidad y utilicen la información solamente para los fines que han sido contratados. Revelamos información personal a estos proveedores con el fin de brindarle un mejor servicio.
                            </p>
                        </div>
                    </div>
                    <div class="preguntas-bloque">
                        <div class="preguntas-bloque-titulo">
                            <h3>Revelación de otra información permitida por la ley.</h3>
                            <div class="icono">
                                <span class="icon icon-desglozar"></span>
                            </div>
                        </div>
                        <div class="preguntas-bloque-respuesta">
                            <p>
                                Es posible que Vk Leather S.A. de C.V. deba divulgar tu información personal en virtud de una ley judicial, administrativo, acción legal, y/o solicitados por autoridades dentro y fuera del país de residencia. También es posible que Vk Leather S.A. de C.V. divulgue la información si considera que, por razones de seguridad nacional, cumplimiento de las leyes, u otras cuestiones de importancia nacional, dicha divulgación es necesaria o conveniente.
                            </p>
                        </div>
                    </div>
                    <div class="preguntas-bloque">
                        <div class="preguntas-bloque-titulo">
                            <h3>Prohibición de revelar información para otros fines.</h3>
                            <div class="icono">
                                <span class="icon icon-desglozar"></span>
                            </div>
                        </div>
                        <div class="preguntas-bloque-respuesta">
                            <p>
                                No compartiremos suinformación personal con compañías externas para fines que no sean los descritos anteriormente. Por ejemplo, no venderemos información personal a compañías externas que deseen ofrecer sus productos o servicios. Podemos ofrecerle productos o servicios en representación de otras compañías, pero no revelarles sus datos personales.
                            </p>
                        </div>
                    </div>
					<div class="preguntas-bloque">
                        <div class="preguntas-bloque-titulo">
                            <h3>Integridad y Conservación de Información Personal.</h3>
                            <div class="icono">
                                <span class="icon icon-desglozar"></span>
                            </div>
                        </div>
                        <div class="preguntas-bloque-respuesta">
                            <p>
                                Vk Leather S.A. de C.V. le garantiza la conservación de información personal de una manera precisa, completa y actualizada. La información personal se conservará por el periodo que sea necesario para cumplir con los fines establecidos en el presente Aviso de Privacidad, excepto que por ley sea requerida o autorice un período de conservación superior.
                            </p>
                        </div>
                    </div>
					<div class="preguntas-bloque">
                        <div class="preguntas-bloque-titulo">
                            <h3>Cambios al presente Aviso de Privacidad.</h3>
                            <div class="icono">
                                <span class="icon icon-desglozar"></span>
                            </div>
                        </div>
                        <div class="preguntas-bloque-respuesta">
                            <p>
								Las modificaciones al presente aviso estarán a su disposición en la página https://myvkshoes.com/ Recomentrados verificar este Archivo de Privacidad en forma periódica para mantenerse informado de cualquier cambio. Aunque nos reservamos el derecho a modificar o complementar este aviso de privacidad, cualquier cambio significativo será informado en este sitio Web durante al menos 30, treinta días, posteriores al mismo.
								<br>
								<br>
								Nos comprometemos a proteger la información personal y utilizar o compartir la misma solo para cumplir las obligaciones derivadas de la adquisición de algún producto o servicio, o para mejorar o aumentar los productos y servicios que ofrecemos. Al no oponerse activamente a los términos de este Aviso de Privacidad presta su consentimiento a la distribución personal a los informes de consumo según lo descrito anteriormente.
								<br>
								<br>
								Las prácticas y políticas incluidas en este Arvizu de privacidad reemplazarán las notificaciones o manifestaciones anteriores al respecto.
								<br>
								Este Aviso de Privacidad se aplica solamente a las prácticas de recopilación, protección utilización y transferencia de información revelada a Vk Leather S.A. de C.V. para los fines mencionados.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script type="module">
    $('.preguntas-bloque-titulo').on('click', function(){
        $(this).find('.icon').toggleClass('icon-flecha')
        $(this).next().slideToggle()
    })
</script>
@endsection
