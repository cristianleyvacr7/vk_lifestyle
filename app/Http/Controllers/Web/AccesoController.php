<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
// use Illuminate\Http\Response;
// use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
// use KlaviyoAPI\KlaviyoAPI;
use Cookie;

class AccesoController extends Controller
{
	protected $klaviyo;

    // public function __construct(Klaviyo $klaviyo)
    // {
    //     $this->klaviyo = $klaviyo;
    // }
    public function login(Request $request)
	{
		if ($request->isMethod('get')) {
			return view('acceso.login');
		}

        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        if($validator->fails())
        {
            $alert = array(
                'estatus' => 'error',
                'mensaje' => 'Los campos son obligatorios'
            );
            return redirect()->route('login')
            ->with('alert', $alert)
            ->withInput();
        }
        $email = $request->input('email');
        $password = \Crypt::encryptString($request->input('password'));

        $credentials = array('email' => $email, 'password' => $password);

        $http = new \GuzzleHttp\Client;
        $url_pv = url(env('URL_PV').'api/vk-lifestyle/');

        $response = $http->request('POST', $url_pv . "login/" . env('VK_ID'), ['json' => $credentials]);
        $login_response = json_decode($response->getBody(), true);

        if ($login_response && $login_response['estatus'] == 'success') {
            Cookie::queue(Cookie::forever('session_token', $login_response['token']));
			// $this->klaviyo->identify([
			// 	'email' => $email,
			// ]);
            return redirect()->route('perfil');
        } else if ($login_response['estatus'] == 'error') {
            $alert = array(
                'mensaje' => $login_response['mensaje'],
                'estatus' => 'error',
            );
            return redirect()->back()
            ->with('alert', $alert)
            ->withInput();
        }
	}

	public function salir()
	{
        Cookie::queue('session_token', null, -1);
        Cookie::queue('laravel_session', null, -1);
		return redirect()->route('home');
	}

    public function signin(Request $request)
    {
		if ($request->isMethod('get')) {
			return view('acceso.signin');
		}

        try {
            $http = new \GuzzleHttp\Client;
            $url_pv = url(env('URL_PV').'api/vk-lifestyle/');

            if ($request->input('password') !== $request->input('confirm_paswsord')) {
                $data = array(
                    'titulo' => '',
                    'mensaje' => 'Las contraseñas no coinciden',
                    'estatus' => 'error'
                );
                return redirect()->back()->with('alert', $data)->withInput();
            }

			$data = $request->except(['password', 'confirm_paswsord']);
            $data['password'] = \Crypt::encryptString($request->input('password'));
			\Log::info($data);
            $response = $http->request('POST', $url_pv . "registro/" . env('VK_ID'), ['json' => $data]);
            $response = json_decode($response->getBody(), true);
            if (isset($response['estatus'])) {
                $data = array(
                    'titulo' => '',
                    'mensaje' => $response['mensaje'],
                    'estatus' => $response['estatus']
                );
                if(isset($response['data'])){
                    $data['mensaje'] = reset($response['data'])[0];
                }
                return redirect()->back()->with('alert', $data)->withInput();
            }

            Cookie::queue(Cookie::forever('session_token', $response['token']));
			// Identificar usuario en Klaviyo
			// $this->klaviyo->identify([
			// 	'email' => $request->input('email'),
			// 	'properties' => [
			// 		'$first_name' => $request->input('nombre'),
			// 		'$last_name' => $request->input('apellidos'),
			// 		'$phone_number' => $request->input('telefono'),
			// 	],
			// ]);
            return redirect()->route('perfil');
        } catch (\Exception $e) {
            \Log::info($e);
            $data = array(
                'titulo' => '',
                'mensaje' => 'Ocurrió un error al guardar el registro, vuelve a intentarlo en unos momentos',
                'estatus' => 'error'
            );
            return redirect()->back()->with('alert', $data)->withInput();
        }
    }

    // public function update_user(Request $request)
    // {
    //     try {
    //         $http = new \GuzzleHttp\Client;
    //         $url_pv = url(env('URL_PV'));

    //         $data = [
    //             'email' => $request->input('usuario'),
    //             'password' => \Crypt::encryptString($request->input('contrasenia')),
    //             'session_token' => $request->cookie('session_token')
    //         ];

    //         $response = $http->request('POST', $url_pv . "/api/rizee-ecommerce/update-user", ['json' => $data]);
    //         $response = json_decode($response->getBody(), true);
    //         if (isset($response['estatus'])) {
    //             $data = array(
    //                 'titulo' => '',
    //                 'mensaje' => $response['mensaje'],
    //                 'estatus' => $response['estatus']
    //             );
    //             if(isset($response['data'])){
    //                 $data['mensaje'] = reset($response['data'])[0];
    //             }
    //             return redirect()->route('perfil')->with('alert', $data)->withInput();
    //         }
    //         Cookie::queue(Cookie::forever('session_token', $response['token']));
    //         $data = array(
    //             'titulo' => '',
    //             'mensaje' => 'Información actualizada',
    //             'estatus' => 'success'
    //         );
    //         return redirect()->route('perfil')->with('alert', $data);
    //     } catch (\Exception $e) {
    //         \Log::info($e);
    //         $data = array(
    //             'titulo' => '',
    //             'mensaje' => 'Ocurrió un error al actualizar, vuelve a intentarlo en unos momentos',
    //             'estatus' => 'error'
    //         );
    //         return redirect()->back()->with('alert', $data)->withInput();
    //     }
    // }
}
