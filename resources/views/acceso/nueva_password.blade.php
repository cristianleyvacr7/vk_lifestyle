@extends('layout')
@section('head')
	@vite(['resources/stylus/acceso.styl'])
    <title>NUEVA CONTRASEÑA - VK LIFESTYLE</title>
	<link rel="stylesheet" href="{{asset('plugins/validationEngine/validationEngine.jquery.css')}}">

@endsection
@section('contenido')
    <div class="nueva">
        <div class="nueva-contenido">
            <h1 class="title">NUEVA CONTRASEÑA</h1>
            <p>
                ¡Casi listo! Por favor, elige una contraseña segura para tu cuenta.
                Asegúrate de utilizar una combinación de letras, números y caracteres
                especiales para garantizar la seguridad de tu cuenta.
            </p>
            <div class="nueva-contenido-form">
                <form id="formNueva" action="{{ route('nueva_contrasena') }}" method="POST">
                    @csrf
                    <input type="hidden" name="correo" value="{{ request()->input('email') }}">
                    <input type="hidden" name="token" value="{{ request()->input('token') }}">
                    <div class="input text">
                        <label for="nueva">Nueva contraseña</label>
                        <input type="password" id="password2" name="nueva_contrasenia"  data-validation-engine="validate[required,minSize[6]]">
                    </div>
                    <div class="input text">
                        <label for="confirmar">Confirmar contraseña</label>
                        <input type="password"  name="confirmar_contrasenia"  data-validation-engine="validate[required,equals[password2]]">
                    </div>
                    <div class="boton">
                        <button>INICIAR SESIÓN</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script src="{{asset('/plugins/validationEngine/jquery.validationEngine.js')}}"></script>
	<script src="{{asset('/plugins/validationEngine/jquery.validationEngine-es.js')}}"></script>
    <script>
         $(function(){
            $('#formNueva').validationEngine({
                scroll: false
            });
        });
    </script>
@endsection
