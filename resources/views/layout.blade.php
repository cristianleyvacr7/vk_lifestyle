<!DOCTYPE html>
<html class="js" lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
	<link rel="manifest" href="/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<!-- Google tag (gtag.js) -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-KXX7Q2QCHG"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'G-KXX7Q2QCHG');
	</script>

	<!-- Meta Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window, document,'script','https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '419164004404931');
	fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=419164004404931&ev=PageView&noscript=1"/></noscript>
	<!-- End Meta Pixel Code -->

	<link rel="stylesheet" href="{{asset('plugins/search-pretty/css/demo.css')}}">
	<link rel="stylesheet" href="{{asset('plugins/search-pretty/css/style6.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('/plugins/slick/slick.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('/plugins/slick/slick-theme.css')}}">
	{!! htmlScriptTagJsApi() !!}
	{{-- <script type="text/javascript" async=""
	src="https://static.klaviyo.com/onsite/js/klaviyo.js?company_id=PUBLIC_API_KEY"></script> --}}

	<!-- opengraph-->
	<meta property="og:type" content="website"/>
    <meta property="og:url" content="{{ url()->current() }}"/>
	<meta property="og:description" content="Descubre el lujo del zapato de piel para dama hecho de forma artesanal en México. Nuestra colección de botas y sandalias ofrece diseños exclusivos, elaborados a mano con materiales de alta calidad. Perfectos para quienes valoran la tradición y el estilo único. Compra ahora y disfruta de la elegancia y comodidad del auténtico calzado mexicano."/>

    @yield('head')
</head>
<body>
	<svg class="hidden">
		<defs>
			<symbol id="icon-arrow" viewBox="0 0 24 24">
				<title>arrow</title>
				<polygon points="6.3,12.8 20.9,12.8 20.9,11.2 6.3,11.2 10.2,7.2 9,6 3.1,12 9,18 10.2,16.8 "/>
			</symbol>
			<symbol id="icon-cross" viewBox="0 0 24 24">
				<title>cross</title>
				<path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/>
			</symbol>
		</defs>
	</svg>
	<div id="app" class="main-wrap">
		<div class="spinner">
			<div id="loading"></div>
		</div>
		<header class="header">
			<div class="header-leyenda">
				<div class="header-leyenda-item">
					<p>Envío gratis a todo el país 🇲🇽</p>
				</div>
				<div class="header-leyenda-item">
					<p>Envíos internacionales</p>
				</div>
			</div>
			<div class="header-container">
				<figure>
					<a href="{{ route('home') }}">
						<img src="{{asset('/images/logo.svg')}}" alt="VK_LIFESTYLE">
					</a>
				</figure>
				<nav class="menu">
					<ul>
						<li  class="">
							<a href="{{route('catalogo', 'botas')}}">Botas</a>
						</li>
						<li class="">
							<a href="{{route('catalogo', 'sandalias')}}">Sandalias</a>
						</li>
					</ul>
				</nav>
				<div class="iconos">
					<ul>
						<li class="iconos-bag">
							<a id="carrito">
								<span class="icon icon-bag"></span>
								<span class="bag-count">@{{cantidad_productos == 0 ? '' : cantidad_productos}}</span>
							</a>
						</li>
						<li class="iconos-user">
							@if(isset($_COOKIE['session_token']))
								<a href="{{route('perfil')}}" class="icon icon-user"></a>
							@else
								<a href="{{route('login')}}" class="icon icon-user"></a>
							@endif
						</li>
						<li>
							<span id="btn-search" class="btn_search btn--search icon-buscar"></span>
						</li>
						<li>
							<span id="btnMenu" class="icon icon-menu"></span>
						</li>
					</ul>
				</div>
			</div>
		</header>
		<div class="carrito">
			<div class="carrito-contenido">
				<div class="carrito-contenido-titulo">
					<h2 class="titulo">CARRITO <span>@{{cantidad_productos == 0 ? '' : `(${cantidad_productos})`}}</span></h2>
					<span class="cerrar icon icon-cerrar"></span>
				</div>
				<div class="carrito-contenido-tabla">
					<div class="carrito-contenido-tabla-item" v-for="producto in venta?.productos">
						<div class="seccion1">
							<div class="seccion1-imagen">
								<template v-if="producto?.producto?.producto_imagenes?.length > 0">
									<img :src="'{{ env('URL_CDN_PV') }}images/productos/'+producto?.producto?.producto_imagenes[0]?.imagen" alt="VK_LIFESTYLE">
								</template>
								<template v-else>
									<img src="/images/default.jpg" alt="VK_LIFESTYLE">
								</template>
							</div>
							<div class="seccion1-texto">
								<div class="seccion1-texto-info">
									<h3>@{{ producto?.producto?.nombre }} - @{{ producto?.producto?.nombre_variante }}</h3>
									<p v-if="producto?.precio_unitario != producto?.precio_venta" style="text-decoration: line-through">@{{ producto?.precio_unitario | currency }} MXN</p>
									<p>@{{ producto?.precio_venta | currency }} MXN</p>
								</div>
								<div class="seccion1-texto-input">
									<div class="boton">
										<span class="icon icon-menos" @click="updateQuantity({venta_producto: producto, accion: '-', cantidad_mod: 1})"></span>
										<input type="text" v-model="producto.cantidad_modificada" @change="updateQuantity({venta_producto: producto})">
										<span class="icon icon-mas" @click="updateQuantity({venta_producto: producto, accion: '+', cantidad_mod: 1})"></span>
									</div>
								</div>
							</div>
						</div>
						<div class="seccion">
							<button class="ver-mas" @click="removeFromCart({venta_producto_id: producto?.id})">Eliminar</button>
						</div>
					</div>
				</div>
				<div class="carrito-contenido-subtotal">
					<div class="carrito-contenido-subtotal-contenido">
						<div class="titulo">
							<h3>SUBTOTAL</h3>
						</div>
						<div class="precio">
							<p>@{{ venta?.subtotal | currency }} MXN</p>
						</div>
					</div>
					<div v-if="venta?.descuento_3x2 > 0">
						<div class="carrito-contenido-subtotal-contenido">
							<div class="titulo">
								<h3>PROMOCIÓN 3x2</h3>
							</div>
							<div class="precio">
								<p>-@{{ venta?.descuento_3x2 | currency }} MXN</p>
							</div>
						</div>
						<div class="carrito-contenido-subtotal-contenido">
							<div class="titulo">
								<h3>TOTAL</h3>
							</div>
							<div class="precio">
								<p>@{{ venta?.total | currency }} MXN</p>
							</div>
						</div>
					</div>
					<div class="carrito-contenido-subtotal-boton" v-if="cantidad_productos>0">
						<a href="{{route('datos_envio')}}">CONFIRMAR COMPRA</a>
					</div>
				</div>
			</div>
		</div>
		<div class="whatsapp">
			<a href="https://wa.me/+524775807353" target="_blank">
				<span class="icon icon-whatsapp"></span>
				<span>CHAT</span>
			</a>
		</div>
		@yield('contenido')
		<footer class="footer">
			<div class="footer-contenido">
				<div class="footer-contenido-seccion center d-none">
					<div class="footer-contenido-seccion-icon">
						<img src="/images/home/icono_negro.png" alt="VK_LIFESTYLE">
					</div>
				</div>
				<div class="footer-contenido-seccion">
					<p>477 580 7353</p>
					<p>contacto@myvkshoes.com</p>
					<div class="redes">
						<a href="https://www.facebook.com/leathervklifestyle" target="_blank" class="icon icon-facebook"></a>
						<a href="https://www.instagram.com/leathervklifestyle/" target="_blank" class="icon icon-instagram"></a>
						{{-- <span class="icon icon-tiktok"></span> --}}
					</div>
				</div>
				<div class="footer-contenido-seccion center">
					<div class="footer-contenido-seccion-icon">
						<img class="visible" src="/images/home/icono_negro.png" alt="VK_LIFESTYLE">
					</div>
					{{-- <form action="{{route('newsletter')}}" method="post" id="form-newsletter">
						@csrf
						<div class="footer-contenido-seccion-info">
							<p>Suscribirse</p>
						</div>
						<div class="footer-contenido-seccion-input">
							<div class="input">
								<input type="text" placeholder="Escribe tu Email" name="email_footer" data-validation-engine="validate[required]">
								<span class="icon icon-flechita"></span>
							</div>
							<br>
							<div class="recaptcha">
								{!!htmlFormSnippet()!!}
							</div>
							<br>
							<div class="boton">
								<button>Suscribirse</button>
							</div>
						</div>
					</form> --}}
				</div>
				<div class="footer-contenido-seccion derecha">
					<a href="{{route('privacidad')}}">Políticas de Privacidad</a>
					<a href="{{route('cambios')}}">Envíos, cambios y devoluciones</a>
					<a href="{{route('terminos')}}">Términos y condiciones</a>
					<div class="tarjeta">
						<img src="/images/home/visa.png" alt="VK_LIFESTYLE">
						<img src="/images/home/mastercard.png" alt="VK_LIFESTYLE">
						{{-- <img src="/images/home/oxxo.png" alt="VK_LIFESTYLE">
						<img src="/images/home/paypal.png" alt="VK_LIFESTYLE"> --}}
					</div>
				</div>
			</div>
			<div class="footer-corvuz">
				<p>Hecho por:</p>
				<a href="https://www.corvuz.com/" target="_blank">
					<img src="/images/logo_corvuz.png" alt="VK_LIFESTYLE">
				</a>
			</div>
		</footer>
		<div class="search">
			<form class="search__form">
				<input id="search-input" class="search__input" name="q" type="text" placeholder="Buscar . . ." autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
			</form>
		</div>
		<button id="btn-search-close" class="btn_close btn--hidden btn--search-close" aria-label="Close search form"><svg class="icon icon_search icon--cross"><use xlink:href="#icon-cross"></use></svg></button>
	</div>
	@vite(['resources/js/app.js'])
	<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
	<script src="{{asset('/plugins/slick/slick.min.js') }}"></script>
    <script src="{{asset('/plugins/spin.min.js')}}"></script>
	<script src="{{asset('plugins/search-pretty/js/demo6.js')}}" type="module"></script>
	<script src="{{ asset('/plugins/typeahead.js') }}"></script>
	<script type="module">
		const app = document.getElementById('app');
		$('#btnMenu').on('click', function(){
			// Toggle class for '.menu'
			$('.menu').toggleClass('active');
			let flag = false;
			// Toggle class for ".header"
			if ($(".header").hasClass("fixed")) {
				$(".header").addClass("fixed");
			} else {
				$(".header").addClass("fixed");
			}
		});
		$(window).scroll(function() {
            var scroll = $(window).scrollTop();

            //>=, not <=
            if (scroll >= 100) {
                //clearHeader, not clearheader - caps H
                $(".header").addClass("fixed");
				$(".header-container ul li").addClass("fixed");
                $(".header-container").addClass("fixed");

            }else{
                $(".header").removeClass("fixed");
				$(".header-container ul li").removeClass("fixed");
                $(".header-container").removeClass("fixed");
            }
        });
		$('#carrito').on('click', function(){
			$('.carrito').addClass('selected');
        })
		$('.cerrar').on('click', function(){
			$('.carrito').removeClass('selected');
        })
		$('.productos-item').on('mouseenter',function() {
			var product = $(this)
			app.__vue__.producto_variante_id_selected = null;
			$('input[type="radio"]:checked').prop('checked', false);
			product.find('.icon-cerrar').on('click', function(){
				product.find('.compra-rapida').addClass('hide')
			})
		}).on('mouseleave', function(){
			var product = $(this)
			product.find('.compra-rapida').removeClass('hide');
		});

		</script>
	{{-- Funcion de buscador --}}
	<script type="module">
		var engine = new Bloodhound({
			remote: {
				url: '{{ route('search') }}?search=%QUERY%',
				wildcard: '%QUERY%'
			},
			datumTokenizer: Bloodhound.tokenizers.whitespace('q'),
			queryTokenizer: Bloodhound.tokenizers.whitespace
		});
		$('#search-input').typeahead({
			hint: true,
			highlight: true,
			minLength: 1
		},{
			source: engine.ttAdapter(),

			// This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
			name: 'searchList',

			display: 'nombre',

			// the key from the array we want to display (name,id,email,etc...)
			templates: {
				empty: [
					'<div class="search__related"><div class="list-group-item">Nothing found.</div></div>'
				],
				suggestion: function (data) {
					var url = "/producto/";
					url = url + data.slug;
					if(data.producto_imagenes.length > 0){
						var imagenSrc = data.producto_imagenes[1].imagen_url;
					}else{
						var imagenSrc = data.image_default;
					}
					return '<div class="search__suggestion"><div class="productos-item"><a href="'+url+'" class="search__suggestion"><div class="productos-item-imagen"><img src="'+imagenSrc+'" alt="VK-Lifestyle"/></div><div class="productos-item-precio"><span class="txt-black">' + data.nombre+'</span></a></div></div>'
				}
			}
		});
	</script>
	<script type="module">
		$(function(){
			// $('#form-newsletter').validationEngine({
			// 	scroll: false
			// });
			$('.header-leyenda').slick({
				autoplay: true,
				fade: true,
				infinite: true,
				dots: false,
				arrows: false
			})
		});
        $(function(){
            @if(Session::get('alert') != null)
                setTimeout(() => {
                    alert("{{Session::get('alert')['mensaje']}}");
                }, 1);
            @endif
			if(window.location.href.includes('?carrito')){
                $('.carrito').addClass('selected');
            }
        })
		var opts = {
			lines: 8 // The number of lines to draw
			, length: 18 // The length of each line
			, width: 5 // The line thickness
			, radius: 20 // The radius of the inner circle
			, scale: 1 // Scales overall size of the spinner
			, corners: 1 // Corner roundness (0..1)
			, color: '#FFFFFF' // #rgb or #rrggbb or array of colors
			, opacity: 0.25 // Opacity of the lines
			, rotate: 0 // The rotation offset
			, direction: 1 // 1: clockwise, -1: counterclockwise
			, speed: 1 // Rounds per second
			, trail: 60 // Afterglow percentage
			, fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
			, zIndex: 2e9 // The z-index (defaults to 2000000000)
			, className: 'spinner' // The CSS class to assign to the spinner
			, top: '50%' // Top position relative to parent
			, left: '50%' // Left position relative to parent
			, shadow: true // Whether to render a shadow
			, hwaccel: false // Whether to use hardware acceleration
			, position: 'absolute' // Element positioning
		};

		var target = document.getElementById('loading');
		var spinner = new Spinner(opts).spin(target);
    </script>
    @yield('js')
</body>
</html>
