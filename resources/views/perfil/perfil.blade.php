@extends('layout')
@section('head')
    <title>PERFIL -VK LIFESTYLE</title>
	@vite(['resources/stylus/perfil.styl'])
	<link rel="stylesheet" href="{{asset('plugins/validationEngine/validationEngine.jquery.css')}}">

@endsection
@section('contenido')
	<div class="cuenta">
		<div class="cuenta-contenido">
			<a href="{{route('logout')}}" class="cerrar-sesion">
				<p>Cerrar sesión</p>
				<span class="icon icon-flechita"></span>
			</a>
			<div class="cuenta-contenido-titulo">
				<a class="selected" href="{{route('perfil')}}">MI PERFIL</a>
				<a href="{{route('mis_compras')}}">MIS COMPRAS</a>
			</div>
			<div class="perfil">
				<div class="perfil-datos">
					<form id="formPerfil" action="{{route('perfil')}}" method="POST">
						@csrf
						<div class="input text">
							<label for="nombre">Nombre</label>
							<input  autocomplete="off" type="text" name="nombre" data-validation-engine="validate[required]" value="{{ $cliente->nombre ? explode('  ', $cliente->nombre)[0] : '' }}">
						</div>
						<div class="input text">
							<label for="apellidos">Apellidos</label>
							<input  autocomplete="off" type="text" name="apellidos" value="{{ $cliente->nombre ? (count(explode('  ', $cliente->nombre))>1 ? explode('  ', $cliente->nombre)[1] : '') : '' }}">
						</div>
						<div class="input text">
							<label for="correo">Correo</label>
							<input  autocomplete="off" type="text" name="email" data-validation-engine="validate[required,custom[email]]" value="{{ $cliente->email }}">
						</div>
						<div class="input check">
							<label for="pass">Actualizar contraseña</label>
							<div class="checkbox">
								<input  autocomplete="off" type="checkbox" name="pass">
								<span class="sq"></span>
							</div>
						</div>
						<div class="actualizar">
							<div class="input text">
								<label for="password">Contraseña</label>
								<input  autocomplete="off" type="password" name="password" id="password" data-validation-engine="validate[required,minSize[6]]">
							</div>
							<div class="input text">
								<label for="confirmar">Confirmar contraseña</label>
								<input  autocomplete="off" type="password" name="confirmar" data-validation-engine="validate[required,equals[password]]">
							</div>
						</div>
						<div class="boton">
							<button>GUARDAR</button>
						</div>
					</form>
				</div>
			</div>

		</div>
	</div>
@endsection
@section('js')
<script src="{{asset('/plugins/validationEngine/jquery.validationEngine.js')}}"></script>
	<script src="{{asset('/plugins/validationEngine/jquery.validationEngine-es.js')}}"></script>
	<script>
		$(document).ready(function() {
			$('input[name="pass"]').change(function() {
				if ($(this).is(':checked')) {
					$('.actualizar').addClass('active');
				} else {
					$('.actualizar').removeClass('active');
				}
			});
		});
		$(function(){
			$('#formPerfil').validationEngine({
				scroll: false
			});
		});
	</script>
@endsection
