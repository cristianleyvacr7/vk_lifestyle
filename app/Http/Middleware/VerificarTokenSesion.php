<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Symfony\Component\HttpFoundation\Response;

use Cookie;

class VerificarTokenSesion
{
    private $cliente;
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if (!$request->cookie('session_token') || !$this->esTokenValido($request->cookie('session_token'))) {
            $data = array(
                'titulo' => '',
                'mensaje' => 'No hay ninguna sesión iniciada',
                'estatus' => 'error'
            );
            return redirect()->route('login')->with('alert', $data);
        }
        View::share('cliente', (object) $this->cliente);
        return $next($request);
    }

    private function esTokenValido($token)
    {
        try{
            $valido = false;
            $http = new \GuzzleHttp\Client;
            $url_pv = url(env('URL_PV').'api/vk-lifestyle/');
            $response = $http->request('GET', $url_pv . 'user', ['headers' => ['Authorization' => 'Bearer '.$token]]);
            $response = json_decode($response->getBody(), true);
            if(isset($response['id'])){
                $valido = true;
                $this->cliente = $response;
            }
        }catch(\Exception $e){
            \Log::info($e);
        }

        if(!$valido){
            Cookie::queue('session_token', null, -1);
        }

        return $valido;
    }
}
