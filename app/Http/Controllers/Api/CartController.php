<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
// use KlaviyoAPI\KlaviyoAPI;

class CartController extends Controller
{
    public function index(Request $request)
    {
        try{
            $http = new \GuzzleHttp\Client;
            $url_pv = url(env('URL_PV').'api/vk-lifestyle/');
            $session_token = request()->session()->get('_token');
            $data = [
                'session_token' => request()->session()->get('_token'),
            ];
            if($request->cookie('session_token')){
                $data['auth_token'] = $request->cookie('session_token');
            }
            $data = $http->request('GET', $url_pv.'get-cart/'.env('VK_ID'), [ 'query' => $data ]);
            $data = json_decode($data->getBody(), true);

            return response()->json($data);
        }catch(\Exception $e){
			\Log::info($e);
            $data = array(
                'msg' => 'Ocurrió un error al cargar el carrito, vuelve a cargar la pagina en unos momentos',
                'error' => $e->getMessage(),
                'line' => $e->getLine()
            );
            return response()->json($data, 500);
        }
    }

    public function add(Request $request)
    {
        try{
            $rules =  array(
				'variante_id' => 'required',
                'cantidad' => 'nullable',
				'codigo_pais' => 'required',
				'moneda' => 'required'
            );

            $validator = Validator::make($request->all(), $rules);

            if($validator->fails()) {
                return array(
					'message' => 'Ocurrión un error, intenta nuevamente',
					'error' => $validator->errors());
            }

            $http = new \GuzzleHttp\Client;
            $url_pv = url(env('URL_PV').'api/vk-lifestyle/');

            $data = [
                'session_token' => request()->session()->get('_token'),
                'variante_id' => $request->input('variante_id'),
                'cantidad' => $request->input('cantidad') ?? 1,
				'codigo_pais' => $request->input('codigo_pais') ?? 'MX',
				'moneda' => $request->input('moneda') ?? 'MXN',
				'facebook_ads_token' => env('FACEBOOK_ADS_TOKEN'),
				'pixel_id' => env('PIXEL_ID'),
                'remote_addr' => $_SERVER['REMOTE_ADDR'],
                'http_user_agent' => $_SERVER['HTTP_USER_AGENT']
            ];
            if($request->cookie('session_token')){
                $data['auth_token'] = $request->cookie('session_token');
            }
            $data = $http->request('GET', $url_pv.'add-to-cart/'.env('VK_ID'), [ 'query' => $data ]);
            $data = json_decode($data->getBody(), true);

        	return response()->json($data);

        }catch(\Exception $e){
			\Log::info($e);
            $data = array(
                'msg' => 'Ocurrión un error, intenta nuevamente',
                'error' => $e->getMessage(),
                'line' => $e->getLine()
                );
            return response()->json($data, 500);
        }
    }

    public function update(Request $request)
    {
        try{
            $rules =  array(
                'venta_producto_id' => 'required',
				'accion' => 'required',
                'cantidad' => 'nullable'
            );

            $validator = Validator::make($request->all(), $rules);

            if($validator->fails()) {
                return array(
					'message' => 'Ocurrión un error, intenta nuevamente',
					'error' => $validator->errors());
            }

            $http = new \GuzzleHttp\Client;
            $url_pv = url(env('URL_PV').'api/vk-lifestyle/');

            $data = [
                'venta_producto_id' => $request->input('venta_producto_id'),
                'accion' => $request->input('accion'),
                'cantidad' => $request->input('cantidad', 1),
                'session_token' => request()->session()->get('_token'),
            ];
            if($request->cookie('session_token')){
                $data['auth_token'] = $request->cookie('session_token');
            }
            $data = $http->request('GET', $url_pv.'update-cart/'.env('VK_ID'), [ 'query' => $data ]);
            $data = json_decode($data->getBody(), true);

        	return response()->json($data);

		}catch(\Exception $e){
			\Log::info($e);
			$data = array(
				'msg' => 'Ocurrión un error, intenta nuevamente',
				'error' => $e->getMessage(),
				'line' => $e->getLine()
				);
			return response()->json($data, 500);
		}
    }

    public function delete(Request $request, int $venta_producto_id)
    {
        try {
			$http = new \GuzzleHttp\Client;
            $url_pv = url(env('URL_PV').'api/vk-lifestyle/');

            $data = [
                'venta_producto_id' => $venta_producto_id,
                'session_token' => request()->session()->get('_token'),
            ];
            if($request->cookie('session_token')){
                $data['auth_token'] = $request->cookie('session_token');
            }
            $data = $http->request('GET', $url_pv.'delete-cart/'.env('VK_ID'), [ 'query' => $data ]);
            $data = json_decode($data->getBody(), true);

            return response()->json($data);

        } catch (\Exception $e) {
			\Log::info($e);
			$data = array(
				'msg' => 'Ocurrió un error, intenta nuevamente',
				'error' => $e->getMessage(),
				'line' => $e->getLine()
			);
            return response()->json($data);
        }
    }

	public function aplicar_cupon(Request $request, $codigo){
        try{
			$http = new \GuzzleHttp\Client;
            $url_pv = url(env('URL_PV').'api/vk-lifestyle/');

            $data = [
                'codigo' => $codigo,
                'session_token' => request()->session()->get('_token'),
            ];
            if($request->cookie('session_token')){
                $data['auth_token'] = $request->cookie('session_token');
            }
            $data = $http->request('GET', $url_pv.'aplicar-cupon/'.env('VK_ID'), [ 'query' => $data ]);
            $data = json_decode($data->getBody(), true);

			return response()->json($data);

        } catch (\Exception $e) {
			\Log::info($e);
			$data = array(
				'alert' => 'Ocurrió un error, intenta nuevamente',
				'error' => $e->getMessage(),
				'line' => $e->getLine()
			);
            return response()->json($data);
        }
    }

    public function eliminar_cupon(Request $request){
		try{
			$http = new \GuzzleHttp\Client;
            $url_pv = url(env('URL_PV').'api/vk-lifestyle/');

            $data = [
                'session_token' => request()->session()->get('_token'),
            ];
            if($request->cookie('session_token')){
                $data['auth_token'] = $request->cookie('session_token');
            }
            $data = $http->request('GET', $url_pv.'quitar-cupon/'.env('VK_ID'), [ 'query' => $data ]);
            $data = json_decode($data->getBody(), true);

			return response()->json($data);

        } catch (\Exception $e) {
			\Log::info($e);
			$data = array(
				'alert' => 'Ocurrió un error, intenta nuevamente',
				'error' => $e->getMessage(),
				'line' => $e->getLine()
			);
            return response()->json($data);
        }
    }
}
