import { defineConfig } from 'vite';
import laravel, { refreshPaths } from 'laravel-vite-plugin';
import vue from '@vitejs/plugin-vue2';
export default defineConfig({
    plugins: [
        laravel({
			input:[
			'resources/stylus/home.styl',
			'resources/stylus/detalle_producto.styl',
			'resources/stylus/catalogo.styl',
			'resources/stylus/compra.styl',
			'resources/stylus/gracias.styl',
			'resources/stylus/login.styl',
			'resources/stylus/perfil.styl',
			'resources/stylus/aviso.styl',
			'resources/stylus/acceso.styl',
			'resources/js/app.js'
			],
            refresh: [
                ...refreshPaths,
            ],
		}),
		vue({
            template: {
                transformAssetUrls: {
                    base: null,
                    includeAbsolute: false,
                }
            }
        })
    ],
    resolve: {
        alias: {
            vue: 'vue/dist/vue.esm.js',
        },
    },
});
