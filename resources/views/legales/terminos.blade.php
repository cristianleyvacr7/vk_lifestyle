@extends('layout')
@section('head')
	@vite(['resources/stylus/aviso.styl'])
    <title>TÉRMINOS Y CONDICIONES - VK LIFESTYLE</title>
@endsection
@section('contenido')
    <div class="aviso">
        <div class="aviso-contenido layout">
            <div class="aviso-contenido-menu">
                <div class="opciones">
                    <div class="opciones-opcion">
                        <a href="{{route('privacidad')}}" id="privacidad">AVISO DE PRIVACIDAD</a>
                    </div>
                    <div class="opciones-opcion">
                        <a href="{{route('cambios')}}" id="cambios">CAMBIOS Y DEVOLUCIONES</a>
                    </div>
                    <div class="opciones-opcion">
                        <a href="{{route('terminos')}}" id="terminos" class="selected">TÉRMINOS Y CONDICIONES</a>
                    </div>
                </div>
            </div>
            <div class="aviso-contenido-informacion">
                <div class="preguntas active">
                    <h1>Términos y condiciones</h1>
					<p>
						Vk Leather S.A. de C.V. en lo sucesivo la “Empresa” a través de su página de internet denominada <a href="https://myvkshoes.com/" style="text-decoration: underline">https://myvkshoes.com/</a> venderá a cualquier persona, quien será identificada como “Usuario” quien podrá adquirir todo tipo de bienes que se encuentren publicados.
						<br>
						<br>
						Página de Internet de la empresa: Es el sitio Web de Vk Leather S.A de C.V. bajo la denominación Vk Leather Lifestyle “myvkshoes.com” en el cual Vk Leather S,A de C.V. pública sus productos de venta, en donde se puede registrar el usuario para adquirir los productos publicados, realizar pedidos, ponerse en contacto con Vk Leather S.A. de C.V. o en un servicio a cliente a través del correo electrónico <span style="text-decoration: underline;">contacto@myvkshoes.com</span>, entre otros.
						<br>
						<br>
						Declaraciones: Vk Leather S.A. de C.V. declara que es una Empresa legalmente constituida bajo las leyes mexicanas, que tiene todos los permisos necesarios para cumplir su Objeto Social y que no tiene algún tipo de impedimento o limitante para cumplir con el mencionado Objeto, que incluye la celebración del contrato.
						<br>
						<br>
						Vk Leather S.A. de C.V. concede al Usuario una licencia personal limitada, no exclusiva, no transferible, por plazo indeterminado, conforme a los presentes Términos y Condiciones, para utilizar el Sitio, a fin de avalar, manifestar o interesarse en adquirir y mediante de ventas los productos de la página que se encuentran publicados. La forma de utilizar el Sitio es personal e intransferible.
						<br>
						<br>
						Vk Leather S.A. de C.V. declara ser propietario de los Bienes o Productos que se encuentran en la página de ventas, además de todos los elementos amparados por normas de propiedad intelectual. Todo lo anterior para efecto del Contrato.
						<br>
						<br>
						Vk Leather S.A. de C.V se reserva todos los derechos no expresamente otorgados bajo ningún documento. Este contrato y cualesquiera derechos de licencias otorgadas aquí, no podrán ser transferidos o cedidos por el Usuario, pero Vk Leather S.A de C.V. estará en posibilidad de transferirlos o cederlos sin restricción alguna.
						<br>
						<br>
						El usuario acepta Términos y Condiciones y se obliga a todo lo señalado en los mismos.
						<br>
						<br>
						El Usuario está de acuerdo en que las exclusiones de garantía y limitación es de responsabilidad establecidas con anterioridad son elementos fundamentales de la base de estos. Términos y Condiciones.
						El usuario está consciente de que el tráfico de datos que proporciona acceso al Sitio es apoyado por un servicio prestado por la operadora de servicios de telecomunicación seleccionada y contratada por el Usuario y dicha contratación es totalmente independiente del Sitio.
						<br>
						<br>
						El usuario reconoce que las comisiones cobradas por la operadora y de servicios de telecomunicaciones de su elección y los impuestos aplicables pueden afectar el tráfico de datos necesarios para eventuales descargas y anuncios de un tercero en el dispositivo.
						<br>
						El usuario declara y reconoce que el download de cualquier contenido del Sitio no le confiere la propiedad sobre cualesquiera marcas exhibidas en el Sitio.
						<br>
						Restricciones: Cualquier trasgresión a los presentes Términos y Condiciones por parte del Usuario generarán el derecho en favor de Vk Leather S.A. de C.V. en cualquier momento y sin necesidad de notificación previa de ningún tipo, suspender o terminar la presentación de los servicios y/o de retirar o denegar el acceso al Sitio al Usuario trasgresor, así como quitarle o sacarlo del Registro.
						<br>
						El sitio puede ser utilizado únicamente con fines lícitos.
						<br>
						Se encuentra terminantemente prohibido cualquier tipo de copia, distribución, transmisión, publicación, impresión, difusión, y/o explotación comercial del material y/o contenido a disposición del público a través de este Sitio, sin el previo consentimiento expreso y por escrito de Vk Leather S.A. de C.V. en su caso del titular de los derechos de propiedad correspondientes. El incumplimiento de lo mencionado sujetará al infractor a todos los reclamos civiles y sanciones penales que pudieran corresponder. Los padres o tutores de menores de edad serán responsables por los actos por ellos realizados según lo dispuesto por estos términos y condiciones, incluyendo los daños causados a terceros, acciones realizadas por ellos y que estén prohibidas por la ley y por las disposiciones de este acuerdo, sin perjuicio de la responsabilidad del usuario, siempre que éste no fuese padre o representante legal del menor infractor.
						<br>
						Al momento del Registro de Usuario no se aceptarán y podrán ser cancelados en cualquier momento, direcciones de correo electrónico (e-mails) o cualquier dato que contengan expresiones o conjuntos gráficos-denominativos que hayan sido escogidos anteriormente por otro Usuario, que de alguna otra forma, fuesen injuriosos altisonantes, coincidentes con marcas, nombres comerciales, anuncios de establecimientos, razones sociales de empresas, expresiones publicitarias, nombres y seudónimos de personas de relevancia pública, famosos o registrados por terceros, cuyo uso no esté autorizado o que fuese en general, contrario a la ley o a las exigencias morales y de buenas costumbres generalmente aceptadas, así como expresiones que pudieran indicar a otras personas por error, quedando claro que el Usuario responderá por el uso indebido tanto en el ámbito civil como penal, si aplicase. El Usuario no deberá hacer Upload, publicar o de cualquier otra forma disponer en el Sitio de cualquier material protegido por derechos del autor, registro de marcas o cualquier otro derecho de propiedad intelectual sin previa y expresa autorización del titular del mencionado derecho.
						<br>
						LOS PRODUCTOS ESTÁN SUJETOS A DISPONIBILIDAD EN ALMACÉN.
						<br>
						<br>
					</p>
					<h2>TÉRMINO DEL CONTRATO</h2>
					<p>
						El contrato termina en el momento que las partes cumplen con sus obligaciones. Vk Leather S,A de C.V podrá terminar anticipadamente el contrato si:
						<br>
						<br>
						● El Precio no es pagado puntualmente, a cuyo efecto las partes pactan expresamente que la falta de pago total o parcial producirá la rescisión de pleno derecho.
						<br>
						● El usuario incumple con cualquiera de las obligaciones asumidas en este documento. Vk Leather S.A de C.V. podrá optar por exigir la ejecución de su obligación de pagar el precio o rescindir el Contrato y la indemnización, además podrá exigir resarcimiento de daños y perjuicios.
						<br>
						<br>
						Vigencia de los Términos y Condiciones La empresa así como el Usuario, reconocen que los Términos y Condiciones son de vigencia ilimitada.
						<br>
						Puedes realizar el pago de tus compras en el portal <a href="https://myvkshoes.com/" style="text-decoration: underline">https://myvkshoes.com/</a>
					</p>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script>
    $('.preguntas-bloque-titulo').on('click', function(){
        $(this).find('.icon').toggleClass('icon-flecha')
        $(this).next().slideToggle()
    })
</script>
@endsection
