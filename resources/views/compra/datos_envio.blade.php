<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
	<link rel="manifest" href="/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="{{asset('plugins/validationEngine/validationEngine.jquery.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/plugins/sweetalert/sweetalert.css')}}">

	<!-- Google tag (gtag.js) -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-KXX7Q2QCHG"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'G-KXX7Q2QCHG');
	</script>

	<!-- Meta Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window, document,'script','https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '419164004404931');
	fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=419164004404931&ev=PageView&noscript=1"/></noscript>
	<!-- End Meta Pixel Code -->

	@vite(['resources/stylus/compra.styl'])
    <script src="{{asset('/plugins/sweetalert/sweetalert.min.js')}}"></script>
    <title>DATOS DE ENVÍO - VK_LIFESTYLE</title>
</head>
<body>
    <div class="spinner">
		<div id="loading"></div>
	</div>
    <div class="top-bar">
        <div class="img">
            <a href="{{route('home')}}">
				<img src="{{asset('/images/logo.svg')}}" alt="VK_LIFESTYLE">
            </a>
        </div>
    </div>
    <div class="Compra" id="app">
        <div class="layout">
            <div class="menu-oruga">
                <div class="menu-oruga-item">
                    <a href="{{ route('home', 'carrito') }}">CARRITO</a>
                    <span class="icon icon-siguiente"></span>
                </div>
                <div class="menu-oruga-item active">
                    <a href="{{ route('datos_envio') }}">DATOS DE ENVÍO</a>
                    <span class="icon icon-siguiente"></span>
                </div>
                <div class="menu-oruga-item">
                    <span>PAGO</span>
                    <span class="icon icon-siguiente"></span>
                </div>
                <div class="menu-oruga-item">
                    <span>RESUMEN</span>
                </div>
            </div>
            <div class="Compra-container">
                <div class="col">
                    <div class="content">
                        <div class="title-1">
                            <h1>Información</h1>
                        </div>
                        <div class="form">
                            <form action="{{ route('datos_envio') }}" method="POST" id="formEnvio">
								@csrf
                                <div class="group-inputs">
                                    <h3>PERSONAL</h3>
                                    <div class="inputs">
                                        <div class="input">
                                            <input type="text" name="nombre" placeholder="Nombre" data-validation-engine="validate[required]" value="{{ old('nombre') ?? ($cliente ? ($cliente->nombre ? explode('  ', $cliente->nombre)[0] : '') : '') }}">
                                        </div>
                                        <div class="input">
                                            <input type="text" name="apellidos" placeholder="Apellidos" data-validation-engine="validate[required]" value="{{ old('apellidos') ?? ($cliente ? ($cliente->nombre ? (count(explode('  ', $cliente->nombre))>1 ? explode('  ', $cliente->nombre)[1] : '') : '') : '') }}">
                                        </div>
                                    </div>
                                    <div class="inputs">
                                        <div class="input">
                                            <input type="text" name="telefono" placeholder="Teléfono" data-validation-engine="validate[required,minSize[10],maxSize[10],custom[onlyNumber]]" value="{{ old('telefono') ?? ($cliente ? ($cliente->telefono) : '') }}">
                                        </div>
                                        <div class="input">
                                            <input type="text" name="email" placeholder="Correo" data-validation-engine="validate[required,custom[email]]" value="{{ old('email') ?? ($cliente ? ($cliente->email) : '') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="group-inputs">
                                    <h3>DIRECCIÓN</h3>
                                    <div class="inputs">
                                        <div class="input">
                                            <input type="text" id="cpInput" name="cp" placeholder="Código Postal" data-validation-engine="validate[required,custom[onlyNumber]]]" value="{{ old('cp') ?? ($cliente ? (count($cliente->direcciones)>0 ? $cliente->direcciones[0]['cp'] : '') : '') }}">
                                        </div>
										<div class="input" id="coloniaInputContainer" style="display: none;">
                                            <input type="text" id="coloniaInput" name="colonia" placeholder="Colonia" data-validation-engine="validate[required]" value="{{ old('colonia') ?? ($cliente ? (count($cliente->direcciones)>0 ? $cliente->direcciones[0]['colonia'] : '') : '') }}">
                                        </div>
                                    </div>
                                    <div class="inputs">
                                    	<div class="input">
                                            <select id="coloniaSelect" name="colonia_select" data-validation-engine="validate[required]">
                                                <option value="">Seleccione una colonia</option>
                                            </select>
                                        </div>
                                        <div class="input">
                                            <button class="btn pulse-effect" type="button" id="showColoniaInput">Mi colonia no aparece.</button>
                                        </div>
                                    </div>
                                    <div class="inputs">
										<div class="input">
                                            <input type="text" id="municipioInput" name="municipio" placeholder="Ciudad" data-validation-engine="validate[required]" value="{{ old('municipio') ?? ($cliente ? (count($cliente->direcciones)>0 ? $cliente->direcciones[0]['ciudad'] : '') : '') }}">
                                        </div>
										<div class="input">
											<input type="text" id="estadoInput" name="estado" placeholder="Estado" data-validation-engine="validate[required]" value="{{ old('estado') ?? ($cliente ? (count($cliente->direcciones)>0 ? $cliente->direcciones[0]['estado'] : '') : '') }}">
										</div>
                                    </div>
                                    <div class="inputs">
										<div class="input">
                                            <input type="text" name="pais" placeholder="País" data-validation-engine="validate[required]" value="{{ app()->isLocale('es') ? 'México' : 'United States of America' }}" disabled>
                                        </div>
                                        <div class="input">
                                            <input type="text" name="calle" placeholder="Calle" data-validation-engine="validate[required]" value="{{ old('calle') ?? ($cliente ? (count($cliente->direcciones)>0 ? $cliente->direcciones[0]['calle'] : '') : '') }}">
                                        </div>
                                    </div>
									<div class="inputs">
										<div class="input">
											<input type="text" name="num_ext" placeholder="No ext" data-validation-engine="validate[required]" value="{{ old('num_ext') ?? ($cliente ? (count($cliente->direcciones)>0 ? $cliente->direcciones[0]['numero_ext'] : '') : '') }}">
                                        </div>
                                        <div class="input">
											<input type="text" name="num_int" placeholder="No int" value="{{ old('num_int') ?? ($cliente ? (count($cliente->direcciones)>0 ? $cliente->direcciones[0]['numero_int'] : '') : '') }}">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="buttons-bot">
                            <div class="buttons-bot-item">
                                <a href="{{route('catalogo', 'lo-mas-nuevo')}}" class="btn-return"><span class=""></span>Volver al catálogo</a>
                            </div>
                            <div class="buttons-bot-item">
                                <button class="btn-sig">SIGUIENTE</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="resume">
                        <p class="cantidad-titulo"><span>@{{ cantidad_productos }}</span> @{{ cantidad_productos == 1 ? 'PRODUCTO' : 'PRODUCTOS' }}</p>
                        <div class="carrito-contenido-tabla">
							<div class="carrito-contenido-tabla-item" v-for="producto in venta?.productos">
								<div class="seccion1">
									<div class="seccion1-imagen">
										<template v-if="producto?.producto?.producto_imagenes?.length > 0">
											<img :src="'{{ env('URL_CDN_PV') }}images/productos/'+producto?.producto?.producto_imagenes[0]?.imagen" alt="VK_LIFESTYLE">
										</template>
										<template v-else>
											<img src="/images/default.jpg" alt="VK_LIFESTYLE">
										</template>
									</div>
									<div class="seccion1-texto">
										<div class="seccion1-texto-info">
											<h3>@{{ producto?.producto?.nombre }} - @{{ producto?.producto?.nombre_variante }}</h3>
											<p v-if="producto?.precio_unitario != producto?.precio_venta" style="text-decoration: line-through">@{{ producto?.precio_unitario | currency }} @{{ venta?.moneda }}</p>
											<p>@{{ producto?.precio_venta | currency }} @{{ venta?.moneda }}</p>
											<br>
											<p>Cantidad: @{{ producto?.cantidad }}</p>
										</div>
									</div>
								</div>
								<div class="seccion">
									<p class="ver-mas">Eliminar</p>
								</div>
							</div>
						</div>
                        <div class="codigo">
                            <h3>código de descuento</h3>
                            <div class="codigo-input">
								<div v-if="venta">
									<div v-if="!venta?.cupon_cantidad || venta?.cupon_cantidad == 0">
										<input v-on:keyup.enter="aplicarDescuento()" v-model="venta.cupon_codigo" type="text" placeholder="Código de descuento">
										<button @click="aplicarDescuento()">APLICAR</button>
									</div>
									<div v-else>
										<p style="color: white">@{{ venta?.cupon_codigo }}</p>
										<button @click="eliminarDescuento()">ELIMINAR</button>
									</div>
								</div>
                            </div>
                        </div>
                        <div class="suma">
                            <div class="campo">
                                <p>Subtotal:</p>
                                <p>@{{ venta?.subtotal | currency }} @{{ venta?.moneda }}</p>
                            </div>
							<div class="campo" v-if="venta?.descuento_3x2 > 0">
                                <p>Promoción 3x2:</p>
                                <p>-@{{ venta?.descuento_3x2 | currency }} @{{ venta?.moneda }}</p>
                            </div>
                            <div class="campo" v-if="venta?.descuento > 0">
                                <p>Descuento:</p>
                                <p>-@{{ venta?.descuento | currency }} @{{ venta?.moneda }}</p>
                            </div>
                            <div class="campo">
                                <p>Envío:</p>
                                <p>@{{ venta?.envio | currency_envio }}<span v-if="venta?.envio > 0"> @{{ venta?.moneda }}</span></p>
                            </div>
                            <div class="campo total">
                                <p>TOTAL:</p>
                                <p><strong>@{{ venta?.total | currency }} @{{ venta?.moneda }}</strong></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	@vite(['resources/js/app.js'])
    <script src="{{ asset('plugins/jquery.min.js') }}"></script>
    <script src="{{asset('plugins/validationEngine/jquery.validationEngine.js')}}"></script>
	<script src="{{asset('plugins/validationEngine/jquery.validationEngine-es.js')}}"></script>
    <script src="{{asset('/plugins/spin.min.js')}}"></script>
    <script type="module">
		@if(app()->isLocale('es'))
			$('#cpInput').on('change', function(){
				var cp = this.value;
				if (cp.length !== 5) {
					alert("El código postal debe tener exactamente 5 caracteres.");
					return;
				}

				var routeUrl = "{{ route('buscar_cp') }}";
				var data = {
					'cp': cp
				};

				$.ajax({
					url: routeUrl,
					type: 'POST',
					data: data,
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					success: function(response) {
						if(response) {
							$('#estadoInput').val(response.estado);
							$('#municipioInput').val(response.municipio);

							var coloniaSelect = $('#coloniaSelect');
							coloniaSelect.empty();
							coloniaSelect.append('<option value="">Seleccione una colonia</option>');
							response.codigo_postal.forEach(function(colonia) {
								coloniaSelect.append('<option value="' + colonia.colonia + '">' + colonia.colonia + '</option>');
							});
						}
					},
					error: function(jqXHR, textStatus, errorThrown) {
						console.log(jqXHR);
						console.log(textStatus);
					}
				});
			});
		@endif
    </script>
    <script>
        $(document).ready(function() {
            // Verificar si hay un valor predefinido para el municipio
            var municipioValue = '{{ old('colonia') ?? ($cliente ? (count($cliente->direcciones)>0 ? $cliente->direcciones[0]['colonia'] : '') : '') }}';

            // Mostrar el botón o el select según si hay un valor predefinido para el municipio
            if (municipioValue) {
                $('#showColoniaInput').hide();
                $('#coloniaInputContainer').show();
                $('#coloniaSelect').hide();
            } else {
                $('#showColoniaInput').hide();
                $('#coloniaInputContainer').show();
                $('#coloniaSelect').hide();
            }

            // Agregar evento de cambio al campo de municipio
			@if(app()->isLocale('es'))
				$('#cpInput').change(function() {
					var municipioValue = $(this).val();
					if (municipioValue) {
						$('#showColoniaInput').show();
						$('#coloniaInputContainer').hide();
						$('#coloniaSelect').show();
					}
				});
			@endif
        });
    </script>
    <script>
        $(function(){

            // Capturar el evento de cambio del select
            $('#coloniaSelect').on('change', function() {
                // Obtener el valor seleccionado del select
                var selectedColonia = $(this).val();

                // Establecer el valor seleccionado en el campo de cliente
                $('input[name="colonia"]').val(selectedColonia);
            });

            $('#showColoniaInput').on('click', function() {
                $('#coloniaInputContainer').show();
                $('#coloniaSelect').hide();
				$('#showColoniaInput').hide();
            });

            @if(Session::get('alert') != null)
                swal("", "{{Session::get('alert')['mensaje']}}", "{{Session::get('alert')['estatus']}}");
            @endif

            $('#formEnvio').validationEngine({
                scroll: false
            });
            $('.btn-sig').on('click', function(){
                $('#formEnvio').submit()
            })

            var opts = {
                    lines: 8 // The number of lines to draw
                    , length: 18 // The length of each line
                    , width: 5 // The line thickness
                    , radius: 20 // The radius of the inner circle
                    , scale: 1 // Scales overall size of the spinner
                    , corners: 1 // Corner roundness (0..1)
                    , color: '#FFFFFF' // #rgb or #rrggbb or array of colors
                    , opacity: 0.25 // Opacity of the lines
                    , rotate: 0 // The rotation offset
                    , direction: 1 // 1: clockwise, -1: counterclockwise
                    , speed: 1 // Rounds per second
                    , trail: 60 // Afterglow percentage
                    , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
                    , zIndex: 2e9 // The z-index (defaults to 2000000000)
                    , className: 'spinner' // The CSS class to assign to the spinner
                    , top: '50%' // Top position relative to parent
                    , left: '50%' // Left position relative to parent
                    , shadow: true // Whether to render a shadow
                    , hwaccel: false // Whether to use hardware acceleration
                    , position: 'absolute' // Element positioning
                };

            var target = document.getElementById('loading');
            var spinner = new Spinner(opts).spin(target);
            // $('.spinner').show();
        });
    </script>
    <style>
        .pulse-effect {
            border: none;
            padding: 10px 10px;
            cursor: pointer;
            border-radius: 5px;
            background-color: #E1E1E1; /* Color base del botón */
            transition: transform 0.3s ease-in-out; /* Transición suave del efecto de pulsación */
        }
        .pulse-effect:hover {
            background-color: #CFD1D5; /* Cambia el color al pasar el cursor sobre el botón */
        }
        .pulse-effect:active {
        transform: scale(0.9); /* Reduce ligeramente el tamaño del botón cuando se hace clic */
        }
    </style>
</body>
</html>
