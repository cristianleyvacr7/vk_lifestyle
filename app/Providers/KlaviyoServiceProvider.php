<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use KlaviyoAPI\KlaviyoAPI;

class KlaviyoServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Klaviyo::class, function ($app) {
            $apiKey = env('KLAVIYO_API_KEY');
            return new Klaviyo($apiKey);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
