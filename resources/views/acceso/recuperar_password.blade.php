@extends('layout')
@section('head')
	@vite(['resources/stylus/acceso.styl'])
    <title>RECUPERAR CONTRASEÑA - VK LIFESTYLE</title>
	<link rel="stylesheet" href="{{asset('plugins/validationEngine/validationEngine.jquery.css')}}">

@endsection
@section('contenido')
    <div class="recuperar">
        <div class="recuperar-contenido">
            <h1 class="title">RECUPERAR CONTRASEÑA</h1>
            <p>
                Si no recuerdas tu contraseña, solo necesitamos tu dirección de correo
                electrónico para enviarte un email con las indicaciones precisas para recuperarla.
            </p>
            <div class="recuperar-contenido-correo">
                <form id="formRecuperar" action="{{ route('recuperar_contrasena') }}" method="POST">
                    @csrf
                    <div class="input text">
                        <label for="correo">Correo</label>
                        <input type="email" name="correo" data-validation-engine="validate[required,custom[email]]">
                    </div>
                    <div class="boton">
                        <button>ENVIAR</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script src="{{asset('/plugins/validationEngine/jquery.validationEngine.js')}}"></script>
<script src="{{asset('/plugins/validationEngine/jquery.validationEngine-es.js')}}"></script>
<script type="module">
    $(function(){
       $('#formRecuperar').validationEngine({
           scroll: false
       });
   });
</script>
@endsection
