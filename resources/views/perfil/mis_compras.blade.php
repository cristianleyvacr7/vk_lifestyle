@extends('layout')
@section('head')
    <title>MIS COMPRAS -VK LIFESTYLE</title>
	@vite(['resources/stylus/perfil.styl'])
@endsection
@section('contenido')
	<div class="cuenta">
		<div class="cuenta-contenido">
			<a href="{{route('home')}}" class="cerrar-sesion">
				<p>Cerrar sesión</p>
				<span class="icon icon-flechita"></span>
			</a>
			<div class="cuenta-contenido-titulo">
				<a href="{{route('perfil')}}">MI PERFIL</a>
				<a class="selected" href="{{route('mis_compras')}}">MIS COMPRAS</a>
			</div>
			<div class="historico">
				<div class="historico-contenido">
					<div class="compras" >
						@foreach ( $compras as $compra )
							<div class="compras-item" data-compra-id="{{ $compra['id'] }}">
								<div class="compras-item-img">
									@if (count($compra['productos']) > 0 && count($compra['productos'][0]['producto']['producto_imagenes']) > 0)
										<img src="{{ env('URL_CDN_PV') }}images/productos/{{$compra['productos'][0]['producto']['producto_imagenes'][0]['imagen']}}" alt="VK_LIFESTYLE">
									@else
										<img src="/images/default.jpg" alt="VK_LIFESTYLE">
									@endif
								</div>
								<div class="compras-item-datos">
									<div class="dato-item dato-folio">
										<p class="negritas">Folio:</p>
										<p class="negritas">{{ str_pad($compra['folio'], 8, "0", STR_PAD_LEFT) }}</p>
									</div>
									<div class="dato-item">
										<p>Estatus:</p>
										<p>{{ ucfirst($compra['ecommerce_estatus']) }}</p>
									</div>
									<div class="dato-item">
										<p>Cantidad:</p>
										<p>${{ number_format($compra['total'], 2, '.', ',') }} {{ $compra['moneda'] }}</p>
									</div>
									@if(count($compra['venta_ecommerce']['envios']) == 0)
										<div class="dato-item">
											<p>Fecha de envío:</p>
											<p>Aún no se a enviado la compra</p>
										</div>
									@else
										@if(count($compra['venta_ecommerce']['envios']) > 1)
											<div class="dato-item">
												<p>Fecha de envío:</p>
												<p>Esta compra tiene varios envíos</p>
											</div>
										@else
											<div class="dato-item">
												<p>Fecha de envío:</p>
												<p>{{ date('d/m/Y H:i', strtotime($compra['venta_ecommerce']['envios'][0]['fecha_envio'])) }}</p>
											</div>
										@endif
									@endif
								</div>
							</div>
						@endforeach
					</div>
				</div>
			</div>
			@foreach ( $compras as $compra )
				<div class="recuadro" style="display: none;" id="recuadro-detalles-{{$compra['id']}}">
					<div class="recuadro-contenido">
						<div class="recuadro-contenido-articulos">
							<h2>{{ array_sum(array_column($compra['productos'], 'cantidad')) == 1 ? array_sum(array_column($compra['productos'], 'cantidad')).' ARTICULO' : array_sum(array_column($compra['productos'], 'cantidad')).' ARTICULOS' }}</h2>
							<div class="cerrar">
								<span class="icon-cerrar"></span>
							</div>
							<div class="compras">
								@foreach ( $compra['productos'] as $venta_producto )
									<div class="compras-historico">
										<div class="compras-item-img">
											@if (count($venta_producto['producto']['producto_imagenes']) > 0)
												<img src="{{ env('URL_CDN_PV') }}images/productos/{{$venta_producto['producto']['producto_imagenes'][0]['imagen']}}" alt="VK_LIFESTYLE">
											@else
												<img src="/images/default.jpg" alt="VK_LIFESTYLE">
											@endif
										</div>
										<div class="compras-item-datos">
											<div class="dato-historico">
												<p class="titulo">{{ $venta_producto['producto']['nombre'] }}</p>
											</div>
											<div class="dato-historico">
												<p class="precio">
													@if ( $venta_producto['precio_unitario'] != $venta_producto['precio_venta'] )
														<span style="text-decoration: line-through;">
															${{ number_format($venta_producto['precio_unitario'], 2, '.', ',') }} {{ $compra['moneda'] }}
														</span>
														<br>
													@endif
													${{ number_format($venta_producto['precio_venta'], 2, '.', ',') }} {{ $compra['moneda'] }}
												</p>
											</div>
											<div class="dato-historico">
												<div class="dato-historico-item">
													<p>Cantidad:</p>
													<p>{{ intval($venta_producto['cantidad']) }}</p>
												</div>
												<div class="dato-historico-item">
													<p>Talla:</p>
													<p>{{ $venta_producto['producto']['nombre_variante'] }}</p>
												</div>
											</div>
										</div>
									</div>
								@endforeach
							</div>
						</div>
						<div class="recuadro-contenido-folio">
							<div class="folio-contenido">
								<h3>Folio: {{ str_pad($compra['folio'], 8, "0", STR_PAD_LEFT) }}</h3>
								<div class="folio-fecha">
									<div class="folio-fecha-opcion">
										<p>Fecha de creación:</p>
										<span>{{ $compra['venta_ecommerce']['fecha_pago'] ? date('d/m/Y', strtotime($compra['venta_ecommerce']['fecha_pago'])) : 'Aún no se ha pagado la compra' }}</span>

									</div>
									<div class="folio-fecha-opcion">
										<p>Tipo de pago:</p>
										<span>{{ ucfirst($compra['venta_ecommerce']['forma_de_pago']) }}</span>
									</div>

									@if($compra['venta_ecommerce']['forma_de_pago'] == 'oxxo')
										@if($compra['venta_ecommerce']['vigencia'] > date('Y-m-d H:i:s'))
											@if($compra['venta_ecommerce']['estatus'] == 'por pagar')
												<br>
												<div class="folio-fecha-opcion">
													<a href="{{ $compra['venta_ecommerce']['referencia_pago_4'] }}" target="_blank" style="margin-right: 5px">Descargar voucher:</a>
													<a href="{{ $compra['venta_ecommerce']['referencia_pago_4'] }}" target="_blank"><img src="/images/oxxo.png" alt="OXXO" style="width: 40px;"></a>
												</div>
												<div class="folio-fecha-opcion">
													<p>Fecha de expiración:</p>
													<span>{{ date('d/m/Y', strtotime($compra['venta_ecommerce']['vigencia'])) }}</span>
												</div>
											@endif
										@else
											<div class="folio-fecha-opcion">
												<p>Fecha de expiración:</p>
												<span>{{ date('d/m/Y', strtotime($compra['venta_ecommerce']['vigencia'])) }} (Expirado)</span>
											</div>
										@endif
									@endif

								</div>
								<div class="folio-personal">
									<div class="folio-personal-opcion">
										<p>Nombre:</p>
										<span>{{ $compra['cliente']['nombre'] }}</span>
									</div>
									<div class="folio-personal-opcion">
										<p>Correo:</p>
										<span>{{ $compra['cliente']['email'] }}</span>
									</div>
									<div class="folio-personal-opcion">
										<p>Teléfono:</p>
										<span>{{ $compra['cliente']['telefono'] }}</span>
									</div>
								</div>
								@php
									$direccion_DB = $compra['cliente']['direcciones'][0];
									if($compra['venta_ecommerce']['direccion']){
										$direccion_DB = $compra['venta_ecommerce']['direccion'];
									}

									$direccion = $direccion_DB['calle'].' #'.$direccion_DB['numero_ext'];
									if($direccion_DB['numero_int'])
									{
										$direccion .= ' no. int. '.$direccion_DB['numero_int'];
									}
									$direccion .= ' Col. '.$direccion_DB['colonia'].', '.$direccion_DB['cp'].' '.$direccion_DB['ciudad'].', '.$direccion_DB['estado'].', '.$direccion_DB['pais'].'.';
								@endphp
								<div class="folio-ubicacion">
									<div class="folio-ubicacion-opcion">
										<p>
											Datos del envío:
											<span>
												{{ $direccion }}
											</span>
										</p>
									</div>
									@foreach ($compra['venta_ecommerce']['envios'] as $key => $envio)
										<br>
										<br>
										<p><strong>Envío {{count($compra['venta_ecommerce']['envios'])>1 ? ($key+1) : ''}}</strong></p>
										<div class="folio-ubicacion-opcion">
											<p>
												Fecha de envío:
												<span>{{ date('d/m/Y H:i', strtotime($envio['fecha_envio'])) }}</span>
											</p>
										</div>
										<div class="folio-ubicacion-opcion">
											<p>
												Paquetería:
												<span>{{ $envio['paqueteria'] }}</span>
											</p>
										</div>
										<div class="folio-ubicacion-opcion">
											<p>
												Guía de envío:
												<span>{{ $envio['guia'] }}</span>
											</p>
										</div>
										<br>
										<table style="width: 100%">
											<thead>
												<tr>
													<td>Producto</td>
													<td style="text-align:center">Talla</td>
													<td style="text-align:center">Cantidad</td>
												</tr>
											</thead>
											<tbody>
												@foreach ($envio['productos'] as $envio_producto)
													<tr>
														<td>{{ $envio_producto['venta_producto']['producto']['nombre'] }}</td>
														<td style="text-align:center">{{ $envio_producto['venta_producto']['producto']['nombre_variante'] }}</td>
														<td style="text-align:center">{{ $envio_producto['cantidad'] }}</td>
													</tr>
												@endforeach
											</tbody>
										</table>
									@endforeach
								</div>

								<div class="total">
									<div class="total-item">
										<p>Subtotal:</p>
										<span>${{ number_format($compra['subtotal'], 2, '.', ',') }} {{ $compra['moneda'] }}</span>
									</div>
									@if ($compra['descuento_3x2'] > 0)
										<div class="total-item">
											<p>Promoción 3x2:</p>
											<span>-${{ number_format($compra['descuento_3x2'], 2, '.', ',') }} {{ $compra['moneda'] }}</span>
										</div>
									@endif
									@if ($compra['descuento'] > 0)
										<div class="total-item">
											<p>Descuento:</p>
											<span>-${{ number_format($compra['descuento'], 2, '.', ',') }} {{ $compra['moneda'] }}</span>
										</div>
									@endif
									<div class="total-item">
										<p>Envio:</p>
										<span>{{ $compra['envio'] != 0 ? '$'.number_format($compra['envio'], 2, '.', ',').' '.$compra['moneda'] : 'GRATIS' }}</span>
									</div>
								</div>
							</div>
							<div class="final">
								<div class="final-contenido">
									<p>TOTAL:</p>
									<span>${{ number_format($compra['total'], 2, '.', ',') }} {{ $compra['moneda'] }}</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			@endforeach
		</div>
	</div>
@endsection
@section('js')
	<script type="module">
		let compraId;
		$('.compras-item').on('click', function(){
			compraId = $(this).data('compra-id');
			$(`#recuadro-detalles-${compraId}`).show();
			$('.spinner').css('z-index', '1000');
			$('.spinner').css('background', 'rgba(0,0,0,0.4)');
			$('.spinner').show();
		})
		$('.cerrar').on('click', function(){
			$(`#recuadro-detalles-${compraId}`).hide();
			$('.spinner').hide();
			$('.spinner').css('z-index', '10000000000000');
			$('.spinner').css('background', 'rgba(0,0,0,0.7)');
		})
	</script>
@endsection
