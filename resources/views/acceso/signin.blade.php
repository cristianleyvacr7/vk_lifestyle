@extends('layout')
@section('head')
    <title>SIGNIN -VK LIFESTYLE</title>
	@vite(['resources/stylus/login.styl'])
	<link rel="stylesheet" href="{{asset('plugins/validationEngine/validationEngine.jquery.css')}}">
	@endsection
@section('contenido')
	<div class="Login">
		<div class="layout">
			<h1 class="titulo">SIGN IN</h1>
			<form action="{{ route('signin') }}" method="post" id="form-registro">
				@csrf
				<div class="input text">
					<label for="">Nombre</label>
					<input type="text" name="nombre" data-validation-engine="validate[required]" value="{{old('nombre')}}">
				</div>
				<div class="input text">
					<label for="">Apellidos</label>
					<input type="text" name="apellidos" data-validation-engine="validate[required]" value="{{old('apellidos')}}">
				</div>
				<div class="input text">
					<label for="">Teléfono</label>
					<input type="text" name="telefono" data-validation-engine="validate[required,minSize[10],maxSize[10],custom[onlyNumber]]" value="{{old('telefono')}}">
				</div>
				<div class="input text">
					<label for="">Email</label>
					<input type="email" name="email" data-validation-engine="validate[required,custom[email]]" value="{{old('email')}}">
				</div>
				<div class="input text">
					<label for="">Contraseña</label>
					<input type="password" name="password" data-validation-engine="validate[required,minSize[6]]" id="password">
				</div>
				<div class="input text">
					<label for="">Confirmar Contraseña</label>
					<input type="password" name="confirm_paswsord" data-validation-engine="validate[required,equals[password]]">
				</div>
				<br>
				<br>
				<div class="boton">
					<button>SIGN IN</button>
				</div>
				<a href="{{route('login')}}">¿Ya tienes cuenta? Inicia sesión</a>
			</form>
		</div>
	</div>
@endsection
@section('js')
	<script src="{{asset('/plugins/validationEngine/jquery.validationEngine.js')}}"></script>
	<script src="{{asset('/plugins/validationEngine/jquery.validationEngine-es.js')}}"></script>
	<script type="module">
		$(function(){
			$('#form-registro').validationEngine({
				scroll: false
			});
		});
	</script>
@endsection
