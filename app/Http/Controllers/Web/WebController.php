<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\Service;
use App\Models\Banner;
use Jenssegers\Agent\Agent;
use Cookie;

class WebController extends Controller
{
    public function index(){

		try{
			// PRODUCTOS
			$lo_mas_nuevo = [
				'metacampos' => 'sandalias',
				'home' => true,
				'paginacion' => '6',
			];

			$western_fashions = [
				'metacampos' => 'botas',
				'home' => true,
				'paginacion' => '8',
			];

			$client = new \GuzzleHttp\Client([
				'base_uri' => env('URL_PV')
			]);
			$promises = [
				$client->getAsync("/api/vk-lifestyle/banners/" . env('VK_ID') . "?seccion=Home"),
				$client->getAsync("/api/vk-lifestyle/productos/" . env('VK_ID'), ['query' => $lo_mas_nuevo]),
				$client->getAsync("/api/vk-lifestyle/productos/" . env('VK_ID'), ['query' => $western_fashions]),
			];

			$responses = \GuzzleHttp\Promise\Utils::settle(
				\GuzzleHttp\Promise\Utils::unwrap($promises),
			)->wait();

			$banners = json_decode($responses[0]['value']->getBody()->getContents(), true);
			$productos_mas_nuevo = json_decode($responses[1]['value']->getBody()->getContents(), true);
			$productos_western_fashions = json_decode($responses[2]['value']->getBody()->getContents(), true);

			// Si no trae ningun producto del Home
			if(isset($productos_mas_nuevo['productos']) && isset($productos_mas_nuevo['productos']['data']) && count($productos_mas_nuevo['productos']['data']) == 0){
				$lo_mas_nuevo = [
					'metacampos' => 'sandalias',
					'paginacion' => '6'
				];

				$promises = [
					$client->getAsync("/api/vk-lifestyle/productos/" . env('VK_ID'), ['query' => $lo_mas_nuevo])
				];

				$responses = \GuzzleHttp\Promise\Utils::settle(
					\GuzzleHttp\Promise\Utils::unwrap($promises),
				)->wait();

				$productos_mas_nuevo = json_decode($responses[0]['value']->getBody()->getContents(), true);
			}

			// Si no trae ningun producto del Home
			// if(isset($productos_western_fashions['productos']) && isset($productos_western_fashions['productos']['data']) && count($productos_western_fashions['productos']['data']) == 0){
				$western_fashions = [
					'metacampos' => 'botas',
					'paginacion' => '8'
				];

				$promises = [
					$client->getAsync("/api/vk-lifestyle/productos/" . env('VK_ID'), ['query' => $western_fashions])
				];

				$responses = \GuzzleHttp\Promise\Utils::settle(
					\GuzzleHttp\Promise\Utils::unwrap($promises),
				)->wait();

				$productos_western_fashions = json_decode($responses[0]['value']->getBody()->getContents(), true);
			// }

			$productos_mas_nuevo = self::calcularUSD($productos_mas_nuevo);
			$productos_western_fashions = self::calcularUSD($productos_western_fashions);
		}catch(\Exception $e){
			\Log::info('--------------- ERROR HOME---------------');
			\Log::info($e);
		}
		return view('home')->with('banners', $banners ?? [])
							->with('los_mas_nuevos', $productos_mas_nuevo ?? [])
							->with('casuales', $productos_casuales ?? [])
							->with('western_fashions', $productos_western_fashions ?? []);
    }

	public function cp(Request $request){
		try {
			$http = new \GuzzleHttp\Client;
			$url_pv = url(env('URL_PV').'api/vk-lifestyle/');

        	$response = $http->request('POST', $url_pv . "search-cp/" . env('VK_ID'), [
				'form_params' => [
					'cp' => $request->cp
				]
			]);

			$data = json_decode($response->getBody(), true);
			return response()->json($data);
		}catch(\Exception $e){
			\Log::info('Error: '.$e);
		}
	}

    public function catalogo(Request $request, $metacampo_slug){
		try{

			if (!$request->input('paginacion')) {
				$request = $request->merge(['paginacion' => 15]);
			}
			$request = $request->merge(['metacampos' => $metacampo_slug]);

			$client = new \GuzzleHttp\Client([
				'base_uri' => env('URL_PV')
			]);
			$promises = [
				$client->getAsync("api/vk-lifestyle/catalogos/" . env('VK_ID')),
				$client->getAsync("api/vk-lifestyle/productos/" . env('VK_ID'), ['query' => $request->all()]),
				$client->getAsync("api/vk-lifestyle/banners/" . env('VK_ID') . "?seccion=".$metacampo_slug),
			];

			$responses = \GuzzleHttp\Promise\Utils::settle(
				\GuzzleHttp\Promise\Utils::unwrap($promises),
			)->wait();

			$filtros = json_decode($responses[0]['value']->getBody()->getContents(), true);
			$productos = json_decode($responses[1]['value']->getBody()->getContents(), true);
			$banners = json_decode($responses[2]['value']->getBody()->getContents(), true);

			$productos = self::calcularUSD($productos);

			$page = $productos['productos']['current_page'];
			$perPage = $productos['productos']['per_page'];

			// Total de elementos y elementos actuales
			$total = $productos['productos']['total'];
			$data = $productos['productos']['data']; // Los datos de la página actual

			// Crear un objeto de paginación
			$paginacion = new LengthAwarePaginator($data, $total, $perPage, $page);
			$paginacion->setPath('/'.$request->path());

		}catch(\Exception $e){
			\Log::info('--------------- ERROR CATALOGO---------------');
			\Log::info($e);
			$alert = array(
				'titulo' => '',
				'mensaje' => 'No fue posible cargar los productos, vuelve a intentralo en unos momentos',
				'estatus' => 'error'
			);
			View::share('alert', $alert);
		}
        return view('catalogo')->with('metacampo_slug', $metacampo_slug)
							->with('productos', $productos ?? [])
							->with('filtros', $filtros ?? [])
							->with('paginacion', $paginacion ?? [])
							->with('banners', $banners ?? []);
    }

	public function detalle($slug){
		try{
			$agent = new Agent();
			$isMobile = $agent->isMobile();

			$http = new \GuzzleHttp\Client;
			$url_pv = url(env('URL_PV').'api/vk-lifestyle/');
			$num_relacionados = [
				'paginacion' => '6',
			];
			$response = $http->request('GET', $url_pv . "detalle-producto/" . env('VK_ID') . "/" . $slug, ['query' => $num_relacionados]);
			$detalle_producto = json_decode($response->getBody(), true);
			$productos = null;
			if(isset($detalle_producto['producto'])){
				$productos = $detalle_producto['relacionados'];
				$productos = self::calcularUSD($productos);

				if(count($detalle_producto['producto'])==0){
					$alert = array(
						'titulo' => '',
						'mensaje' => 'No fue posible cargar el producto, vuelve a intentralo en unos momentos',
						'estatus' => 'error'
					);
					return redirect()->route('home')->with('alert', $alert);
				}

				$data = [
					'productos' => [
						'data' => [
							$detalle_producto['producto']
						]
					]
				];
				$producto = self::calcularUSD($data);
				$detalle_producto['producto'] = $producto['productos']['data'][0];
			}

			return view('detalle_producto')->with('detalle_producto', $detalle_producto ?? [])
										->with('relacionados', $productos ?? []);
		}catch(\Exception $e){
			\Log::info('--------------- ERROR DETALLE PRODUCTO---------------');
			\Log::info($e);
			$alert = array(
				'titulo' => '',
				'mensaje' => 'No fue posible cargar el producto, vuelve a intentralo en unos momentos',
				'estatus' => 'error'
			);
			return redirect()->route('home')->with('alert', $alert);
		}

    }

	public function envio(Request $request){
		try{
			$http = new \GuzzleHttp\Client;
			$url_pv = url(env('URL_PV').'api/vk-lifestyle/');
			$session_token = request()->session()->get('_token');

			if ($request->isMethod('get')) {
				$data = [
					'session_token' => request()->session()->get('_token'),
				];
				if($request->cookie('session_token')){
					$data['auth_token'] = $request->cookie('session_token');
				}
				$response = $http->request('GET', $url_pv . 'get-cart/' . env('VK_ID'), [ 'query' => $data ]);
				$carrito = json_decode($response->getBody(), true);
				if ($carrito['cantidad_productos'] == 0) {
					$mensaje = array(
						'titulo' => '',
						'mensaje' => 'No hay productos en tu carrito',
						'estatus' => 'warning'
					);
					return redirect()->route('home')->with('alert', $mensaje);
				}

				return view('compra.datos_envio');
			}elseif ($request->isMethod('post')) {

				if($request->cookie('session_token')){
					$request->merge(['session_token' => $request->cookie('session_token')]);
				}
				try {
					$request->merge(['pais' => app()->isLocale('es') ? 'México' : 'United States of America']);

					$response = $http->request('POST', $url_pv . "datos-cliente/" . env('VK_ID'), ['json' => $request->all()]);
					$response = json_decode($response->getBody(), true);
					if (isset($response['estatus'])) {
						$data = array(
							'titulo' => '',
							'mensaje' => $response['mensaje'],
							'estatus' => $response['estatus']
						);
						return redirect()->back()->with('alert', $data)->withInput();
					}
					if (isset($response['mensaje'])) {
						$data = array(
							'titulo' => '',
							'mensaje' => $response['mensaje'],
							'estatus' => 'warning'
						);
						return redirect()->route('datos_pago')->with('alert', $data);
					}
					return redirect()->route('datos_pago');
				} catch (\Exception $e) {

					\Log::info($e);
					$data = array(
						'titulo' => '',
						'mensaje' => 'Ocurrió un error al guardar la información, vuelve a intentarlo en unos momentos',
						'estatus' => 'error'
					);
					return redirect()->back()->with('alert', $data)->withInput();
				}
			}
		}catch(\Exception $e){
			\Log::info('---------------------- Error Datos Del Envío ----------------------');
			\Log::info($e);
			$data = array(
				'titulo' => '',
				'mensaje' => 'Ocurrió un error, vuelve a intentarlo en unos momentos',
				'estatus' => 'error'
			);
			return redirect()->route('home')->with('alert', $data);
		}
    }

	public function pago(Request $request){
		$http = new \GuzzleHttp\Client;
        $url_pv = url(env('URL_PV').'api/vk-lifestyle/');
        $session_token = request()->session()->get('_token');

		// Validación que el carrito exista y tenga productos
		$data = [
			'session_token' => $session_token,
		];
		if($request->cookie('session_token')){
			$data['auth_token'] = $request->cookie('session_token');
		}
		$response = $http->request('GET', $url_pv . 'get-cart/' . env('VK_ID'), [ 'query' => $data ]);
		$carrito = json_decode($response->getBody(), true);

		if ($carrito['cantidad_productos'] == 0) {
			$mensaje = array(
				'titulo' => '',
				'mensaje' => 'No hay productos en tu carrito',
				'estatus' => 'warning'
			);
			return redirect()->route('home')->with('alert', $mensaje);
		}

		if ($request->isMethod('get')) {
			try {
				// Consulta de credenciales
				$data = [
					'pasarela' => 'stripe',
				];
				$response = $http->request('GET', $url_pv . 'get-credentials/' . env('VK_ID'), [ 'query' => $data ]);
				$credenciales = json_decode($response->getBody(), true);

				$pasarela = [];
				if(isset($credenciales['stripe'])){
					$pasarela = [
						'stripe' => [
							'public_key' => $credenciales['stripe']['keys_pasarelas']['public_key'],
							'private_key' => $credenciales['stripe']['keys_pasarelas']['private_key'],
							'no_meses' => $credenciales['stripe']['keys_pasarelas']['no_meses']
						]
					];
				}

				return view('compra.datos_pago')->with('credenciales', $pasarela);
			} catch (\Exception $e) {
				\Log::info('---------------------- Error Datos De Pago ----------------------');
                \Log::info($e);
                $data = array(
                    'titulo' => '',
                    'mensaje' => 'Ocurrió un error al cargar las formas de pago',
                    'estatus' => 'error'
                );
                return view('compra.datos_pago')->with('credenciales', $pasarela ?? [])->with('alert', $data);
            }
		}elseif ($request->method() == 'POST') {
			try{
				if ($request->input('metodo_pago') == 'tarjeta') {
					$street = $carrito['venta']['cliente']['direcciones'][0]['calle'].' '.$carrito['venta']['cliente']['direcciones'][0]['numero_ext'];
					if($carrito['venta']['cliente']['direcciones'][0]['numero_int'])
					{
						$street = $street . ' no. int. '.$carrito['venta']['cliente']['direcciones'][0]['numero_int'];
					}
					$street = $street . ' Col. '.$carrito['venta']['cliente']['direcciones'][0]['colonia'];

					$data = [
						'pasarela' => 'stripe'
					];
					$response = $http->request('GET', $url_pv . 'get-credentials/' . env('VK_ID'), [ 'query' => $data ]);
					$credenciales = json_decode($response->getBody(), true);

					if(!isset($credenciales['stripe'])){
						$data = array(
							'titulo' => '',
							'mensaje' => 'Ocurrió un error al hacer el pago, vuelve a intentarlo en unos momentos',
							'estatus' => 'error'
						);
						return redirect()->back()->with('alert', $data);
					}

					\Stripe\Stripe::setApiKey($credenciales['stripe']['keys_pasarelas']['private_key']);
					$token = $request->input('stripeToken');

					$intent = \Stripe\PaymentIntent::create([
						'amount' => floatval($carrito['venta']['total'])*100,
						'currency' => $carrito['venta']['moneda'],
						'description' => 'Compra en ecommerce VK Lifestyle',
						'metadata' => [
							'order_id' => 'VK-'.$carrito['venta']['id']
						],
						'payment_method_data' => [
							'type' => 'card',
							'card' => [
								'token' => $token
							]
						],
						'payment_method_options' => [
							'card'=>[
								'installments' => [
									'enabled' => true
								],
								'request_three_d_secure' => 'any'
							]
						],
						'shipping' => [
							"name" => $carrito['venta']['cliente']['nombre'],
							"phone" => $carrito['venta']['cliente']['telefono'],
							"address" => array(
								'line1' => $street,
								'city' => $carrito['venta']['cliente']['direcciones'][0]['ciudad'],
								'country' => $carrito['venta']['cliente']['direcciones'][0]['pais'],
								'postal_code' => $carrito['venta']['cliente']['direcciones'][0]['cp'],
								'state' => $carrito['venta']['cliente']['direcciones'][0]['estado']
							)
						]
					]);

					$venta_ecommerce = [
						'referencia_pago_1' => $intent->id,
						'referencia_pago_2' => $intent->client_secret,
						'referencia_pago_3' => $intent->status,
						'session_token' => $session_token,
					];
					if($request->cookie('session_token')){
						$venta_ecommerce['auth_token'] = $request->cookie('session_token');
					}
					$response = $http->request('POST', $url_pv . 'nueva-venta-ecommerce/' . env('VK_ID'), ['json' => $venta_ecommerce]);
					$venta_ecommerce_response = json_decode($response->getBody(), true);

					if (isset($venta_ecommerce_response['mensaje'])) {
						$data = array(
							'titulo' => '',
							'mensaje' => $venta_ecommerce_response['mensaje'],
							'estatus' => 'error'
						);
						return redirect()->back()->with('alert', $data);
					}

					try{
						if($intent->status != 'succeeded'){
                            $meses = $request->input('no_meses') ?? 0;
                            if($meses != 0){
                                $plans = $intent->payment_method_options->card->installments->available_plans;
                                if($plans)
                                {
                                    $bn = 0;
                                    $planes_validos = [];
                                    foreach ($plans as $plan)
                                    {
                                        if($plan['count'] == $meses)
                                        {
                                            $bn = 1;
                                            $data = [
                                                'payment_method_options' =>
                                                [
                                                    'card' => [
                                                        'installments' => [
                                                            'plan' => [
                                                                'count' => $plan->count,
                                                                'interval' => $plan->interval,
                                                                'type' => $plan->type,
                                                            ]
                                                        ],
                                                        'request_three_d_secure' => 'any'
                                                    ]
                                                ],
                                                'return_url' => route('stripe')
                                            ];
                                            $intent->confirm($data);
                                            break;
                                        }
                                        $planes_validos[] = $plan['count'];
                                    }

                                    if($bn == 0)
                                    {
                                        $data = array(
                                            'titulo' => '',
                                            'mensaje' => 'No esta disponible este plan de '.$meses.' meses para esta tarjeta, vuelve a intentarlo con otro plan. '.(count($planes_validos)==1 ? 'Plan valido '.$planes_validos[0].'.' : 'Planes validos '.implode(', ', $planes_validos).'.'),
                                            'estatus' => 'error'
                                        );
                                        return redirect()->back()->with('alert', $data);
                                    }
                                }else
                                {
									$data = array(
										'titulo' => '',
										'mensaje' => 'No esta disponible ningun plan de meses para esta tarjeta, intentalo con otra tarjeta o sin plan de meses.',
										'estatus' => 'error'
									);
									return redirect()->back()->with('alert', $data);
                                }
                            }else{
                                $intent->confirm(['return_url' => route('stripe')]);
                            }
                        }
						$redirect = $intent->next_action->redirect_to_url->url;
						return redirect($redirect);
					}catch (\Exception $e){
						return redirect()->route('stripe', ['payment_intent'=> $intent->id, 'payment_intent_client_secret' => $intent->client_secret]);
					}
				}
				if($request->input('metodo_pago') == 'paypal'){

					$provider = \PayPal::setProvider();

					$data = [
						'pasarela' => 'paypal',
					];
					$response = $http->request('GET', $url_pv . 'get-credentials/' . env('VK_ID'), [ 'query' => $data ]);
					$credenciales = json_decode($response->getBody(), true);

					if(!isset($credenciales['paypal'])){
						$data = array(
							'titulo' => '',
							'mensaje' => 'Ocurrió un error al hacer el pago, vuelve a intentarlo en unos momentos',
							'estatus' => 'error'
						);
						return redirect()->back()->with('alert', $data);
					}

					$config = [
						'mode'    => 'sandbox',
						'sandbox' => [
							'client_id'         => $credenciales['paypal']['keys_pasarelas']['public_key'],
							'client_secret'     => $credenciales['paypal']['keys_pasarelas']['private_key'],
							'app_id'            => 'APP-80W284485P519543T',
						],

						'payment_action' => 'Sale',
						'currency'       => 'MXN',
						'notify_url'     => 'https://your-app.com/paypal/notify',
						'locale'         => 'es_MX',
						'validate_ssl'   => true,
					];

					$provider->setApiCredentials($config);
					$token = $provider->getAccessToken();
					$provider->setAccessToken($token);

					$data = [
						'intent' => 'CAPTURE',
						'purchase_units' => [[
							'reference_id' => $carrito['venta']['id'],
							'amount' => [
								'currency_code' => 'MXN',
								'value' => number_format($carrito['venta']['total'], 2, '.', '')
							]
						]],
						'application_context' => [
							'cancel_url' => route('paypal_cancel'),
							'return_url' => route('paypal_success')
						]
					];

					// Prepare Order
					$order = $provider->createOrder($data);

					$venta_ecommerce = [
						'referencia_pago_1' => $order['id'],
						'session_token' => $session_token,
					];
					if($request->cookie('session_token')){
						$venta_ecommerce['auth_token'] = $request->cookie('session_token');
					}
					$response = $http->request('POST', $url_pv . 'nueva-venta-ecommerce/' . env('VK_ID'), ['json' => $venta_ecommerce]);
					$venta_ecommerce_response = json_decode($response->getBody(), true);

					if (isset($venta_ecommerce_response['mensaje'])) {
						$data = array(
							'titulo' => '',
							'mensaje' => $venta_ecommerce_response['mensaje'],
							'estatus' => 'error'
						);
						return redirect()->back()->with('alert', $data);
					}

					return redirect($order['links'][1]['href']);
				}
				if($request->input('metodo_pago') == 'oxxo'){

					return redirect()->route('gracias', $request->input('ventaEcommerce'));
				}

				$data = array(
					'titulo' => '',
					'mensaje' => 'Ocurrió un error al hacer el pago, vuelve a intentarlo más tarde',
					'estatus' => 'error'
				);
				return redirect()->back()->with('alert', $data);
			}catch(\Exception $e){
				\Log::info($e);
				$data = array(
					'titulo' => '',
					'mensaje' => 'Ocurrió un error al hacer el pago, vuelve a intentarlo más tarde',
					'estatus' => 'error'
				);
				return redirect()->back()->with('alert', $data);
			}
        }
    }

	public function paypal_success(Request $request)
    {
        try {
            \Log::info('----- Request de paypal -----');
            \Log::info($request->all());

            $provider = \PayPal::setProvider();
            $http = new \GuzzleHttp\Client;
            $url_pv = url(env('URL_PV').'api/vk-lifestyle/');
			$data = [
				'pasarela' => 'paypal',
			];
			$response = $http->request('GET', $url_pv . 'get-credentials/' . env('VK_ID'), [ 'query' => $data ]);
			$credenciales = json_decode($response->getBody(), true);

			if(!isset($credenciales['paypal'])){
				$data = array(
					'titulo' => '',
					'mensaje' => 'Ocurrió un error al hacer el pago, vuelve a intentarlo en unos momentos',
					'estatus' => 'error'
				);
				return redirect()->route('datos_pago')->with('alert', $data);
			}

			$config = [
				'mode'    => 'sandbox',
				'sandbox' => [
					'client_id'         => $credenciales['paypal']['keys_pasarelas']['public_key'],
					'client_secret'     => $credenciales['paypal']['keys_pasarelas']['private_key'],
					'app_id'            => 'APP-80W284485P519543T',
				],

				'payment_action' => 'Sale',
				'currency'       => 'MXN',
				'notify_url'     => 'https://your-app.com/paypal/notify',
				'locale'         => 'es_MX',
				'validate_ssl'   => true,
			];

            $provider->setApiCredentials($config);
            $token = $provider->getAccessToken();
            $provider->setAccessToken($token);

            $order = $provider->showOrderDetails($request->token);

            \Log::info('---------------Paypal Order Details-------------');
            \Log::info($order);

            if (isset($order['status']) && $order['status'] == 'APPROVED') {

                $payment_order = $provider->capturePaymentOrder($request->token);
                \Log::info('---------------Paypal Capture Payment Order-------------');
                \Log::info($payment_order);

                if (isset($payment_order['status']) && $payment_order['status'] == 'COMPLETED') {
                    $data = [
                        'paypal_info' => $request->all(),
                        'payment_order' => $payment_order,
                    ];

                    $response = $http->request('POST', $url_pv . 'paypal-success', ['json' => $data]);
                    $paypal_response = json_decode($response->getBody(), true);
					\Log::info('--------------- Response -------------');
					\Log::info($paypal_response);
                    if (isset($paypal_response['mensaje'])) {

                        $data = [
                            'titulo' => '',
                            'mensaje' => $paypal_response['mensaje'],
                            'estatus' => 'error'
                        ];

                        return redirect()->route('datos_pago')->with('alert', $data);
                    }

                    return redirect()->route('gracias', \Crypt::encryptString($paypal_response['id']));
                }
            }

            $data = [
                'titulo' => '',
                'mensaje' => 'No fue posible realizar la operación',
                'estatus' => 'error'
            ];

            return redirect()->route('datos_pago')->with('alert', $data);
        } catch (\Exception $e) {
			\Log::info('-------------- ERROR PAYPAL SUCCESS --------------');
            \Log::error($e);

            $data = [
                'titulo' => '',
                'mensaje' => 'Ocurrió un error al cargar la información',
                'estatus' => 'error'
            ];

            return redirect()->back()->with('alert', $data);
        }
    }

    public function paypal_cancel()
    {
        $data = array(
            'titulo' => '',
            'mensaje' => 'Se ha cancelado la operacion',
            'estatus' => 'error'
        );

        return redirect()->route('datos_pago')->with('alert', $data);
    }

	public function stripe(Request $request)
    {
        try{
			$http = new \GuzzleHttp\Client;
			$url_pv = url(env('URL_PV').'api/vk-lifestyle/');
			$payment_intent = $request->input('payment_intent');
			$payment_intent_client_secret = $request->input('payment_intent_client_secret');

			$data = [
				'pasarela' => 'stripe'
			];
			$response = $http->request('GET', $url_pv . 'get-credentials/' . env('VK_ID'), [ 'query' => $data ]);
			$credenciales = json_decode($response->getBody(), true);

			if(!isset($credenciales['stripe'])){
				$data = array(
					'titulo' => '',
					'mensaje' => 'Ocurrió un error al hacer el pago, vuelve a intentarlo en unos momentos',
					'estatus' => 'error'
				);
				return redirect()->back()->with('alert', $data);
			}

			\Stripe\Stripe::setApiKey($credenciales['stripe']['keys_pasarelas']['private_key']);

			$intent = \Stripe\PaymentIntent::retrieve($payment_intent);
			if($intent->status == 'succeeded')
			{
				$data = [
					'payment_intent' => $payment_intent,
					'payment_intent_client_secret' => $payment_intent_client_secret,
					'status' => $intent->status
				];

				$response = $http->request('POST', $url_pv . 'stripe-success', ['json' => $data]);
				$stripe_response = json_decode($response->getBody(), true);

				if (isset($stripe_response['mensaje'])) {

					$data = [
						'titulo' => '',
						'mensaje' => $stripe_response['mensaje'],
						'estatus' => 'error'
					];

					return redirect()->route('datos_pago')->with('alert', $data);
				}

				return redirect()->route('gracias', \Crypt::encryptString($stripe_response['id']));
			}else{
				$data = array(
					'titulo' => '',
					'mensaje' => 'No se puede procesar el pago',
					'estatus' => 'error'
				);
				return redirect()->route('datos_pago')->with('alert', $data);
			}
		}catch(\Exception $e){
			\Log::info($e);
			$data = array(
				'titulo' => '',
				'mensaje' => 'Ocurrió un error al cargar la información',
				'estatus' => 'error'
			);
			return redirect()->route('datos_pago')->with('alert', $data);
		}
    }

	public function gracias($venta){
		try{
			$http = new \GuzzleHttp\Client;
			$url_pv = url(env('URL_PV').'api/vk-lifestyle/');
			$data = [
				'venta' => $venta,
			];

			$response = $http->request('GET', $url_pv . 'get-venta/', [ 'query' => $data ]);
			$venta = json_decode($response->getBody(), true);

			$pasarela = [];
			if($venta['venta_ecommerce']['forma_de_pago'] == 'oxxo'){
				$data = [
					'pasarela' => 'stripe'
				];
				$response = $http->request('GET', $url_pv . 'get-credentials/' . env('VK_ID'), [ 'query' => $data ]);
				$credenciales = json_decode($response->getBody(), true);

				if(isset($credenciales['stripe'])){
					$pasarela = [
						'stripe' => [
							'public_key' => $credenciales['stripe']['keys_pasarelas']['public_key'],
							'private_key' => $credenciales['stripe']['keys_pasarelas']['private_key']
						]
					];
				}


				//get intent stripe
				\Stripe\Stripe::setApiKey($credenciales['stripe']['keys_pasarelas']['private_key']);
				$intent = \Stripe\PaymentIntent::retrieve($venta['venta_ecommerce']['referencia_pago_1']);

				if($intent)
				{
					$data = [
						'payment_intent' => $intent->id,
						'payment_intent_client_secret' => $intent->client_secret,
						'status' => $intent->status,
						'voucher_url' => $intent['next_action']['oxxo_display_details']['hosted_voucher_url'],
						'referencia_pago' => $intent['next_action']['oxxo_display_details']['number']
					];

					$response = $http->request('POST', $url_pv . 'oxxo-success', ['json' => $data]);
					$stripe_response = json_decode($response->getBody(), true);

					if (isset($stripe_response['mensaje'])) {

						$data = [
							'titulo' => '',
							'mensaje' => $stripe_response['mensaje'],
							'estatus' => 'error'
						];

						return redirect()->route('datos_pago')->with('alert', $data);
					}

					//return redirect()->route('gracias', \Crypt::encryptString($stripe_response['id']));
				}else{
					$data = array(
						'titulo' => '',
						'mensaje' => 'No se puede procesar el pago',
						'estatus' => 'error'
					);
					return redirect()->route('datos_pago')->with('alert', $data);
				}


				if(isset($venta['mensaje'])){
					$data = array(
						'titulo' => '',
						'mensaje' => $venta['mensaje'],
						'estatus' => 'error'
					);
					return redirect()->route('home')->with('alert', $data);
				}


				return view('compra.gracias')
				->with('venta', $venta ?? [])
				->with('voucher_url', $intent['next_action']['oxxo_display_details']['hosted_voucher_url'] ?? '')
				->with('number', $intent['next_action']['oxxo_display_details']['number'] ?? '');
			}

			if(isset($venta['mensaje'])){
				$data = array(
					'titulo' => '',
					'mensaje' => $venta['mensaje'],
					'estatus' => 'error'
				);
				return redirect()->route('home')->with('alert', $data);
			}

		} catch (\Exception $e) {
			\Log::info('-------------- ERROR GRACIAS --------------');
			\Log::error($e);
		}

		return view('compra.gracias')->with('venta', $venta ?? []);
	}

	public function perfil(Request $request){
		if ($request->isMethod('get')) {
        	return view('perfil.perfil');
		}elseif ($request->isMethod('post')) {
			try{
				$http = new \GuzzleHttp\Client;
				$url_pv = url(env('URL_PV').'api/vk-lifestyle/');

				$request->merge(['session_token' => $request->cookie('session_token')]);
				$data = $request->all();
				if($request->has('pass') && $request->input('pass') == 'on'){
					$error = array(
						'titulo' => '',
						'mensaje' => '',
						'estatus' => 'warning'
					);

					if(strlen($request->input('password')) < 6 ){
						$error['mensaje'] = "La contraseña debe tener al menos 6 caracteres";
						return redirect()->back()->with('alert', $error)->withInput();
					}elseif($request->input('password') != $request->input('password')){
						$error['mensaje'] = "Las contraseñas no coinciden";
						return redirect()->back()->with('alert', $error)->withInput();
					}else{
						$data = $request->except('pass', 'password', 'confirmar');
						$data['password'] = \Crypt::encryptString($request->input('password'));
					}
				}

				$response = $http->request('POST', $url_pv . "update-user", ['json' => $data]);
				$response = json_decode($response->getBody(), true);

				if (isset($response['estatus'])) {
					$data = array(
						'titulo' => '',
						'mensaje' => $response['mensaje'],
						'estatus' => $response['estatus']
					);
					if(isset($response['data'])){
						$data['mensaje'] = reset($response['data'])[0];
					}
					return redirect()->back()->with('alert', $data)->withInput();
				}
				Cookie::queue(Cookie::forever('session_token', $response['token']));
				$data = array(
					'titulo' => '',
					'mensaje' => 'Información actualizada',
					'estatus' => 'success'
				);
				return redirect()->back()->with('alert', $data);
			}catch(\Exception $e){
				\Log::info('-------------- ERROR POST PERFIL --------------');
				\Log::info($e);
				$data = array(
					'titulo' => '',
					'mensaje' => 'Ocurrió un error al actualizar la información',
					'estatus' => 'error'
				);
				return redirect()->back()->withInput()->with('alert', $data);
			}
		}
    }

	public function mis_compras(Request $request){
		try{
			$http = new \GuzzleHttp\Client;
			$url_pv = url(env('URL_PV').'api/vk-lifestyle/');

			$data = [
				'session_token' => $request->cookie('session_token')
			];
			$response = $http->request('GET', $url_pv . "compras", [ 'query' => $data ]);
			$compras = json_decode($response->getBody(), true);

		}catch(\Exception $e){
			\Log::info('-------------- ERROR MIS COMPRAS --------------');
			\Log::info($e);
		}

        return view('perfil.mis_compras')->with('compras', $compras ?? []);
    }

	public function newsletter(Request $request)
    {
        $http = new \GuzzleHttp\Client;
        $url_pv = url(env('URL_PV').'api/vk-lifestyle/');
		try {
			$validator = Validator::make($request->all(), [
				'g-recaptcha-response' => 'required',
			]);

			if ($validator->fails()) {
				$data = array(
					'titulo' => 'Ups!',
					'mensaje' => 'Te ha faltado llenar el reCAPTCHA',
					'estatus' => 'error'
				);
				return redirect()->back()->with('alert', $data)->withInput();
			}

			$response = $http->request('POST', $url_pv . "newsletter/" . env('VK_ID'), ['json' => $request->all()]);
			$response = json_decode($response->getBody(), true);
			if (isset($response['estatus'])) {
				$data = array(
					'titulo' => '',
					'mensaje' => $response['mensaje'],
					'estatus' => $response['estatus']
				);
				return redirect()->back()->with('alert', $data)->withInput();
			}
			// $mandrill = new Mandrill(env('MANDRILL_SECRET_KEY'));
			// $result = $mandrill->messages->sendTemplate('Mail Contacto PrecioMayoreo', [], array(
			//     'subject' => $request->input('asunto'),
			//     'from_email' => 'no-reply@corvuz.com',
			//     "from_name" => "PRECIOS DE MAYOREO",
			//     'to' => array(
			//         array(
			//             'email' => $configuracion->correo,
			//             "type" => "to"
			//         ),
			//     ),
			//     "merge" => true,
			//     'global_merge_vars' => array(
			//         array(
			//             'name' => 'nombre',
			//             'content' => $request->input('nombre')
			//         ),
			//         array(
			//             'name' => 'email',
			//             'content' => $request->input('email')
			//         ),
			//         array(
			//             'name' => 'telefono',
			//             'content' => $request->input('telefono')
			//         ),
			//         array(
			//             'name' => 'mensaje',
			//             'content' => $request->input('mensaje')
			//         ),
			//     )
			// ));
			$data = array(
				'titulo' => '¡Gracias!',
				'mensaje' => 'Te has suscrito correctamente.',
				'estatus' => 'success'
			);

			return redirect()->back()->with('alert', $data);
		} catch (\Exception $e) {
			\Log::info($e);
			$data = array(
				'titulo' => '',
				'mensaje' => 'Ocurrió un error al suscribirte, vuelve a intentarlo en unos momentos',
				'estatus' => 'error'
			);
			return redirect()->back()->with('alert', $data)->withInput();
		}
    }

	public function privacidad(){
        return view('legales.privacidad');
    }
	public function cambios(){
        return view('legales.cambios');
    }
	public function terminos(){
        return view('legales.terminos');
    }

	public function recuperar_contrasena(Request $request){
		if ($request->isMethod('get')) {
			return view('acceso.recuperar_password');
		}elseif ($request->isMethod('post')) {
			$http = new \GuzzleHttp\Client;
			$url_pv = url(env('URL_PV').'api/vk-lifestyle/');

			$request->merge(['url' => route('nueva_contrasena')]);
			$response = $http->request('POST', $url_pv . "recuperar-contrasenia/" . env('VK_ID'), ['json' => $request->all()]);
			$response = json_decode($response->getBody(), true);

			$data = array(
				'titulo' => '',
				'mensaje' => $response['mensaje'],
				'estatus' => $response['estatus']
			);
			if(isset($response['data'])){
				$data['mensaje'] = reset($response['data'])[0];
				return redirect()->back()->with('alert', $data)->withInput();
			}
			return redirect()->back()->with('alert', $data);
		}
    }

	public function nueva_contrasena(Request $request){
		if ($request->isMethod('get')) {
			return view('acceso.nueva_password');
		}elseif ($request->isMethod('post')) {

			$data = [];
			$error = array(
				'titulo' => '',
				'mensaje' => '',
				'estatus' => 'warning'
			);
			if(strlen($request->input('nueva_contrasenia')) < 6 ){
				$error['mensaje'] = "La contraseña debe tener al menos 6 caracteres";
				return redirect()->back()->with('alert', $error)->withInput();
			}elseif($request->input('nueva_contrasenia') != $request->input('confirmar_contrasenia')){
				$error['mensaje'] = "Las contraseñas no coinciden";
				return redirect()->back()->with('alert', $error)->withInput();
			}else{
				$data = $request->except('nueva_contrasenia', 'confirmar_contrasenia');
				$data['password'] = \Crypt::encryptString($request->input('nueva_contrasenia'));
			}

			$http = new \GuzzleHttp\Client;
			$url_pv = url(env('URL_PV').'api/vk-lifestyle/');

			$response = $http->request('POST', $url_pv . "nueva-contrasenia/" . env('VK_ID'), ['json' => $data]);
			$response = json_decode($response->getBody(), true);

			$data = array(
				'titulo' => '',
				'mensaje' => $response['mensaje'],
				'estatus' => $response['estatus']
			);
			if(isset($response['estatus']) && $response['estatus'] == 'success'){
				return redirect()->route('login')->with('alert', $data);
			}
			return redirect()->back()->with('alert', $data);
		}
    }
	public function search(Request $request)
    {
		$query = $request->all();
		$http = new \GuzzleHttp\Client;

        // Realiza la búsqueda en los productos obtenidos
        $url_pv = url(env('URL_PV').'api/vk-lifestyle/');

		$response = $http->request('GET', $url_pv . "productos/search/" . env('VK_ID'), ['query' => $query]);
        $resultados = json_decode($response->getBody(), true);
        return response()->json($resultados);
    }

	public function new_intent(Request $request){
		try {

			$session_token = request()->session()->get('_token');
			$http = new \GuzzleHttp\Client;
			$url_pv = url(env('URL_PV').'api/vk-lifestyle/');

			// Consulta de credenciales
			$data = [
				'pasarela' => 'stripe',
			];

			$response = $http->request('GET', $url_pv . 'get-credentials/' . env('VK_ID'), [ 'query' => $data ]);
			$credenciales = json_decode($response->getBody(), true);

			$data = [
				'session_token' => $session_token,
			];
			if($request->cookie('session_token')){
				$data['auth_token'] = $request->cookie('session_token');
			}

			$response = $http->request('GET', $url_pv . 'get-cart/' . env('VK_ID'), [ 'query' => $data ]);
			$carrito = json_decode($response->getBody(), true);

			if ($carrito['cantidad_productos'] == 0) {
				$mensaje = array(
					'titulo' => '',
					'mensaje' => 'No hay productos en tu carrito',
					'estatus' => 'warning'
				);
				return redirect()->route('home')->with('alert', $mensaje);
			}

			$street = $carrito['venta']['cliente']['direcciones'][0]['calle'].' '.$carrito['venta']['cliente']['direcciones'][0]['numero_ext'];
			if($carrito['venta']['cliente']['direcciones'][0]['numero_int'])
			{
				$street = $street . ' no. int. '.$carrito['venta']['cliente']['direcciones'][0]['numero_int'];
			}
			$street = $street . ' Col. '.$carrito['venta']['cliente']['direcciones'][0]['colonia'];


			\Stripe\Stripe::setApiKey($credenciales['stripe']['keys_pasarelas']['private_key']);

			$payment_intent = \Stripe\PaymentIntent::create([
				'amount' => floatval($carrito['venta']['total'])*100,
				'currency' => 'mxn',
				'description' => 'Compra en ecommerce VK Lifestyle',
				'confirm' => false,
				'metadata' => [
					'order_id' => 'M-'.$carrito['venta']['id']
				],
				'payment_method_types' => ['oxxo'],
				'payment_method_options' => [
					'oxxo' => [
						'expires_after_days' => 2
					]
				],
				'shipping' => [
					"name" => $carrito['venta']['cliente']['nombre'],
					"phone" => $carrito['venta']['cliente']['telefono'],
					"address" => array(
						'line1' => $street,
						'city' => $carrito['venta']['cliente']['direcciones'][0]['ciudad'],
						'country' => 'México',// $carrito['venta']['cliente']['direcciones'][0]['pais'],
						'postal_code' => $carrito['venta']['cliente']['direcciones'][0]['cp'],
						'state' => $carrito['venta']['cliente']['direcciones'][0]['estado']
					)
				]
			]);

			$paymentIntentConfirm = \Stripe\PaymentIntent::retrieve([
				'id' => $payment_intent->id,
			]);

			$paymentMethod = \Stripe\PaymentMethod::create([
				'type' => 'oxxo',
				'billing_details' => [
					'name' => $carrito['venta']['cliente']['nombre'],
					'email' => $carrito['venta']['cliente']['email']
				]
			]);

			$paymentIntentConfirm->confirm([
				'payment_method' => $paymentMethod->id,
			]);

			$vigencia = new \DateTime();
			$vigencia->modify('+2 day');
			$vigencia = $vigencia->format('Y-m-d H:i:s');

			$venta_ecommerce = [
				'referencia_pago_1' => $payment_intent->id,
				'referencia_pago_2' => $payment_intent->client_secret,
				'referencia_pago_3' => $payment_intent->status,
				'session_token' => $session_token,
				'forma_de_pago' => 'oxxo',
				'vigencia' => $vigencia,
			];

			if($request->cookie('session_token')){
				$venta_ecommerce['auth_token'] = $request->cookie('session_token');
			}

			$response = $http->request('POST', $url_pv . 'nueva-venta-ecommerce/' . env('VK_ID'), ['json' => $venta_ecommerce]);
			$venta_ecommerce_response = json_decode($response->getBody(), true);

			$data = [
				'payment_intent' => $payment_intent,
				'venta_ecommerce' => \Crypt::encryptString($venta_ecommerce_response['venta_id'])
			];


			return response()->json($data);

		} catch (\Exception $e) {
			\Log::info('---------------------- Error Datos De Pago ----------------------');
			\Log::info($e);
			$data = array(
				'titulo' => '',
				'mensaje' => 'Ocurrió un error al cargar las formas de pago',
				'estatus' => 'error'
			);
			return view('compra.datos_pago')->with('credenciales', $pasarela ?? [])->with('alert', $data);
		}

	}

	static function calcularUSD($response_products){
		if(!app()->isLocale('es')){
			if(isset($response_products['productos']) && isset($response_products['productos']['data']) && count($response_products['productos']['data']) > 0){
				foreach ($response_products['productos']['data'] as $key => &$producto) {
					if(isset($producto['variantes']) && count($producto['variantes']) > 0){
						$producto['variantes'][0]['precio'] = ceil(floatval($producto['variantes'][0]['precio']) / 20);
						if(isset($producto['variantes'][0]['precio_descuento'])){
							$producto['variantes'][0]['precio_descuento'] = ceil(floatval($producto['variantes'][0]['precio_descuento']) / 20);
						}
						if(isset($producto['variantes'][0]['precio_promocion'])){
							$producto['variantes'][0]['precio_promocion'] = ceil(floatval($producto['variantes'][0]['precio_promocion']) / 20);
						}
					}
				}
			}
		}
		return $response_products;
	}
}
