@extends('layout')
@section('head')
    <title>{{ strtoupper($detalle_producto['producto']['nombre']) }} | Calzado de Piel para Dama - My VK Shoes | Leather Lifestyle</title>
	@vite(['resources/stylus/detalle_producto.styl'])
	<!-- opengraph-->
	<meta property="og:site_name" content="{{ strtoupper($detalle_producto['producto']['nombre']) }} | Calzado de Piel para Dama - My VK Shoes | Leather Lifestyle"/>

	@if(count($detalle_producto['producto']['imagenes'])>0)
		@if(count($detalle_producto['producto']['imagenes'])>1)
			<meta property="og:image" content="{{$detalle_producto['producto']['imagenes'][1]['imagen_url']}}"/>
		@else
		<meta property="og:image" content="{{$detalle_producto['producto']['imagenes'][0]['imagen_url']}}"/>
		@endif
	@else
		<meta property="og:image" content="{{asset('/images/default.jpg')}}"/>
	@endif
	<style>
        #slider-container {
            width: 80%;
            margin: 20px auto;
        }
        #mySpriteSpin {
            margin-top: 20px;
        }
    </style>
	<script type="text/javascript">
		fbq('track', 'ViewContent', {
			content_id: ['{{ $detalle_producto["producto"]["producto_id"] }}'],
			content_type: 'product',
		});
	</script>
@endsection
@section('contenido')
    <div class="detalle layout">
		<div class="detalle-contenido">
			@php
				$is360 = false;
				if(count($detalle_producto['producto']['metacampos']) > 0){
					foreach($detalle_producto['producto']['metacampos'] as $metacampo){
						if($metacampo['nombre'] == '360°' && $metacampo['valor'] == 1){
							if(count($detalle_producto['producto']['imagenes'])>0){
								$is360 = true;
							}
							break;
						}
					}
				}
			@endphp
			@if($is360)
				<div class="detalle-contenido-imagenes">
					<div class="detalle-contenido-imagenes-imagen w-100">
						<div class="detalle-360">
							<span class="icon icon-anterior" id="360-anterior"></span>
							<div id='mySpriteSpin'></div>
							<span class="icon icon-siguiente" id="360-siguiente"></span>
						</div>
						<div class="btn-pausa">
							<button class="btn-scnd pausa" id="pausar360">
								<span>
									<svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22" fill="none">
										<g clip-path="url(#clip0_313_2192)">
											<path d="M13.2444 0.229779C15.2476 0.648469 17.0966 1.61287 18.5878 3.01678C19.8371 4.20715 20.7882 5.72546 21.4232 7.57859C23.1184 12.4967 20.9188 18.0716 16.3642 20.5892C10.5473 23.804 3.33688 21.3329 0.86752 15.1417C-0.764974 11.049 -0.0956555 7.19127 2.75348 3.79057C3.2366 3.21206 3.78143 2.68822 4.37824 2.22837C5.65539 1.2582 7.13048 0.582247 8.69834 0.248712C10.1959 -0.0737938 11.7442 -0.0802402 13.2444 0.229779ZM7.16894 11.3331C7.16894 13.039 7.17668 14.7441 7.16207 16.45C7.16207 16.8064 7.27978 16.9295 7.63292 16.9191C8.33317 16.8985 9.03515 16.9045 9.7354 16.9191C10.0301 16.9243 10.1418 16.8115 10.1409 16.5155C10.1358 13.0611 10.1358 9.60702 10.1409 6.15324C10.1409 5.85629 10.0258 5.74612 9.73368 5.75129C9.04631 5.76162 8.35895 5.77366 7.67158 5.74612C7.27377 5.73063 7.15606 5.87006 7.15606 6.26255C7.1784 7.95214 7.16808 9.64347 7.16894 11.3339V11.3331ZM15.4586 11.3331C15.4586 9.62798 15.4517 7.92374 15.4646 6.21865C15.4646 5.86834 15.3598 5.73062 14.9998 5.74267C14.3571 5.76419 13.711 5.75817 13.0691 5.74267C12.7589 5.73579 12.6344 5.852 12.6395 6.16788C12.6452 9.60617 12.6452 13.0442 12.6395 16.4819C12.6395 16.7935 12.7529 16.9191 13.0691 16.9122C13.6981 16.8976 14.3279 16.889 14.9594 16.9122C15.352 16.9286 15.4801 16.7952 15.4749 16.3958C15.4491 14.7165 15.4594 13.0261 15.4586 11.3365V11.3331Z" fill="black"/>
										</g>
										<defs>
											<clipPath id="clip0_313_2192">
											<rect width="22" height="22" fill="white" transform="translate(-0.00292969)"/>
											</clipPath>
										</defs>
									</svg>
									PAUSAR
								</span>
							</button>
							<button class="btn-scnd play" id="play360">
								<span>
									<svg xmlns="http://www.w3.org/2000/svg" width="22" height="23" viewBox="0 0 22 23" fill="none">
										<g clip-path="url(#clip0_313_2332)">
											<path d="M12.6806 0.707508C16.2382 1.26796 18.4332 3.45426 18.8918 3.93056C20.4449 5.53761 21.4945 7.63349 21.8736 10.0472C22.8062 15.9892 18.3295 21.865 12.3757 22.5079C9.52087 22.8167 6.86185 22.1184 4.7366 20.6474C4.22518 20.2938 1.23981 18.1594 0.284022 14.0975C-0.101185 12.4485 -0.101185 10.7328 0.284022 9.08386C0.788454 6.9033 1.94202 4.92743 3.59216 3.41756C5.21514 1.97477 7.21996 1.03241 9.36531 0.703923C10.4642 0.5363 11.5821 0.537511 12.6806 0.707508ZM6.98435 14.8531C6.98435 15.5693 6.99686 16.2856 6.97987 17.0018C6.97093 17.3671 7.05587 17.4083 7.37059 17.2247C8.55347 16.5345 9.75066 15.8666 10.9416 15.1906C13.001 14.0214 15.0607 12.853 17.1207 11.6856C17.3764 11.5405 17.5158 11.4537 17.1466 11.2379C13.9201 9.35126 10.6993 7.45443 7.48414 5.54746C7.15869 5.35408 7.06749 5.37736 7.06749 5.77576C7.06749 8.79916 7.07286 11.8261 6.98435 14.8513V14.8531Z" fill="black"/>
										</g>
										<defs>
											<clipPath id="clip0_313_2332">
											<rect width="22" height="22" fill="white" transform="translate(-0.00292969 0.578125)"/>
											</clipPath>
										</defs>
									</svg>
									PLAY
								</span>
							</button>
						</div>
					</div>
					@if(count($detalle_producto['producto']['imagenes'])>0)
						@if(count($detalle_producto['producto']['imagenes'])>8)
							@foreach (array_slice($detalle_producto['producto']['imagenes'], 8) as  $img)
								<div class="detalle-contenido-imagenes-imagen">
									<img src="{{ $img['imagen_url']}}" alt="VK_LIFESTYLE">
								</div>
							@endforeach
						@endif
					@else
						<div style="width: 100%; display:flex; justify-content: center;">
							<div class="detalle-contenido-imagenes-imagen">
								<img src="/images/default.jpg" alt="VK_LIFESTYLE">
							</div>
						</div>
					@endif
				</div>
			@else
				@if(count($detalle_producto['producto']['imagenes'])>0)
					<div class="product-detail-images">
						<div class="product-detail-images-thumbs">
							@if(!empty($detalle_producto['producto']['imagenes']))
								@foreach($detalle_producto['producto']['imagenes'] as $img)
									<div class="product-detail-images-thumbs-item">
										<img src="{{ $img['imagen_url']}}" alt="VK_LIFESTYLE">
									</div>
								@endforeach
							@endif
						</div>
						<div class="product-detail-images-focus">
							@if(!empty($detalle_producto['producto']['imagenes']))
								@foreach($detalle_producto['producto']['imagenes'] as $img)
									<div class="product-detail-images-focus-item">
										<img src="{{ $img['imagen_url']}}" alt="VK_LIFESTYLE">
									</div>
								@endforeach
							@endif
						</div>
					</div>
					{{-- <div class="detalle-contenido-imagenes" style="display: block">
						@foreach ($detalle_producto['producto']['imagenes'] as  $img)
							<div class="detalle-contenido-imagenes-imagen">
								<img src="{{ $img['imagen_url']}}" alt="VK_LIFESTYLE">
							</div>
						@endforeach
					</div> --}}
				@else
					<div class="detalle-contenido-imagenes">
						<div style="width: 100%; display:flex; justify-content: center;">
							<div class="detalle-contenido-imagenes-imagen">
								<img src="/images/default.jpg" alt="VK_LIFESTYLE">
							</div>
						</div>
					</div>
				@endif
			@endif

			<div class="detalle-contenido-info">
				<div class="titulo">
					<h1 class="titulo">{{ strtoupper($detalle_producto['producto']['nombre']) }}</h1>

					@if(count($detalle_producto['producto']['variantes']) > 0)
						@if(isset($detalle_producto['producto']['variantes'][0]['precio_descuento']) && $detalle_producto['producto']['variantes'][0]['precio_descuento'] > 0)
							<div style="display: flex">
								<p class="txt-black" style="text-decoration: line-through; color: rgb(255, 77, 77);">${{ number_format($detalle_producto['producto']['variantes'][0]['precio'], 2, '.', ',') }} {{ app()->isLocale('es') ? 'MXN' : 'USD' }}</p>
								<p class="txt-black" style="padding-left: 10px">${{ number_format($detalle_producto['producto']['variantes'][0]['precio_descuento'], 2, '.', ',') }} {{ app()->isLocale('es') ? 'MXN' : 'USD' }}</p>
							</div>
						@else
							<p class="txt-black">${{ number_format($detalle_producto['producto']['variantes'][0]['precio'], 2, '.', ',') }} {{ app()->isLocale('es') ? 'MXN' : 'USD' }}</p>
						@endif
					@else
						<p class="txt-black">No tiene un precio disponible</p>
					@endif
					@if (isset($detalle_producto['producto']) && isset($detalle_producto['producto']['promocion_nombre']) && $detalle_producto['producto']['promocion_nombre'])
						<p class="txt-black">
							{{ $detalle_producto['producto']['promocion_nombre'] }}
						</p>
					@endif
				</div>
				<br>
				<span class="etiqueta">
					<svg xmlns="http://www.w3.org/2000/svg" width="17" height="21" viewBox="0 0 17 21" fill="none">
						<path d="M15.9535 5.73902C16.0767 5.57049 16.2046 5.39609 16.3278 5.22062L16.4009 5.11395V4.76889L16.3439 4.67235C16.2963 4.59129 16.2476 4.50969 16.1994 4.42862C16.0959 4.25315 15.9871 4.07182 15.8887 3.89529C15.7334 3.61155 15.5263 3.23769 15.3368 2.86062C15.2794 2.73355 15.186 2.62736 15.0689 2.55605C14.9518 2.48474 14.8165 2.45165 14.6807 2.46115C14.3219 2.46809 13.9558 2.46649 13.6006 2.46542H13.293C12.1088 2.46275 11.2736 1.80995 10.9432 0.628087C10.93 0.583615 10.9143 0.539983 10.8961 0.49742L10.8842 0.46862L10.7874 0.21582H5.99153L5.90661 0.496887C5.75127 1.01369 5.54829 1.40355 5.27179 1.71769C4.84875 2.20089 4.33561 2.44409 3.70648 2.45902C3.12448 2.47289 2.55179 2.46702 2.11839 2.45902C1.98396 2.45107 1.85027 2.48444 1.73429 2.5549C1.61831 2.62536 1.52528 2.72973 1.467 2.85475C1.19723 3.37369 0.912954 3.89795 0.63852 4.40515L0.598132 4.47929C0.579337 4.50916 0.558575 4.53767 0.535995 4.56462C0.516837 4.58915 0.497678 4.61422 0.479037 4.63929L0.401367 4.74595V5.14222L0.504927 5.25689C1.18428 6.01849 1.7347 6.93742 2.18674 8.06649C2.82208 9.65156 2.7325 11.2436 1.91438 12.9326C1.47632 13.8366 1.02894 14.53 0.504927 15.115L0.401367 15.2297V15.5444L0.453147 15.6356C0.54428 15.8023 0.635759 15.9689 0.727582 16.1353C0.958002 16.554 1.19619 16.9886 1.41781 17.4153C1.58868 17.7476 1.83464 17.9022 2.1914 17.9022H2.19813C2.32551 17.9022 2.45289 17.9022 2.58079 17.898C3.11205 17.8905 3.61638 17.8836 4.10622 17.9513C5.3013 18.1177 6.3369 18.7604 7.19179 19.3678C7.40253 19.5172 7.60085 19.6878 7.81315 19.8697C7.90946 19.9534 8.00888 20.0393 8.10985 20.123L8.21651 20.2116H8.61108L8.72344 20.1049C8.76124 20.0681 8.798 20.0302 8.83477 19.9929C8.89485 19.9283 8.95884 19.8678 9.02635 19.8116C10.0231 19.0276 11.1276 18.2649 12.4438 18.0084C13.0098 17.898 13.6265 17.9017 14.223 17.9065H14.4539H14.4632C14.791 17.9065 15.198 17.834 15.4595 17.2889C15.6475 16.8974 15.867 16.5065 16.0808 16.1278C16.173 15.9641 16.2647 15.8004 16.3548 15.635L16.4065 15.5438V15.2382L16.3133 15.1257C14.9893 13.5294 14.3214 12.0324 14.2116 10.4084C14.1355 9.28409 14.4498 8.15449 15.2011 6.85315C15.4322 6.46804 15.6833 6.09611 15.9535 5.73902ZM14.0086 13.778C14.2732 14.2686 14.5829 14.7337 14.8821 15.1838C14.9505 15.2868 15.0194 15.3902 15.0893 15.4932C14.8485 15.923 14.6362 16.3193 14.4425 16.7006H14.428C14.2292 16.7097 14.0236 16.714 13.8248 16.7177C13.2697 16.7289 12.6955 16.7401 12.1326 16.8526C10.7273 17.1332 9.54104 17.923 8.46816 18.7342L8.40137 18.7849L8.29781 18.7081C8.05858 18.5326 7.81108 18.3508 7.55994 18.1806C6.05263 17.1614 4.67373 16.6724 3.27049 16.6724C3.06993 16.6724 2.86885 16.6823 2.66726 16.7022C2.37522 16.7305 2.30221 16.6665 2.21729 16.4457C2.09492 16.1488 1.94802 15.8633 1.7782 15.5924L1.71865 15.4905C2.32458 14.6933 2.82614 13.8175 3.21043 12.8857C4.04201 10.8761 3.94001 8.83235 2.907 6.81742C2.63101 6.27875 2.30014 5.77102 1.98014 5.28035C1.89108 5.14382 1.80202 5.00782 1.71451 4.87075C1.93664 4.47609 2.1536 4.07075 2.36072 3.66435L2.55697 3.66809C2.97121 3.67555 3.39632 3.68409 3.82195 3.65369C5.20499 3.55502 6.20797 2.80729 6.80292 1.43182C6.80913 1.41689 6.81483 1.40569 6.81897 1.39715H6.84072C7.9281 1.40995 8.99321 1.40569 9.98376 1.39715C10.6398 2.92035 11.7769 3.68409 13.365 3.66542H13.4908C13.8015 3.66169 14.1262 3.65742 14.4374 3.67235C14.63 4.01209 14.8112 4.35502 14.9867 4.68569L15.0546 4.81422C14.9448 5.00142 14.834 5.18755 14.7258 5.36622C14.399 5.91075 14.0904 6.42489 13.8165 6.97635C12.711 9.20569 12.7757 11.4932 14.0086 13.778Z" fill="white"/>
					</svg>
					PIEL
				</span>
				<br>
				<br>
				@if(count($detalle_producto['colores']) > 0)
					<div class="colores seccion">
						<h2>COLORES</h2>
						<div class="colores-contenido d-flex left">
							@foreach($detalle_producto['colores'] as $key => $color)
								<div class="color-item">
									<a href="{{route('detalle', $color['slug'])}}">
										<label for="color{{$key}}">
											@if(count($color['imagenes'])>0)
												@if(count($color['imagenes'])>1)
													<img src="{{ $color['imagenes'][1]['imagen_url']}}" alt="VK_LIFESTYLE">
												@else
													<img src="{{ $color['imagenes'][0]['imagen_url']}}" alt="VK_LIFESTYLE">
												@endif
											@else
												<img src="/images/default.jpg" alt="VK_LIFESTYLE">
											@endif
										</label>
									</a>
								</div>
							@endforeach
						</div>
					</div>
				@endif
				@if(count($detalle_producto['producto']['variantes']) > 0)
					<div class="talla seccion">
						<h2>TALLA</h2>
						<div class="talla-contenido d-flex">
							@foreach($detalle_producto['producto']['variantes'] as $key => $variante)
								<div class="recuadros-item">
									<input type="radio" name="talla" id="talla{{$key}}" {{ $variante['stock'] < 1 ? 'disabled' : '' }} @click="seleccionarProductoVariante({{$variante['id']}})">
									<label for="talla{{$key}}">{{ $variante['nombre'] }}</label>
								</div>
							@endforeach
						</div>
					</div>
				@else
					<p class="txt-black">No tiene tallas disponibles</p>
				@endif
				<br>
				<div class="boton">
					<button @click="addToCart({ variante_id: producto_variante_id_selected, cantidad: 1, codigo_pais: '{{ isset($position) && $position->countryCode ? $position->countryCode : 'US' }}', moneda: '{{ app()->isLocale('es') ? 'MXN' : 'USD' }}' })">
						Agregar al carrito
					</button>
				</div>
				<div class="meses">
					<svg xmlns="http://www.w3.org/2000/svg" width="36" height="27" viewBox="0 0 36 27" fill="none">
						<g clip-path="url(#clip0_307_2135)">
							<path d="M5.51737 21.7835C4.915 21.7468 4.36482 21.7509 3.82871 21.6756C3.0156 21.5687 2.26763 21.1686 1.7216 20.5486C1.17556 19.9286 0.867941 19.1301 0.854985 18.2992C0.83089 13.4987 0.83089 8.69765 0.854985 3.89587C0.859446 3.44151 0.952163 2.99249 1.12784 2.57444C1.30351 2.1564 1.55872 1.77753 1.87886 1.45946C2.199 1.14138 2.57781 0.890349 2.99368 0.72068C3.40954 0.55101 3.85431 0.466033 4.30258 0.470599C6.76027 0.440071 9.21997 0.470599 11.6797 0.470599C16.9444 0.470599 22.2099 0.470599 27.476 0.470599C29.3393 0.470599 30.7549 1.56147 31.0902 3.28531C31.2026 3.87552 31.1685 4.49221 31.2047 5.17808C31.536 5.17808 31.8552 5.17808 32.1765 5.17808C34.3671 5.20453 35.8369 6.70042 35.8389 8.92085C35.8389 13.5191 35.8389 18.1174 35.8389 22.7156C35.8389 24.9544 34.3872 26.4502 32.1905 26.4523C24.5216 26.4631 16.8521 26.4631 9.18182 26.4523C6.97311 26.4523 5.54146 24.9788 5.52339 22.7299C5.51536 22.4205 5.51737 22.1111 5.51737 21.7835ZM20.6249 7.55522V7.56743C16.836 7.56743 13.0471 7.56743 9.25812 7.56743C8.17586 7.56743 7.85258 7.95412 7.85258 9.05314C7.85258 13.5578 7.85258 18.0624 7.85258 22.567C7.85258 23.7068 8.23408 24.0914 9.36253 24.0914H31.9857C33.1302 24.0914 33.5198 23.6844 33.5198 22.5243V9.12233C33.5198 7.91341 33.1543 7.54911 31.9436 7.54911L20.6249 7.55522ZM3.16811 5.13737H28.8353V4.50441C28.8353 3.14284 28.512 2.81721 27.1567 2.81721H12.7338C9.98432 2.81721 7.23481 2.81721 4.4853 2.81721C3.68214 2.81721 3.21027 3.25478 3.17212 4.03834C3.15405 4.39451 3.16811 4.74254 3.16811 5.13737ZM5.48524 12.3054H3.19019C3.19019 14.3407 3.17012 16.3515 3.20626 18.3541C3.2096 18.6003 3.30381 18.8364 3.47031 19.0157C3.63682 19.195 3.86353 19.3046 4.1058 19.3229C4.56485 19.3577 5.02566 19.3618 5.48524 19.3351V12.3054ZM5.48524 9.89573C5.52942 9.10199 5.56957 8.35913 5.61174 7.60406H3.21831V9.89573H5.48524Z" fill="black"/>
							<path d="M20.6409 21.7428C17.617 21.7428 14.5944 21.7428 11.5732 21.7428C10.5692 21.7428 9.98089 21.0509 10.2339 20.2103C10.3925 19.6832 10.8122 19.3779 11.4386 19.3657C12.1133 19.3515 12.786 19.3657 13.4606 19.3657H29.6826C30.5982 19.3657 31.1162 19.7728 31.1483 20.4912C31.1805 21.2096 30.6805 21.7327 29.8673 21.7449C28.7569 21.7591 27.6465 21.7449 26.5362 21.7449L20.6409 21.7428Z" fill="black"/>
							<path d="M13.6433 12.264C12.8983 12.264 12.1514 12.264 11.4065 12.264C10.6615 12.264 10.1596 11.743 10.1716 11.0774C10.1836 10.4119 10.6997 9.9194 11.4386 9.91533C12.9311 9.90583 14.4217 9.90583 15.9102 9.91533C16.6592 9.91533 17.1531 10.3956 17.1591 11.0815C17.1652 11.7674 16.6913 12.2497 15.9323 12.264C15.1733 12.2782 14.4063 12.264 13.6433 12.264Z" fill="black"/>
						</g>
						<defs>
							<clipPath id="clip0_307_2135">
							<rect width="35" height="26" fill="white" transform="translate(0.836914 0.456055)"/>
							</clipPath>
						</defs>
					</svg>
					<span>Paga hasta 12 MSI con tarjeta de crédito</span>
				</div>
				<div class="detalles seccion">
					<h2>DETALLES</h2>
					<div class="line"></div>

					@if(count($detalle_producto['producto']['metacampos']) > 0)
						@foreach($detalle_producto['producto']['metacampos'] as $metacampo)
							@if(\Str::slug($metacampo['nombre']) == 'descripcion')
								<div class="descripcion">
									{{-- <h3>{{ ucfirst($metacampo['nombre']) }}:</h3> --}}
									<p>{{ $metacampo['valor'] }}</p>
								</div>
							@endif
						@endforeach
						@php
							$omitirMetacampos = [
								'descripcion',
								'gproduct-type',
								'ggoogle-product-category',
								'sexo'
							];
						@endphp
						@foreach($detalle_producto['producto']['metacampos'] as $metacampo)
							@if($metacampo['tipo'] != 'Si / No' && !in_array(\Str::slug($metacampo['nombre']), $omitirMetacampos))
								<div class="metacampo">
									<h3>{{ ucfirst($metacampo['nombre']) }}:</h3>
									@if($metacampo['tipo'] == 'Texto' || $metacampo['tipo'] == 'Texto largo')
										<p>{{ $metacampo['valor'] }}</p>
									@endif
								</div>
							@endif
						@endforeach
					@endif
				</div>
			</div>
		</div>
		@php
			if(isset($relacionados['productos'])){
				$relacionados['productos']['data'] = array_filter($relacionados['productos']['data'], function($relacionado) use($detalle_producto){ return $relacionado['producto_id'] != $detalle_producto['producto']['producto_id']; } );
			}
		@endphp
		@if(isset($relacionados['productos']) && count($relacionados['productos']['data']) > 0)
			<div class="detalle-relacionados">
				<div class="linea"></div>
				<h2 class="titulo">TAMBIEN PODRÍA GUSTARTE</h2>
				<div class="detalle-relacionados-contenido productos">
					@foreach ($relacionados['productos']['data'] as $relacionado)
						<div class="mg productos-item">
							<a href="{{route('detalle', $relacionado['slug'])}}" >
								<div class="productos-item-imagen">
									@if(count($relacionado['imagenes'])>0)
										@if(count($relacionado['imagenes'])>1)
											<img src="{{ $relacionado['imagenes'][1]['imagen_url']}}" alt="VK_LIFESTYLE">
											<img class="scnd" src="{{ $relacionado['imagenes'][0]['imagen_url']}}" alt="K_LIFESTYLE">
										@else
											<img src="{{ $relacionado['imagenes'][0]['imagen_url']}}" alt="VK_LIFESTYLE">
										@endif
									@else
										<img src="/images/default.jpg" alt="VK_LIFESTYLE">
									@endif
									@if(date('Y-m-d', strtotime($relacionado['created_at'])) > date('Y-m-d', strtotime(date('Y-m-d')."- 1 week")))
										<span class="etiqueta">NEW</span>
									@endif
								</div>
								<div class="productos-item-precio">
									<span class="txt-black">{{ $relacionado['nombre'] }}</span>
									@if(isset($relacionado['variantes'][0]['precio_descuento']) && $relacionado['variantes'][0]['precio_descuento'] > 0)
										<p style="text-decoration: line-through; color: rgb(255, 77, 77);">${{ number_format($relacionado['variantes'][0]['precio'], 2, '.', ',') }} {{ app()->isLocale('es') ? 'MXN' : 'USD' }}</p>
										<p>${{ number_format($relacionado['variantes'][0]['precio_descuento'], 2, '.', ',') }} {{ app()->isLocale('es') ? 'MXN' : 'USD' }}</p>
									@else
										<p>${{ number_format($relacionado['variantes'][0]['precio'], 2, '.', ',') }} {{ app()->isLocale('es') ? 'MXN' : 'USD' }}</p>
									@endif
								</div>
							</a>
							<div class="compra-rapida">
								<div class="compra-rapida-contenido">
									<div class="talla">
										<p>Seleccionar talla</p>
										<span class="icon icon-cerrar"></span>
									</div>
									<div class="recuadros">
										@foreach($relacionado['variantes'] as $variante)
											<div class="recuadros-item">
												<input type="radio" name="talla-relacionado-{{$relacionado['producto_id']}}" id="talla-relacionado-{{$variante['id']}}" {{ $variante['stock'] < 1 ? 'disabled' : '' }} @click="seleccionarProductoVariante({{$variante['id']}})">
												<label for="talla-relacionado-{{$variante['id']}}">{{ $variante['nombre'] }}</label>
											</div>
										@endforeach
									</div>
									<div class="boton" @click="addToCart({variante_id: producto_variante_id_selected, cantidad: 1})">
										<button>Agregar al carrito</button>
									</div>
								</div>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		@endif
    </div>
@endsection
@section('js')
	<script>
		$(document).ready(function(){
			let num_slides = $('.product-detail-images-focus-item').length;
			$('.product-detail-images-thumbs').slick({
				slidesToShow: num_slides,
				slidesToScroll: 0,
				infinite: false,
				arrows: false,
				vertical: true,
				verticalSwiping: false,
				asNavFor: '.product-detail-images-focus',
				centerMode: false,
				focusOnSelect: true,
				responsive: [
					{
						breakpoint: 951,
						settings:{
							slidesToShow: 3,
							slidesToScroll: 1,
							vertical: false,
							infinite: true,
							draggable: true
						}
					},
					{
						breakpoint: 451,
						settings:{
							slidesToShow: 3,
							slidesToScroll: 1,
							vertical: false,
							infinite: true,
							draggable: true
						}
					}
				]
			})
			$('.product-detail-images-focus').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false,
				fade: true,
				asNavFor: '.product-detail-images-thumbs',
				infinite: true,
			})
		})
    </script>
	@if($is360)
		<script src="https://unpkg.com/spritespin@4.0.11/release/spritespin.js" type="text/javascript"></script>
		<script type='module'>
			$(document).ready(function(){
				var imagenes = [
					@foreach (array_slice($detalle_producto['producto']['imagenes'], 0, 8) as  $img)
						"{{ $img['imagen_url']}}",
					@endforeach
				];
				var totalImages = imagenes.length;
				var currentFrame = 0;
				$("#mySpriteSpin").spritespin({
					source: imagenes,
					width: 700,
					height: 700,
					frames: totalImages,
					renderer:'canvas',
					animate: true,
					frameTime : 600,
					loop: false,
					stopFrame : totalImages,
					onInit: function(e, data) {
						$('.spritespin-slider')
							.attr("min", 0)
							.attr("max", data.source.length - 1)
							.attr("value", 0)
							.on("input", function(e) {
							SpriteSpin.updateFrame(data, e.target.value);
						})
					}
				});

				$('#360-anterior').on('click', function(){
					$("#mySpriteSpin").spritespin("animate", false);
					$('#play360').css('display', 'block');
					$('#pausar360').css('display', 'none');

					var data = $("#mySpriteSpin").spritespin("data");
					var currentFrame = data.frame;
					var previousFrame = (currentFrame > 0) ? currentFrame - 1 : data.frames - 1;
					SpriteSpin.updateFrame(data, previousFrame);
				})
				$('#360-siguiente').on('click', function(){
					$("#mySpriteSpin").spritespin("animate", false);
					$('#play360').css('display', 'block');
					$('#pausar360').css('display', 'none');

					var data = $("#mySpriteSpin").spritespin("data");
					var currentFrame = data.frame;
					var nextFrame = (currentFrame < data.frames - 1) ? currentFrame + 1 : 0;
					SpriteSpin.updateFrame(data, nextFrame);
				});
				$('#pausar360').on('click', function(){
					$("#mySpriteSpin").spritespin("animate", false);
					$(this).hide();
					$('#play360').css('display', 'block');
				})
				$('#play360').on('click', function(){
					var data = $("#mySpriteSpin").spritespin("data");
					$("#mySpriteSpin").spritespin("animate", true);
					$(this).hide();
					$('#pausar360').css('display', 'block');
				})
			});

		</script>

	@endif
@endsection
