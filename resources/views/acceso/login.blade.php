@extends('layout')
@section('head')
    <title>LOGIN -VK LIFESTYLE</title>
	@vite(['resources/stylus/login.styl'])
	<link rel="stylesheet" href="{{asset('plugins/validationEngine/validationEngine.jquery.css')}}">

	@endsection
@section('contenido')
	<div class="Login">
		<div class="layout">
			<h1 class="titulo">Login</h1>
			<form action="{{ route('login') }}" method="post" id="form-login">
				@csrf
				<div class="input text">
					<label for="">Email</label>
					<input type="email" name="email" data-validation-engine="validate[required,custom[email]]" value="{{old('email')}}">
				</div>
				<div class="input text">
					<label for="">Contraseña</label>
					<input type="password" name="password" data-validation-engine="validate[required,minSize[6]]">
				</div>
				<br>
				<br>
				<div class="boton">
					<button>Login</button>
				</div>
				<a href="{{route('recuperar_contrasena')}}">¿Olvidaste tu contraseña?</a>
				<a href="{{route('signin')}}">¿No tienes cuenta? Regístrate</a>
			</form>
		</div>
	</div>
@endsection
@section('js')
<script src="{{asset('/plugins/validationEngine/jquery.validationEngine.js')}}"></script>
	<script src="{{asset('/plugins/validationEngine/jquery.validationEngine-es.js')}}"></script>
	<script type="module">
		$(function(){
			$('#form-login').validationEngine({
				scroll: false
			});
		});
	</script>
@endsection
