<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Stripe\Stripe;
use Stripe\Event;

class StripeWebHookController extends Controller
{

    public function eventos(Request $request)
    {
        try {
            \Log::info('--------------------------------OXXO PAGO 0--------------------------------');

            // Se obtiene el evento de Stripe desde el cuerpo de la solicitud
            $payload = $request->getContent();
            $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
            $event = null;

            \Log::info('--------------------------------OXXO PAGO 1--------------------------------');

            // Se establece la URL de la pasarela de ventas
            $http = new \GuzzleHttp\Client;
            $url_pv = url(env('URL_PV').'api/vk-lifestyle/');

            $data = [
                'pasarela' => 'stripe',
            ];
            $response = $http->request('GET', $url_pv . 'get-credentials/' . env('VK_ID'), [ 'query' => $data ]);
            $credenciales = json_decode($response->getBody(), true);

            $secret_key = $credenciales['stripe']['keys_pasarelas']['private_key'];
            // Se establece la clave secreta de la API de Stripe
            Stripe::setApiKey($secret_key);

            \Log::info('--------------------------------OXXO PAGO--------------------------------');

            // Se construye el evento
            try {
                $event = Event::constructFrom(
                    json_decode($payload, true),
                    $sig_header,
                    env('STRIPE_WEBHOOKKEY_PAGO')
                );
            } catch (\UnexpectedValueException $e) {
                // Se maneja el error al construir el evento
                \Log::error($e->getMessage);
                return response()->json(['error' => $e->getMessage()], 400);
            }

            // Handle the event
            switch ($event->type) {
                case 'payment_intent.succeeded':
                    $intent = $event->data->object;
                    
                    $data = [
                        'payment_intent' => $intent->id,
                        'payment_intent_client_secret' => $intent->client_secret,
                        'status' => $intent->status
                    ];

                    $response = $http->request('POST', $url_pv . 'oxxo-confirm-success', ['json' => $data]);
                    $stripe_response = json_decode($response->getBody(), true);

                    \Log::info('--------------------------------OXXO PAGO CONFIRMADO--------------------------------');
                    \Log::info($stripe_response);

                    break;
                // case 'payment_method.attached':
                //     $paymentMethod = $event->data->object; // contains a \Stripe\PaymentMethod
                    
                //     break;
                // // ... handle other event types
                default:
                    // Unexpected event type
                    http_response_code(400);
                    exit();
            }

            http_response_code(200);
        } catch (\Exception $e) {
            
            \Log::error($e->getMessage());
            return response()->json(['error' => $e->getMessage()], 400);

        }
    }
   
}
