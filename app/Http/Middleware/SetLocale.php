<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Stevebauman\Location\Facades\Location;
use Symfony\Component\HttpFoundation\Response;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
		// $position = Location::get(request()->ip()); // Get the location of the user
		// $position = Location::get(); // Usuario de USA
		$position = Location::get('189.162.47.140'); // Usuario de Mexico
		// $position = Location::get('195.12.50.155'); // Usuario de España

		if($position && $position->countryCode == 'MX'){
			app()->setLocale('es');
		}else{
			app()->setLocale('en');
		}

		View::share('position', $position);

        return $next($request);
    }
}
