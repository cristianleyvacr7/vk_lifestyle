@extends('layout')
@section('head')
	@vite(['resources/stylus/aviso.styl'])
    <title>CAMBIOS Y DEVOLUCIONES - VK LIFESTYLE</title>
@endsection
@section('contenido')
    <div class="aviso">
        <div class="aviso-contenido layout">
            <div class="aviso-contenido-menu">
                <div class="opciones">
                    <div class="opciones-opcion">
                        <a href="{{route('privacidad')}}" id="privacidad">AVISO DE PRIVACIDAD</a>
                    </div>
                    <div class="opciones-opcion">
                        <a href="{{route('cambios')}}" id="cambios" class="selected">CAMBIOS Y DEVOLUCIONES</a>
                    </div>
                    <div class="opciones-opcion">
                        <a href="{{route('terminos')}}" id="terminos">TÉRMINOS Y CONDICIONES</a>
                    </div>
                </div>
            </div>
            <div class="aviso-contenido-informacion">
                <div class="cambios active">
                    <h1>Políticas y condiciones generales de venta</h1>
					<p>
						1. En caso de que desees cambiar el producto recibido por otra variante del modelo, ya sea color o talla, deberás reportar la descripción correcta de la mercancía que desees cambiar a contacto@myvkshoes.com dentro de los primeros 15 días naturales a partir de que recibiste tu paquete.
						<br>
						<br>
						a) Al realizar tu reporte es necesario indicar el producto que enviarás de regreso, así como el artículo nuevo de cambio (Modelo, color y talla).
						<br>
						<br>
						2. La empresa te enviará una orden de aprobación.
						<br>
						<br>
						3. Vía e-mail recibirás una guía pre-pagada y tendrás que enviar el producto de
						cambio vía
						mensajería FedEx a la empresa Vk Leather S.A. de C.V. se encargará de
						reenviar el
						producto de cambio solicitado.
						<br>
						<br>
						4. El envío de tu nuevo producto podrá tardar de 5 a 10 días hábiles. En este caso, la empresa absorberá los gastos que generen por este envío en una sola vez. A
						Partir de una segunda solicitud de cambio, los gastos de envío transcurrieron por tu cuenta.
						<br>
						<br>
						Si no has recibido la mercancía, podrás enviarnos un e-mail a contacto@myvkshoes.com o comunicate a los teléfonos 4775807353 Nuestro horario de atención al cliente de tienda en línea es lunes a viernes de 9 a 6:00 pm.
					</p>
					<br>
					<br>
					<h2>Devoluciones.</h2>
					<p>
						Nuestro compromiso es ofrecerte artículos únicos y de alta calidad, y partiendo de esta premisa, podrás devolver y/o cambiar cualquier artículo comprado en el sitio <a href="https://myvkshoes.com/" style="text-decoration: underline">https://myvkshoes.com/</a> por las siguientes causas:
						<br>
						<br>
						a) Si el artículo presenta defectos de fabricación. BOTA Y CALZADO EN GENERAL. Cierre descompuesto, o en el caso de estoperoles, remaches o cualquier detalle de adorno que se haya desprendido a causa del trato de la caja en trayecto .
						<br>
						<br>
						b) Si existe equivocación en el artículo enviado, siempre y cuando este no presente muestras de maltrato o uso.
						<br>
						<br>
						c) El producto adquirido no te ajusta en talla o deseas cambiar por modelo, color o talla.
						<br>
						<br>
						A continuación, se detalla los pasos que debes seguir para hacer efectiva una devolución:
						<br>
						<br>
						1. En el caso de mercancía con defecto o daño, se aplicará el cambio físico del producto, únicamente si es reportada en los primeros 30 días naturales a partir de que hayas recibido tu pedido.
						<br>
						<br>
						2. En caso de que desees realizar cambio por talla, modelo o color, solo procederá en cambio si se reporta dentro de los primeros 15 días naturales posteriores a la entrega del producto.
						<br>
						<br>
						3. Para generar tu solicitud y obtener tu guía de retorno, es importante que tengas lista las siguiente información:
						<br>
						<br>
						● Número de pedido.
						<br>
						● Correo con el cual registraste tu pedido.
						<br>
						● Modelo/Descripción/Color/Talla de los productos a retornar.
						<br>
						● Fotografías de los productos a retornar. Para los casos de garantía (producto dañado), enviar fotografías evidenciando el daño. Para los casos que no es el producto que solicitaste, foto de la etiqueta y el producto que recibiste.
						<br>
						● Motivo de la devolución.
						<br>
						<br>
						4. Inicia tu solicitud en el botón Cambios y Devoluciones.
						<br>
						<br>
						5. Indica el modelo que retornaras, la razón de tu solicitud y selecciona el nuevo modelo y/o talla de cambio. Favor de exponer claramente y detalles de tu cambio, ya que tu solicitud se procederá con la información que proporciones en el registro.
						<br>
						<br>
						6. Tu solicitud será revisada y se verificará de manera preliminar que cumpla con los
						requisitos para generar tu guía de retorno.
						<br>
						<br>
						7. De ser aprobada, se te enviará un correo electrónico indicando paso a paso el
						proceso a seguir, así como una guía de forma electrónica ya con los datos de envío. Salvo que tu solicitud haya sido rechazada y se señale explícitamente el motivo en el correo que recibirás.
						<br>
						<br>
						8. Al recibir la guia electronica, deberás imprimir la misma y entregar el producto especificado en cualquier oficina de FedEx
					</p>
					<h3>NOTA***</h3>
					<p>
						Si el total de tu pedido es superior a los $3,500 pesos, es posible realizar un cambio con guía gratuita. Al llevar el paquete a FedEx, no debes realizar pago adicional, ya que la guia que entregues se encuentra pagada. En el caso que tu pedido sea inferior a $3,500 pesos, deberás cubrir los gastos correspondientes al transporte (regresó y reposición). Este pago deberás realizarlo mediante transferencia, deposito a la cuenta de VK Leather S.A. de C.V. que te enviaremos.
						<br>
						El primer cambio /devolución que se realice por tu compra, no generará costo de envios para ti, siempre y cuando se indique de esa forma en el correo que se te haga llegar, y se elabore el reporte en un periodo mexico de 15 días naturales para cambio y 30 días naturales por temas de calidad en el producto.
						<br>
						A considerar: En las solicitudes de cambio subsecuentes, derivados del mismo pedido, deberás cubrir los costos de paquetería, independientemente del total del monto en tu pedido.
					</p>
					<h2>REEMBOLSO</h2>
					<p>
						Si el producto presenta un daño de fabricación o un error en el envío del mismo, será efectivo, puedes solicitar un reembolso con el cumplimiento de las siguientes condiciones:
						<br>
						<br>
						1. En caso que la mercancía esté con defecto se aceptan reembolsos únicamente en los primeros 15 días naturales a partir de la entrega del producto.
						<br>
						<br>
						2. En la notificación de la mercancía errónea o dañada es posible aplicar el reembolso de la misma solo si esta fue reportada durante las primeros 15 días naturales posteriores a la entrega, a los siguientes teléfonos: Teléfono de atención al cliente 4775807353 correo electrónico contacto@myvkshoes.com adjuntando una descripción y fotografía que demuestre el tipo de daño por fabricación. “Enviar una fotografía señalando el defecto de fabricación.
						<br>
						<br>
						3. En caso de reembolso, recibirás vía correo electrónico una guía pre-pagada por parte de Vk Leather S.A. de C.V. para que presentes la guia y el artículo para el reembolso en las oficinas de FedEx. Además de enviar el producto adquirido en excelente estado, sin usar y no estar dañada y en su empaque original en buen estado a través de FeDex, dentro de los primeros 10 días naturales, sin costo alguno. Transcurrida dicha fecha, no será posible aplicar esta política.
						<br>
						<br>
						4. Al momento en que nos sea entregado el producto nos pondremos en contacto contigo para confirmar la recepción de la mercancía e iniciar el proceso de reembolso.
						<br>
						<br>
						5. En un plazo de 5 a 10 días hábiles. Vk Leather S.A. de C.V. te reembolsará el valor total pagado por el producto mediante un depósito o transferencia a la cuenta que nos indiques.
					</p>
					<h2>PRECIOS</h2>
					<p>
						Los precios de los productos en nuestro sitio ya incluyen IVA y están expresados en Pesos Mexicanos.
						<br>
						<br>
						Nos reservamos el derecho de modificar los precios de venta ofrecidos en el sitio en cualquier momento y sin previo aviso; tu orden de compra mantendrá el precio de cada producto al momento en que generaste y pagaste tu compra.
						<br>
						<br>
						Nuestra página de internet: <a href="https://myvkshoes.com/" style="text-decoration: underline">https://myvkshoes.com/</a> cuenta promociones y precios exclusivos para las compras en Línea.
					</p>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script>
    $('.preguntas-bloque-titulo').on('click', function(){
        $(this).find('.icon').toggleClass('icon-flecha')
        $(this).next().slideToggle()
    })
</script>
@endsection
