<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::group(['namespace' => 'Web'], function () {

	Route::group(['middleware' => 'setLocale'], function () { // Vistas donde se revisa la ubicación del usuario

		Route::get('/', "WebController@index")->name('home');
		Route::get('/producto/{slug}', "WebController@detalle")->name('detalle');
		Route::get('/catalogo/{metacampo_slug}', "WebController@catalogo")->name('catalogo');

		Route::get('/gracias/{venta}', "WebController@gracias")->name('gracias');

		Route::match(['get', 'post'], '/login', "AccesoController@login")->name('login');
		Route::match(['get', 'post'], '/signin', "AccesoController@signin")->name('signin');

		Route::group(['middleware' => 'auth_session'], function () {
			Route::match(['get', 'post'], '/perfil', "WebController@perfil")->name('perfil');
			Route::get('/mis-compras', "WebController@mis_compras")->name('mis_compras');
		});

		Route::match(['get', 'post'], '/recuperar-contrasena', "WebController@recuperar_contrasena")->name('recuperar_contrasena');
		Route::match(['get', 'post'], '/nueva-contrasena', "WebController@nueva_contrasena")->name('nueva_contrasena');

		Route::match(['get', 'post'], '/datos-envio', "WebController@envio")->name('datos_envio')->middleware('auth_cliente');
		Route::match(['get', 'post'], '/datos-pago', "WebController@pago")->name('datos_pago');

		// Estáticas
		Route::get('/politicas-de-privacidad', "WebController@privacidad")->name('privacidad');
		Route::get('/cambios-y-devoluciones', "WebController@cambios")->name('cambios');
		Route::get('/terminos-y-condiciones', "WebController@terminos")->name('terminos');

	});

	Route::post('/buscar/cp', 'WebController@cp' )->name('buscar_cp');

	Route::get('/salir', 'AccesoController@salir')->name('logout');


	Route::post('/newsletter', "WebController@newsletter")->name('newsletter');

	// Paypal
	Route::get('/paypal-cancel', 'WebController@paypal_cancel')->name('paypal_cancel');
	Route::get('/paypal-success', 'WebController@paypal_success')->name('paypal_success');

	// Stripe
	Route::get('/stripe','WebController@stripe')->name('stripe');
	Route::get('/new-intent','WebController@new_intent')->name('stripe_intent');
	Route::post('/oxxo-webhook','StripeWebHookController@eventos')->name('oxxo_webhook');

	// Buscador
	Route::get('/search', 'WebController@search')->name('search');

});

Route::group(['namespace' => 'Api', 'prefix' => 'api'], function () {

    // // RUTAS DE CARRITO
    Route::post('/cart', 'CartController@index');
    Route::post('/cart/add', 'CartController@add');
    Route::put('/cart/update', 'CartController@update');
    Route::delete('/cart/delete/{venta_producto_id}', 'CartController@delete');

	Route::post('/cart/cupon/aplicar/{codigo}', 'CartController@aplicar_cupon');
    Route::get('/cart/cupon/eliminar', 'CartController@eliminar_cupon');
});


