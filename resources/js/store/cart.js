const cart = {
    state: {
        venta: null,
        cantidad_productos: 0,
    },
    mutations: {
        SET_VENTA(state, venta) {
            state.venta = venta;
        },
        SET_CANTIDAD_PRODUCTOS(state, cantidad_productos) {
            state.cantidad_productos = cantidad_productos;
        },
    },
    actions: {
        async fetchCart({ commit }){
            const response = await axios.post('/api/cart');
			if(response.data.venta){
				response.data.venta.productos.forEach(venta_producto => {
					venta_producto.cantidad_modificada = parseInt(venta_producto.cantidad_modificada);
					venta_producto.cantidad = parseInt(venta_producto.cantidad);
				});
			}
            commit('SET_VENTA', response.data.venta);
            commit('SET_CANTIDAD_PRODUCTOS', response.data.cantidad_productos);
        },

        async addToCart({ commit }, { variante_id, cantidad, codigo_pais, moneda }) {
            try{

                if(variante_id == null) {
                    alert('Selecciona alguna talla');
                    return;
                }
				$('.spinner').show();
				const data = {
					variante_id: variante_id,
					cantidad: cantidad,
					codigo_pais: codigo_pais,
					moneda: moneda
				};
                const response = await axios.post('/api/cart/add', data);
				console.log(response.data);
                if (response.data.msg) {
					$('.spinner').hide();
					alert(response.data.msg);
                } else {
					if(response.data.venta){
						response.data.venta.productos.forEach(venta_producto => {
							venta_producto.cantidad_modificada = parseInt(venta_producto.cantidad_modificada);
							venta_producto.cantidad = parseInt(venta_producto.cantidad);
						});
					}
                    commit('SET_VENTA', response.data.venta);
                    commit('SET_CANTIDAD_PRODUCTOS', response.data.cantidad_productos);
					window.location.href = '/datos-envio';
					$('.spinner').hide();
                }
            }catch(error)
            {
				$('.spinner').hide();
                console.log(error);
            }
        },

        async updateQuantity({ commit }, { venta_producto, accion, cantidad_mod }) {
            try {
				const data = {
					venta_producto_id: venta_producto.id,
					accion: accion,
					cantidad: cantidad_mod
				};
				if(accion != undefined){
					if(accion == '-' && venta_producto.cantidad <= 1) {
						return;
					}
				}else{
					if(venta_producto.cantidad_modificada <= 0) {
						venta_producto.cantidad_modificada = venta_producto.cantidad;
						return;
					}

					let accion_buscada;
					let cantidad_modificada = venta_producto.cantidad_modificada - venta_producto.cantidad;
					if(cantidad_modificada > 0) {
						accion_buscada = '+';
					}else{
						accion_buscada = '-';
						cantidad_modificada = Math.abs(cantidad_modificada);
					}
					data.accion = accion_buscada;
					data.cantidad = cantidad_modificada;
				}
				$('.spinner').show();
                const response = await axios.put('/api/cart/update', data);

                if (response.data.msg){
					$('.spinner').hide();
                    alert(response.data.msg);
                } else {
					if(response.data.venta){
						response.data.venta.productos.forEach(venta_producto => {
							venta_producto.cantidad_modificada = parseInt(venta_producto.cantidad_modificada);
							venta_producto.cantidad = parseInt(venta_producto.cantidad);
						});
					}
                    commit('SET_VENTA', response.data.venta);
                    commit('SET_CANTIDAD_PRODUCTOS', response.data.cantidad_productos);
					$('.spinner').hide();
                }
            } catch (error) {
				$('.spinner').hide();
                console.error(error);
            }
        },

        async removeFromCart({ commit }, {venta_producto_id}) {
            try {
				$('.spinner').show();
                const response = await axios.delete(`/api/cart/delete/${venta_producto_id}`);
				$('.spinner').hide();
                if (response.data.msg) {
					alert(response.data.msg);
                } else {
					if(response.data.venta){
						response.data.venta.productos.forEach(venta_producto => {
							venta_producto.cantidad_modificada = parseInt(venta_producto.cantidad_modificada);
							venta_producto.cantidad = parseInt(venta_producto.cantidad);
						});
					}
                    commit('SET_VENTA', response.data.venta);
                    commit('SET_CANTIDAD_PRODUCTOS', response.data.cantidad_productos);
                }
            } catch (error) {
				$('.spinner').hide();
                console.error(error);
            }
        }
    }
};

export default cart;
