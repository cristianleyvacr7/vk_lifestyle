<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RecaptchaKeys
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
		try{
			$http = new \GuzzleHttp\Client;
			$url_pv = url(env('URL_PV').'api/vk-lifestyle/');

			$response = $http->request('GET', $url_pv . "get-credentials-recaptcha/" . env('VK_ID'));
			$credentials = json_decode($response->getBody(), true);
			if(isset($credentials['recaptcha_site_key'])){
				config(['recaptcha.api_site_key' => $credentials['recaptcha_site_key']]);
				config(['recaptcha.api_secret_key' => $credentials['recaptcha_secret_key']]);
			}
		}catch(\Exception $e){
			\Log::info('---------- ERROR RECAPTCHA ----------');
			\Log::info($e);
		}
        return $next($request);
    }
}
